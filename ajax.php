<?php 
$apiKey = 'test_b3ae25c3bb6010e74185d48920ff85dc82e666a2';
$url    ='https://direct.paylane.com/paylane';
const API_KEY = 'test_b3ae25c3bb6010e74185d48920ff85dc82e666a2';
const URL    ='https://direct.paylane.com/paylane';

const _FORM_TYPE_NEWSLETTER     = 'newsletter';
const _FORM_TYPE_REGISTRATION   = 'registration';
const _FORM_TYPE_EBOOK          = 'ebook';
const _FORM_TYPE_CONTACT        = 'contact';
const _FORM_TYPE_SALES_CONTACT  = 'sales-contact';


$errorTable = array(
    1001=>'first_name',
    1002=>'last_name',
    1003=>'company',
    1004=>'email',
    1005=>'phone',
    1006=>'website',
    1007=>'country',
    1008=>'subject',
    1009=>'message',
    1010=>'tags',
    1011=>'target',
    1012=>'details',
    1013=>'ebooks',
    // 1013=>'promo_code', //doouble 1013?
    1014=>'name',
    1015=>'email już istnieje (w create-account)',
);

//a wrapper for sending requests and handling responses
function sendRequest($_url, $data){
    $_url = URL.$_url;

    $ch = curl_init($_url);
    $data = json_encode($data);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Auth-Key:'.API_KEY));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = json_decode(curl_exec($ch));
    $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    return generateResponse($result,$info);
}

//return form field
function ff($f, $default = null){
    if(isset($_POST[$f])){
        return $_POST[$f];
    }

    return $default;
}

function generateDetails(){
    $output = array(
        "selected_country"=> "PL",                 
        "browser_language"=> substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
        // "browser_language"=> "pl",   //substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)           

        "ip"              => get_client_ip(),
        // "ip"              => "192.168.1.1",  //get_client_ip()      

        "source"          => "paylane_mainsite",   //$_SERVER['REQUEST_URI'],

        "domain"          => "paylane.pl",
        "page"            => "/cennik"
    );
    return $output;
}

function generateResponse($result,$info){
    $errorMessages = array(
        1001=>'Imię jest nieprawidłowe',
        1002=>'Nazwisko jest nieprawidłowe',
        1003=>'Firma jest nieprawidłowa',
        1004=>'Adres Email jest nieprawidłowy',
        1005=>'Telefon jest nieprawidłowy',
        1006=>'Strona jest nieprawidłowa',
        1007=>'Kraj jest nieprawidłowy',
        1008=>'Tytuł jest nieprawidłowy',
        1009=>'Wiadomość jest nieprawidłowa',
        1010=>'Tagi są nieprawidłowe',
        1011=>'Cel jest nieprawidłowy',
        1012=>'Szczegóły są nieprawidłowe',
        1013=>'Ebook jest nieprawidłowy',
        // 1013=>'promo_code', //doouble 1013?
        1014=>'Imię jest nieprawidłowe',
        1015=>'Email już istnieje',
    );

    $errormessage="";
    if(isset($result->error) && isset($result->error->code)){
        $errormessage = $errorMessages[$result->error->code];
    }
    return array(
        "result"=>$result,
        "info"=>$info,
        "errormessage"=>$errormessage
    );
}

function get_client_ip() { 
    $ipaddress = ''; 
    if (isset($_SERVER['HTTP_CLIENT_IP'])) 
        $ipaddress = $_SERVER['HTTP_CLIENT_IP']; 
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) 
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR']; 
    else if(isset($_SERVER['HTTP_X_FORWARDED'])) 
        $ipaddress = $_SERVER['HTTP_X_FORWARDED']; 
    else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) 
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR']; 
    else if(isset($_SERVER['HTTP_FORWARDED'])) 
        $ipaddress = $_SERVER['HTTP_FORWARDED']; 
    else if(isset($_SERVER['REMOTE_ADDR'])) 
        $ipaddress = $_SERVER['REMOTE_ADDR']; 
    else 
        $ipaddress = 'UNKNOWN'; 
    return $ipaddress; 
} 

function verify_recaptcha(){
    $secret = "6LeBEmUUAAAAAClHd6jQ_r_pZoMLVDtzs_WxLQ5t";//TODO: get it from rwmb_meta( $captcha, array( 'object_type' => 'setting' ), 'paylane' )
    $key    = "6LeBEmUUAAAAAD398Ysoe_jHBsi7F-JTUHXCxtTr";
	
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
	 	'secret' => $secret,
	 	'response' => $_POST["g-recaptcha-response"]
	 );
	 $options = array(
	 	'http' => array (
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
	 		'method' => 'POST',
	 		'content' => http_build_query($data)
	 	)
	 );
	 $context  = stream_context_create($options);
	 $verify = file_get_contents($url, false, $context);
	 $captcha_success=json_decode($verify);
	 if ($captcha_success->success==true) {
	 	return true;
     }
     return false;
}

$output = array();

if(isset($_POST['formtype']) && strlen($_POST['formtype'])){
    switch ($_POST['formtype']) {
        case _FORM_TYPE_NEWSLETTER:
            $d = generateDetails();
            $data = [
                "name" => ff('name'),
                "email" => ff('email'),
                "details" => $d,
            ];
            $output = sendRequest('/newsletters/subscribe', $data);
            break;
        case _FORM_TYPE_REGISTRATION:
                if(verify_recaptcha()){
                    $url.='/accounts/create';
                    $ch = curl_init($url);
                    $data = array(
                        'name'=>$_POST['name'],
                        'email'=>$_POST['email'],
                        'promo_code'=>"null",
                        'details'=>generateDetails()
                    );

                    if($_POST['promocode'] && $_POST['promocode']!=''){
                        $data['promo_code']=$_POST['promocode'];
                    }

                    $data = json_encode($data);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Auth-Key:'.$apiKey));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = json_decode(curl_exec($ch));
                    $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    $output = generateResponse($result,$info);
                }
                break;
        case _FORM_TYPE_EBOOK:
            if(!verify_recaptcha()){
                $output = ['captcha' => false];
                break;
            }
            $d = generateDetails();
            $data = [
                "email" => ff('email'),
                "ebooks" => [
                    ['id' => ff('ebook_id'), 'title' => ff('ebook_title')]
                ],
                "details" => $d,
            ];
            $output = sendRequest('/ebooks/get', $data);
            break;
        case _FORM_TYPE_CONTACT:
            if(!verify_recaptcha()){
                $output = ['captcha' => false];
                break;
            }
            // {
            //     "first_name": "Imię",                    // minlength: 2, maxlength: 32
            //     "last_name" : "Nazwisko",                // minlength: 2, maxlength: 32
            //     "company"   : "Moja Firma",              // minlength: 2, maxlength: 32
            //     "email"     : "imie.nazwisko@mail.pl",   // minlength: 6, maxlength: ??????
            //     "phone"     : "123123123",               // validacja????
            //     "website"   : "www.page.pl",             // validacja???
            //     "country"   : "PL",                      // minlength: 2, maxlength: 2
            //     "subject"   : "Temat",                   // validacja???
            //     "message"   : "Treść",                   // validacja???
            //     "tags"      : [],
            //     "target"    : "sales",                   // sales|support|partnership; required
            //     "details"   : {}                         // zdefiniowane na gÃ³rze; required
            //   }

            $d = generateDetails();
            $data = [
                "first_name"=> ff('name'),
                "last_name" => ff('surname'),
                "company" => ff('company'),
                "email" => ff('email'),
                "phone" => ff('phone'),
                "website" => ff('website'),
                "message" => ff('message'),

                "subject" => 'contact form',
                "country" => strtoupper($d['browser_language']),
                "tags" => [],
                "target" => 'support',
                "details" => $d,
            ];

            $output = sendRequest('/messages/basic/send', $data);
            break;
        case _FORM_TYPE_SALES_CONTACT:
            if(!verify_recaptcha()){
                $output = ['captcha' => false];
                break;
            }
            $d = generateDetails();
            $data = [
                "phone" => ff('phone'),
                "first_name"=> "",
                "last_name" => "",
                "company" => "",
                "email" => "",
                "phone" => "",
                "website" => "",
                "message" => "",
                "subject" => 'sales form',
                "country" => strtoupper($d['browser_language']),
                "target" => 'sales',
                "details" => $d,
            ];
            
            $output = sendRequest('/messages/basic/send', $data);
            break;
    }   
}

echo json_encode($output);