<?php

class MBB_SingleImage extends MBB_Field {
    public $basic = array(
		'id',
		'name',
		// 'label_description',
		'desc' => array(
			'type' 	=> 'textarea',
		),
		'force_delete' => array(
			'type' 	=> 'checkbox',
			// 'label' => 'Force Delete?'
		),

		'required' => array(
			'type' => 'checkbox',
		),
		'clone'	=> array(
			'type' 	=> 'checkbox',
			'size'	=> 'wide'
		),

	);

	public function register_fields() {
        parent::register_fields();

		if ( isset( $this->appearance['size'] ) ) {
			unset( $this->appearance['size'] );
		}
		if ( 'placeholder' === $this->appearance[0] ) {
			unset( $this->appearance[0] );
		}
    }
}

new MBB_SingleImage;
