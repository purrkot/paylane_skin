<?php

class MBB_Background extends MBB_Field {
	public $basic = array(
		'id',
		'name',
		// 'label_description',
		'desc' => array(
			'type' 	=> 'textarea',
		),
		'required' => array(
			'type' => 'checkbox',
		),
		'clone' => array(
			'type' => 'checkbox',
		),
	);
    public function register_fields() {
        parent::register_fields();
		if ( isset( $this->appearance['size'] ) ) {
			unset( $this->appearance['size'] );
		}
		if (  'placeholder' === $this->appearance[0] ) {
			unset( $this->appearance[0] );
		}
    }
}

new MBB_Background;
