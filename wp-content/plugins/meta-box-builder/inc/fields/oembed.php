<?php

class MBB_OEmbed extends MBB_Field
{
	public $basic = array(
		'id',
		'name',
		'desc' => array(
			'type' 	=> 'textarea',
		),
		'std',
		'required' => array(
			'type' => 'checkbox',
		),
		'clone' => array(
			'type' => 'checkbox',
		),
	);
    public function register_fields() {
        parent::register_fields();

		if (  'placeholder' === $this->appearance[0] ) {
			unset( $this->appearance[0] );
		}
    }
}

new MBB_OEmbed;