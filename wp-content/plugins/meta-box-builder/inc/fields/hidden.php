<?php

class MBB_Hidden extends MBB_Field
{
	public function __construct() {
		$this->basic = array(
			'id',
			'name',
			// 'label_description',
			'desc' => array(
				'type' 	=> 'textarea',
			),
			'std',
			'clone'	=> array(
				'type' 	=> 'checkbox',
				// 'label' => 'Force Delete?'
			),

		);

		parent::__construct();
	}

    public function register_fields() {
        parent::register_fields();

		if ( isset( $this->appearance['size'] ) ) {
			unset( $this->appearance['size'] );
		}
		if ( isset( $this->appearance['label_description'] ) ) {
			unset( $this->appearance['label_description'] );
		}
		if (  'placeholder' === $this->appearance[0] ) {
			unset( $this->appearance[0] );
		}
    }
}

new MBB_Hidden;