<?php
class MBB_Heading extends MBB_Field {
	public function __construct() {
		$this->basic = array(
			'name',
			// 'label_description',
			'desc' => array(
				'type' 	=> 'textarea',
			),

		);

		parent::__construct();
	}
    public function register_fields() {
        parent::register_fields();

		if ( isset( $this->appearance['size'] ) ) {
			unset( $this->appearance['size'] );
		}
		if (  'placeholder' === $this->appearance[0] ) {
			unset( $this->appearance[0] );
		}
    }
}
new MBB_Heading;