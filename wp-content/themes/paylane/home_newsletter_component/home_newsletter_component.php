
        <div class="home_newsletter_component row">
            <div class="col-lg-8 offset-lg-2">
                <div class="row">
                    <div class="col-lg-4 first-column">
                        <p>
                            Chcesz <span class="lightblue">być na bieżąco</span> z nowościami?<br>
                            Zapisz się na nasz <span class="darkblue">newsletter</span>
                        </p>
                    </div>
                    <div class="col-lg-8 second-column">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Wpisz swój email">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-arrow"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
