<?php 
	$left_additionals = rwmb_meta('left-charge');
	$right_additionals = rwmb_meta('right-charge');
?>
<div class="pricing_footer_component">
	<div class="container container1440">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10 col-lg-9">
				<div class="row">
					<div class="col-12 title">
						<h3>
							Dodatkowe opłaty uzależnione od metody płatności
						</h3>
					</div>
					<div class="col-12 col-lg-6 left d-flex justify-content-center">
						<table>
							<?php foreach($left_additionals as $left_additional){

							$additional_title = 'additional-title';
							$additional_content = 'additonal-content';

							echo "<tr>
									<td>",$left_additional[$additional_title],"</td>
									<td>",$left_additional[$additional_content],"</td>
								</tr>";
							}?>
						</table>
					</div>
					<div class="col-12 col-lg-6 right d-flex flex-column">
						<?php foreach($right_additionals as $right_additional){

							$subd_ads = $right_additional['additional-pos'];
							
							$additional_main_title = 'main-title-2';
	
							$additional_title_2 = 'additional-title-2';
							$additional_content_2 = 'additonal-content-2';

							$upper_table = "<h3 >". $right_additional['additonal-main-2'] ."</h3><table>";
							echo $upper_table;
								foreach($subd_ads as $sub_ad){
									$middle_table="<tr>
									<td>".$sub_ad[$additional_title_2]."</td>
									<td>".$sub_ad[$additional_content_2]."</td>
									</tr>";
									echo $middle_table;
								}
							$lowe_table="</table>";
							echo $lowe_table;
							}?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>