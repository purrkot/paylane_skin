var response2
var ebookRecaptcha2 = function(a){
    response2 = a
}

jQuery(document).ready(function ($) {
    let EbookSingleComponent = function(){
        var self=this;

        //init events
        $('.ebook-send-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData();
        })

        $('.ebook-send-form').on('validation', function(evt, valid){
            self.valid = valid;
        });
    
    }
    
    EbookSingleComponent.prototype.sendFormData = function(){
        var self = this
        if(!self.valid){
            return
        }

        let email = $('.ebook-send-form .email-input input').val();
        let ebookId = $('.ebook-send-form input#ebook_id').val();
        let title = $('.ebook-send-form input#ebook_title').val();

        $.post('/ajax.php',{
            'formtype': 'ebook', 
            'email': email, 
            'ebook_id': ebookId ,
            'ebook_title': title,
            'g-recaptcha-response': response2
        }).done(function(data){
            data = JSON.parse(data);
            if(data.info==200){
                $('.ebook-send-form .validation-errors').empty().append("Ebook został wysłany");
            }else{
                $('.ebook-send-form .validation-errors').empty().append(data.errormessage);
            }
        })
    }


    //init
    let oEbookSingleComponent = new EbookSingleComponent();
    $.validate({
        form : '.ebook-send-form',
        lang: 'pl',
        modules : 'html5',
    });
});