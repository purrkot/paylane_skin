<?php

$privacy_id = 'privacy-link';
$terms_id = 'terms-link';
$captcha = 'captcha';
$option_name = 'paylane';
$post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
$post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );

$site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="ebook_single_component">
	<div class="container container1440">
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 col-lg-5 cover-side d-flex flex-column">
				<h1 class="main-title">
					<?php the_title();?>
				</h1>
				<div class="cover-image"><img class="img-fluid" src="<?php the_post_thumbnail_url('full'); ?>" alt=""></div>
				<div class="social-wrapper">
				<a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/twitter.svg"></a>
                <img class="social-icon" id="facebook-share-<?php the_ID();?>" src="<?php echo get_template_directory_uri(); ?>/assets-blog/facebook.svg">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/linked-in.svg"></a>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-5 description-side d-flex flex-column">
				<div class="description-wrapper">
					<?php the_content();?>
				</div>
				<div class="tags-wrapper">					
						<img class="tag-img" src="<?php echo get_template_directory_uri();?>/assets-ebook/tag.svg" alt="">
						<?php the_tags();?>
				</div>
				<?php $ebook_id = rwmb_meta('ebook-id'); if(!empty($ebook_id)){?>
				<div class="ebook-send-form-wrapper">
					<form action="" class="ebook-send-form">
						<div class="email-input">
							<div class="icon"></div><input type="email" placeholder="Wpisz swój adres e-mail" required data-validation="required">
						</div>
						<input type="hidden" name="ebook_id" value="<?php echo $ebook_id;?>" id="ebook_id"/>
						<input type="hidden" name="title" value="<?php the_title();?>" id="ebook_title"/>
						<div class="policy-link">
							<span>Akceptuję </span><a href="<?php the_permalink( $post_id_2 );?>">regulamin</a> i <a href="<?php the_permalink( $post_id );?>">politykę prywatności.</a>
						</div>
        				<div class="g-recaptcha" id="recap2" data-sitekey="<?php echo $site_key;?>" data-callback="ebookRecaptcha2"></div>
						<button type="submit" class="submit-button submit btn"><span><span>Wyślij poradnik</span></span></button>
						<div class="form-group validation-errors">
                        </div>
					</form>
				</div>
				<?php };?>
			</div>
		</div>
	</div>
</div>
<script>
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '1918887535015636',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>