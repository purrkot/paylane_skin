
<?php 
$payments_id = 'payments-link';
$option_name = 'paylane';
$post_id_3 = rwmb_meta( $payments_id, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="pricing_logo_rotator_component">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 slogan-row">
				<h3 class="slogan">
					Poznaj wiele innych metod płatności dostępnych dzięki PayLine
				</h3>
			</div>
			<?php echo get_template_part('logo_rotator_component/logo_rotator_component');?>
			<div class="col-12 pay-method-link">
				<a href="<?php the_permalink($post_id_3);?>">Więcej metod płatności<img  src="<?php echo get_template_directory_uri(); ?>/assets-pricing/chevrons-right.svg"></a>
			</div>
		</div>
	</div>
</div>