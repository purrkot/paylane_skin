jQuery(document).ready(function($){

    if($('.payment_methods_pay_card_component').length){

        $('.payment_methods_pay_card_component .card-logos-wrapper a img').on('mouseover',(e)=>{
            let clickedIndex = $(e.target).attr('index');            
            $(e.target).addClass('active');
            let changelogo = $(e.target).attr('src');
            $('.payment_methods_pay_card_component .card-logos-wrapper a img').removeClass('active');
            $('.payment_methods_pay_card_component .card-holder').css('background-image','url('+changelogo+')');
            $('.payment_methods_pay_card_component .description-wrapper p.active').removeClass('active');
            $('.payment_methods_pay_card_component .description-wrapper p').eq(clickedIndex).addClass('active');
            $('.payment_methods_pay_card_component .button-bar a.active').removeClass('active');
            $('.payment_methods_pay_card_component .button-bar a:not(.btn)').eq(clickedIndex).addClass('active');
        });

        let firstLogo = $('.payment_methods_pay_card_component .card-logos-wrapper a img:first').attr('src');
        $('.payment_methods_pay_card_component .card-holder').css('background-image','url('+firstLogo+')');



    }
})