<?php   
    $register_id = 'register-link';
	$contact_id = 'contact-link';
	$payments_id = 'payments-link';
	$option_name = 'paylane';
	$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
	$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );	
?>
<?php
	$data = array();
	$the_query = new WP_Query(array(
		'post_type'=>'metody-platnosci',
		'tax_query'=>array(array(
			'taxonomy'=>'metody-platnosci_categories',
			'field'=>'slug',
			'terms'=>'karty-platnicze'
		))
	));
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
 array_push($data,array(
	 'title'=>get_the_title(),
	 'permalink'=>get_the_permalink(),
	 'image'=>get_the_post_thumbnail_url(),
	 'excerpt'=>substr(get_the_excerpt(),0,300)
	));

endwhile; endif; ?>
<div id="wiecej" class="payment_methods_pay_card_component">
	<div class="container">
	<div class="d-flex d-sm-flex d-md-flex pay-cards-row d-lg-none card-logos-row row justify-content-center">
					<div class="card-logos-wrapper text-center">
						<?php 
							foreach($data as $key=>$value){
								echo '<a href="'. $value['permalink'] .'"><img class="img-fluid '.( ($key==0)?'active':'' ).'" src="'.$value['image'].'" alt=""></a>';
							}								
						?>
					</div>
				</div>
		<div class="row pay-cards-row justify-content-center">
			<div class="d-none d-sm-none d-md-none d-lg-flex col-12 col-lg-6 text-center text-lg-right pay-card-image">
				<div class="logo-container">
					<div class="card-holder"></div>
				</div>
			</div>
			<div class="col-12 col-lg-6 order-1 order-lg-0 pay-card-description">
				<div class="row">
					<div class="description-wrapper col-md-12 col-lg-9">
						<h2>Obsługuj karty płatnicze z całego świata i niech Twój biznes leci tam, gdzie chcesz!</h2>
						<div class="content">
						<?php
							foreach($data as $key=>$value)	{
								echo '<p class="'.( ($key==0)?'active':'' ).'">'.$value['excerpt'].'</p>';
							}
						?>
						</div>
					</div>
				</div>
				<div class="button-bar row align-content-center align-items-center">
					<div  class="col-auto">
						<a class="btn btn-yellow" href="<?php the_permalink($post_id);?>">Załóż konto</a>
					</div>
					<div class="d-none d-sm-none d-md-none d-lg-block">
						<?php
							foreach($data as $key=>$value){
								echo '<a href ="'. $value['permalink'] .'" class=" link '.( ($key==0)?'active':'' ).'">Więcej o karcie '.$value['title'].'</a>';
							}
						?>
					</div>
				</div>
			</div>
				<div class="d-none d-sm-none d-md-none d-lg-flex card-logos-row row justify-content-center">
					<div class="card-logos-wrapper">
						<?php 
							foreach($data as $key=>$value){
								echo '<a index="'.$key.'" href="'. $value['permalink'] .'"><img index="'.$key.'" class="img-fluid '.( ($key==0)?'active':'' ).'" src="'.$value['image'].'" alt=""></a>';
							}								
						?>
					</div>
				</div>
		</div>
		<div class="col-12 d-none d-lg-block">
			<?php get_template_part('payment_methods_clouds_component/payment_methods_clouds_component');?>
		</div>
	</div>
</div>