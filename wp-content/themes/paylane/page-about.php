<?php 
/**
 * Template Name: O nas
 *
 */

$offer_title = rwmb_meta('about-offer-title');
$offer_copy = rwmb_meta('about-offer-copy'); 
$city1 = rwmb_meta('city-1'); 
$city2 = rwmb_meta('city-2'); 
$city3 = rwmb_meta('city-3'); 
$city4 = rwmb_meta('city-4'); 
$city5 = rwmb_meta('city-5'); 

$cityimg5 = rwmb_meta('city-img-5',array( 'size' => 'medium' ));
$cityimg4 = rwmb_meta('city-img-4',array( 'size' => 'medium' ));
$cityimg3 = rwmb_meta('city-img-3',array( 'size' => 'medium' ));
$cityimg2 = rwmb_meta('city-img-2',array( 'size' => 'medium' ));
$cityimg1 = rwmb_meta('city-img-1',array( 'size' => 'medium' ));

$sec_img = rwmb_meta('sec-img',array('size'=>'large'));
$sec_quote = rwmb_meta('sec-quote');
$sec_quote_author = rwmb_meta('sec-quote-author');

$sec_title = rwmb_meta('sec-title');
$sec_content = rwmb_meta('sec-content');

$offer_btn_1  = rwmb_meta('offer-btn-1');
$offer_btn_2  = rwmb_meta('offer-btn-2');

$offer_btn_1_url = rwmb_meta('offer-btn-1-url');
$offer_btn_2_url = rwmb_meta('offer-btn-2-url');

$sec_btn_1 = rwmb_meta('sec-btn-1');
$sec_btn_1_url = rwmb_meta('sec-btn-1-url');

$mp4 = rwmb_meta('about-mp4'); 
$ogg = rwmb_meta('about-ogg'); 
$webm = rwmb_meta('about-webm');

$ceoquote = rwmb_meta('ceo-quote');
$ceoname = rwmb_meta('name-surname');
$ceoimage = rwmb_meta('image-quote',array( 'size' => 'full' ));

$charity_img = rwmb_meta('charity-image',array('size'=>'full'));

$charity_title = rwmb_meta('charity-title');
$charity_content = rwmb_meta('charity-content');

$charity_btn_1 = rwmb_meta('charity-btn-1');
$charity_btn_1_url = rwmb_meta('charity-press-url');

$charity_btn_2 = rwmb_meta('charity-btn-2');
$charity_btn_2_url = rwmb_meta('charity-blog-url');

$quote_2_img = rwmb_meta('quote-2-image',array('size'=>'full'));

$quote_2_quote = rwmb_meta('quote-2-content');
$quote_2_position = rwmb_meta('quote-2-position');

$about_payment_title = rwmb_meta('about-payment-title');
$about_payment_content = rwmb_meta('about-payment-subtitle');

$option_name = 'paylane';
$register = 'register-link';
$blog = 'blog-link';
$offer = 'offer-link';
$pressroom = 'pressroom-link';
$work = 'work-link';
$email = 'email-link';
$work_url = rwmb_meta( $work, array( 'object_type' => 'setting' ), $option_name );
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$blog_url = rwmb_meta( $blog, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$pressroom_url = rwmb_meta( $pressroom, array( 'object_type' => 'setting' ), $option_name );
$email_url = rwmb_meta( $email, array( 'object_type' => 'setting' ), $option_name );

get_header();?>
<div class="container-fluid about-video">
    <?php if(wp_is_mobile()){?>
        <div class="photo-header" style="background-image:url('<?php the_post_thumbnail_url('full');?>');"></div>
    <?php } else { ?>
        <video id="video-header" class="video-header" autoplay muted>
            <?php if(!empty($mp4)){ ?>
                <source src="<?php echo $mp4;?>" type="video/mp4">
            <?php };?>
            <?php if(!empty($ogg)){ ?>
                <source src="<?php echo $ogg;?>" type="video/ogg">
            <?php };?>
            <?php if(!empty($ogg)){ ?>
                <source src="<?php echo $webm;?>" type="video/webm">
            <?php };?>
        </video>
    <?php };?>
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-8 title"><?php the_title();?></div>
        </div>
        <div class="row justify-content-center contents">
            <div class="col-12 col-sm-12 col-md-8"><?php the_content();?></div>
        </div>
    </div>
    <div class="control-drawer">
                <div class="d-none d-sm-none d-md-flex controls">
                <?php if(!wp_is_mobile()){?>
                        <div id="play-pause" style="background-image:url('<?php echo get_template_directory_uri();?>/about/pause.svg');"></div>
                        <div id="volume-up-down" style="background-image:url('<?php echo get_template_directory_uri();?>/about/muted.svg');"></div>
                <?php }; ?>
                </div>
            <div class="more"><a href="#more">Zobacz co osiągneliśmy<img src="<?php echo get_template_directory_uri();?>/about/chevrons-down.svg"></a></div>
            <div class="d-none d-sm-none d-md-flex social">
                <a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/about/twitter.svg"></a>
                <img class="social-icon" id="facebook-share" src="<?php echo get_template_directory_uri(); ?>/about/facebook.svg">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/about/linked-in.svg"></a>
            </div>
    </div>
            
    <?php if(!wp_is_mobile()){ ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", init());

        function init(){
            var icons = {
                play: "<?php echo get_template_directory_uri();?>/about/play.svg",
                pause: "<?php echo get_template_directory_uri();?>/about/pause.svg",
                volume: "<?php echo get_template_directory_uri();?>/about/volume.svg",
                muted: "<?php echo get_template_directory_uri();?>/about/muted.svg"
            };
            var buttons = {
                play: document.getElementById('play-pause'),
                volume: document.getElementById('volume-up-down'),
            };
            var playerState = {
                loudness: false,
                play: true,
            };
            var player = document.getElementById('video-header');

            function playpause(){
                if(playerState.play == true){
                    player.pause();
                    playerState.play = false,
                    buttons.play.style.backgroundImage = "url(" + icons.play + ")";
                } else {
                    player.play();
                    playerState.play = true,
                    buttons.play.style.backgroundImage = "url(" + icons.pause + ")";
                }
            }
            function volumes(){
                if(playerState.loudness == false){
                    player.volume = 1.0;
                    playerState.loudness = true,
                    buttons.volume.style.backgroundImage = "url(" + icons.volume + ")";
                } else {
                    player.volume = 0.0;
                    playerState.loudness = false,
                    buttons.volume.style.backgroundImage = "url(" + icons.muted + ")";
                }
            }
            buttons.play.addEventListener('click', playpause);
            buttons.volume.addEventListener('click', volumes);
        };
    </script>
    <?php };?>
</div>

<div id="more" class="container container1440 about-offer">
    <div class="row">
        <div class="col-12 text-center title">
            <?php echo $offer_title;?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center copy">
            <?php echo $offer_copy;?>
        </div>
    </div>
    <div class="row justify-content-center cities">
        <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2 text-center city">
            <div class="rect" style="background-image:url(<?php echo $cityimg1['url'];?>)"></div>
            <div><?php echo $city1;?></div>
        </div>
        <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2 text-center city">
            <div class="d-none d-sm-none d-md-block"><?php echo $city2;?></div>
            <div class="rect" style="background-image:url(<?php echo $cityimg2['url'];?>)"></div>
            <div class="d-block d-sm-block d-md-none"><?php echo $city2;?></div>
        </div>
        <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2 text-center city">
            <div class="rect" style="background-image:url(<?php echo $cityimg3['url'];?>)"></div>
            <div><?php echo $city3;?></div>
        </div>
        <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2 text-center city">
            <div class="d-none d-sm-none d-md-block"><?php echo $city4;?></div>
            <div class="rect" style="background-image:url(<?php echo $cityimg4['url'];?>)"></div>
            <div class="d-block d-sm-block d-md-none"><?php echo $city4;?></div>
        </div>
        <div class="d-none d-sm-none d-md-none d-lg-none d-xl-inline-block col-xl-2 text-center city">
            <div class="rect" style="background-image:url(<?php echo $cityimg5['url'];?>)"></div>
            <div><?php echo $city5;?></div>
        </div>
    </div>
    <div class="row button-dock text-center justify-content-center align-items-center">
        <a class="btn btn-fixed-medium btn-blue" href="<?php the_permalink($register_url);?>"><?php echo $offer_btn_1;?></a>
        <span>albo</span>
        <a class="btn btn-fixed-medium btn-yellow" href="<?php the_permalink($offer_url);?>"><?php echo $offer_btn_2;?></a>
    </div>
</div>
<div class="container-fluid security">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 image-holder">
            <div class="separator top separator-gradient--full-width"></div>
                <div class="image" style="background-image:url('<?php echo $sec_img['url'];?>');"></div>
                <div class="overlay"></div>
            <div class="quote">
                <?php echo $sec_quote;?>
                <div class="quote-author"><?php echo $sec_quote_author;?></div>
            </div>
            <div class="shield"><img src="<?php echo get_template_directory_uri();?>/about/security.svg"></div>
            <div class="separator bottom separator-gradient--full-width"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 content-holder">
            <h2 class="title"><?php echo $sec_title;?></h2>
            <p class="content"><?php echo $sec_content;?></p>
            <a class="btn btn-medium btn-blue"href="<?php echo $sec_btn_1_url;?>"><?php echo $sec_btn_1;?></a>
        </div>
    </div>
</div>
<div class="container quote">
    <div class="row justify-content-center ceo-row">
        <div class="col-12 col-sm-12 col-md-6 col-lg-7 text-center text-md-right ceo-content">
            <h1 class="ceo-quote"><?php echo $ceoquote;?></h1>
            <div class="position"><?php echo $ceoname;?></div>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-2 ceo-image">
            <img src="<?php echo $ceoimage['url'];?>" alt="">
        </div>
    </div>
    <div class="separator-gradient--full-width"></div>
</div>
<div class="clients">
    <div class="clients-wave justify-content-center glide"> 
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <?php $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                        if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                            <li class="glide__slide"><?php the_post_thumbnail();?></li>
                        <?php endwhile; endif;?>
                     </ul>
                </div>
            </div>
            <div class="clients-wave justify-content-center glide2"> 
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                <?php 
                     $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                        <li class="glide__slide"><?php the_post_thumbnail();?></li>
                     <?php endwhile; endif;?>
                 </ul>
            </div>
            </div>
            <div class="clients-wave justify-content-center glide3"> 
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                <?php 
                     $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                        <li class="glide__slide"><?php the_post_thumbnail();?></li>
                     <?php endwhile; endif;?>
                 </ul>
            </div>
            </div>
        </div>   
</div>
<div class="container buttons">
    <div class="row">
        <div class="col-sm-12">
            <div class="button-bar center">
                <a class="btn-fixed-medium btn btn-blue" href="<?php the_permalink($register_url);?>">Załoź konto</a>
                <span>albo</span>
                <a class="btn-fixed-medium btn btn-yellow" href="<?php the_permalink($register_url);?>">Wypróbuj</a>
            </div>
        </div>
    </div>
    <div class="separator separator-gradient--full-width"></div>
</div>
<div class="container-fluid charity security">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-6 charity-holder">
            <div class="separator separator-gradient--full-width"></div>
            <img src="<?php echo $charity_img['url'];?>" alt="">
            <div class="separator separator-gradient--full-width"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-6 content-holder">
            <h2 class="title"><?php echo $charity_title;?></h2>
            <?php echo do_shortcode( wpautop( $charity_content ) );?>
            <div class="button-bar left">
                <a class="btn btn-fixed-medium btn-blue" href="<?php the_permalink($pressroom_url);?>"><?php echo $charity_btn_1;?></a>
                <span>albo</span>
                <a class="btn btn-fixed-medium btn-yellow" href="<?php the_permalink($blog_url);?>"><?php echo $charity_btn_2;?></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid about-payment">
    <div class="row justify-content-center">
        <div class="col-12 text-center"><h1 class="title"><?php echo $about_payment_title;?></h1></div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 text-center"><p class="content"><?php echo $about_payment_content;?></p></div>
    </div>
    <div id="payments" class="logo_rotator_component glide4">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <?php $the_query = new WP_Query(array(
                        'posts_per_page'   => -1,
                        'post_type'=>'metody-platnosci',
                        'orderby' => 'rand',
                    ));
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <li class="glide__slide">
                <?php if('' !== get_post()->post_content){?><a href="<?php the_permalink();?>"><?php };?><img class="bankster" src="<?php echo get_the_post_thumbnail_url(); ?>"><?php if('' !== get_post()->post_content){?></a><?php };?>
                    </li>
                <?php endwhile; endif; wp_reset_postdata(); wp_reset_query();?>
            </ul>
            <div class="mask-box"></div>
        </div>
    </div>
</div>
<div class="container quote">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-6 col-lg-7 text-center text-md-right ceo-content">
            <h1 class="ceo-quote"><?php echo $quote_2_quote;?></h1>
            <div class="position"><?php echo $quote_2_position;?></div>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-2 ceo-image">
            <img src="<?php echo $quote_2_img['url'];?>" alt="">
        </div>
    </div>
</div>
<div class="work_single_members_img">
	<div class="container container1440">
		<div class="row justify-content-center text-center">
			<div class="col-12 col-lg-10">
				<h1 class="title">Poznaj ludzi którzy dbają o Twoje pieniądze</h1>	
			</div>
		</div>		
		<div class="row justify-content-center">
            <div class="col-12 col-lg-10 subtitle text-center">Nasz zespół marzeń</div>	
			<div class="members col-12 col-lg-10">
				<div class="row">
					<?php 
					$loop = new WP_Query(array('post_type'=>'nasz-zespol', 'posts_per_page'=>-1, 'orderby'=>"order" ));
					if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="member-img col-12 col-md-6 col-lg-3">
                            <div class="team-member" style="background:url('<?php 
                            $american = rwmb_meta('american')['full_url'];
                            if (!empty($american)){
                                echo $american;
                            } else {
                                the_post_thumbnail_url();
                            }?>');">
								<div class="overlay">
									<div class="content text-left">
										<?php the_content();?>
									</div>
									<?php 
									$facebook = rwmb_meta('url-facebook');
									$twitter = rwmb_meta('url-twitter');
									$linkedin = rwmb_meta('url-linked-in');
									$gitlab = rwmb_meta('url-gitlab');
									$github = rwmb_meta('url-github');
									?>
									<div class="mini-social text-center">
										<?php if(!empty($github)){?>
										<a href="<?php echo $github ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/github.svg" alt="GitHub"></a>
										<?php }?>
										<?php if(!empty($gitlab)){?>
										<a href="<?php echo $gitlab ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/gitlab.svg" alt="GitLab"></a>
										<?php }?>
										<?php if(!empty($facebook)){?>
										<a href="<?php echo $facebook ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/facebook.svg" alt="Facebook"></a>
										<?php }?>
										<?php if(!empty($twitter)){?>
										<a href="<?php echo $twitter ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/twitter.svg" alt="Twitter"></a>
										<?php }?>
										<?php if(!empty($linkedin)){?>
										<a href="<?php echo $linkedin ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/linked-in.svg" alt="Linked In"></a>
										<?php }?>
									</div>
								</div>
							</div>	
							<h2 class="name"><?php the_title();?></h2>
							<p class="position"><?php echo rwmb_meta('textarea_2');?></p>
					</div>
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container czywiesz">
    <div class="row">
        <div class="col-12 separator-gradient--full-width"></div>
    </div>
    <div class="row text-center">
        <h1 class="know-title col-12">Czy wiesz że możesz do nas dołączyć?</h1>
        <h3 class="know-subtitle col-12">Cały czas szukamy zdolnych pionierów niebojących się wyzwań.</h3>
        <h3 class="know-subtitle col-12">Zobacz nasze:</h3>
    </div>
    <div class="button-bar center">
        <a class="btn-fixed-medium btn btn-blue" href="<?php the_permalink($work_url);?>">Oferty pracy</a>
        <span>albo</span>
        <a class="btn-fixed-medium btn btn-yellow" id="contact-over" href="<?php echo $email_url;?>">Napisz do nas</a>
    </div>
</div>
<script>
    var args = {
        autoplay: 3000,
        animationDuration: 750,
        animationTimingFunc: 'ease-in-out',
    type: 'carousel',
    perView: 8,
    hoverpause:false,
    breakpoints: {
        1440: {
        perView: 7
        },
        960: {
        perView: 4
        },
        500: {
        perView: 2
        }
    }
    };
    var args2 = {
    autoplay: 3000,
    animationDuration: 750,
    animationTimingFunc: 'ease-in-out',
    type: 'carousel',
    perView: 8,
    direction: 'rtl',
    hoverpause:false,
    breakpoints: {
        1440: {
        perView: 7
        },
        960: {
        perView: 5
        },
        500: {
        perView: 4
        }
    }
    };
    var glide = new Glide('.glide', args);
    var glide2 = new Glide('.glide2', args2);
    var glide3 = new Glide('.glide3', args);
    var glide4 = new Glide('.glide4', args);

    document.addEventListener("DOMContentLoaded", function(){
        glide.mount();
        glide2.mount();
        glide3.mount();
        glide4.mount();
    });
    document.getElementById('facebook-share').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
    </script>
<?php get_footer('wave');?>