<?php $payments = rwmb_meta('payment-methods-country');
		$currency = rwmb_meta('country-currency');
		$register_btn = rwmb_meta('country-btn-1');
		$register_btn_url = rwmb_meta('country-btn-1-url');
		$register_btn_2 = rwmb_meta('country-btn-2');
		$register_btn_2_url = rwmb_meta('country-btn-2-url');
		$others = rwmb_meta('other-payments');
		$shops = rwmb_meta('popular-shopping');
		$ad_content = rwmb_meta('additional-info');
?>
<div class="countries_single">
    <div class="container-fluid">
		<div class="container container1440">
			<div class="country-information row justify-content-center">
				<div class="country-flag col-12 col-md-3 text-right">
					<div class="flag-img ">
						<img src="<?php the_post_thumbnail_url('full'); ?>" alt="Flaga kraju <?php the_title();?> | PayLane - Płatności elektroniczne" class="img-fluid">
					</div>
					<p class="currency">Jednostka monetarna</p>
					<?php if( !empty($currency) ){
						foreach($currency as $curr){
							echo "<p class='currency-unit'>",$curr,"</p>";
						}
					}
					else
					{
						echo "<p class='currency-unit'>1 Euro(EUR, &euro;)</p>";
					};?>
				</div>
				<div class="country-description col-12 col-md-5">
					<h1 class="title">
						<?php the_title();?>
					</h1>
					<p>
						<?php the_content();?>
					</p>
				</div>
			</div>
		</div>
		<div class="separator-gradient--full-width row"></div>
        <div class="clouds-section row justify-content-center">
			<div class="wave">
				<img src="<?php echo get_template_directory_uri() ?>/assets-countries/wave-grey.svg" alt="Szara fala | PayLane - Płatności elektroniczne" class="img-fluid">
			</div>
			<div class="container container1440">
				<div class="row justify-content-center">
					<div class="popular-payment-methods col-12 col-md-4 d-flex">
						<h2 class="title text-right">Popularne metody płatności</h2>
						<ul>
							<?php 
							foreach ($payments as $payment){
								echo '<li><a href=',get_the_permalink($payment),'>', get_the_title($payment) ,'</a></li>';
							}
							?>
							<?php 
							foreach ($others as $other){
								echo '<li>', $other ,'</li>';
							}
							?>
						</ul>
					</div>
					<?php if(!empty($shops)){?>
					<div class="e-comerce col-12 col-md-4 d-flex ">
						<h2 class="title text-right">Popularne zakupy e&#8209;commerce</h2>
						<ul>
						<?php 
							foreach ($shops as $shop){
								echo '<li>', $shop ,'</li>';
							}
						?>
						</ul>
					</div>
					<?php };?>
				</div>
			</div>
            <div class="cloud left-cloud ">
                <img src="<?php echo get_template_directory_uri() ?>/assets-countries/cloud.svg" alt="Chmura | PayLane - Płatności elektroniczne" class="img-fluid">
            </div>
            <div class="cloud right-cloud">
                <img src="<?php echo get_template_directory_uri() ?>/assets-countries/cloud.svg" alt="Chmura | PayLane - Płatności elektroniczne" class="img-fluid">
            </div>
		</div>		
		<div class="container container1440">
			<?php if(!empty($ad_content)){ ?>
			<div class="aditional-info-section row justify-content-center">
				<div class="col-12 col-md-8">
					<h1 class="title">Dodatkowe informacje handlowe</h1>
					<div class="additional-content">
						<?php 
							echo do_shortcode( wpautop( $ad_content ) );
						?>
					</div>
				</div>
			</div>
			<?php }; ?>
			<div class="btn-section row justify-content-center">
				<div class="col-12 col-md-8 btn-bar">		
					<a href="<?php echo $register_btn_url;?>" class="btn btn-blue btn-medium"><?php if(!empty($register_btn)){echo $register_btn;}else{echo"Załóż konto";}?></a>
					<span>albo</span>
					<a href="<?php echo $register_btn_2_url;?>" class="btn btn-yellow btn-medium"><?php if(!empty($register_btn_2)){echo $register_btn_2;}else{echo"Wypróbuj";}?></a>                
				</div>
			</div>
		</div>
    </div>
</div>
