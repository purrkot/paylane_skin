<?php 
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="overflowed">
    <div class="home_world_range_component container">
        <div class="cloud-left-1 cloud left"><img src="<?php echo get_template_directory_uri();?>/assets-home/icon_clouds_1.svg" alt="Chmura | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza"></div>
        <div class="cloud-left-2 cloud left"><img src="<?php echo get_template_directory_uri();?>/assets-home/icon_clouds_2.svg" alt="Chmura | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza"></div>
        <div class="cloud-right-1 cloud right"><img src="<?php echo get_template_directory_uri();?>/assets-home/icon_clouds_3.svg" alt="Chmura | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza"></div>
        <div class="cloud-right-2 cloud right"><img src="<?php echo get_template_directory_uri();?>/assets-home/icon_clouds_4.svg" alt="Chmura | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza"></div>
        <div class="row">
            <div class="col-12 text-center"><h2>Cały świat w zasięgu Twojego biznesu</h2></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-10 col-sm-10 col-md-8 col-lg-6 text-center"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_walut.svg" alt="Płatności międzynarodowe wielowalutowe | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza"></div>
        </div>
        <div class="row text-row">
            <div class="col-12 text-center"><h4>160 walut do wyboru i 30 najpopularniejszych rodzajów płatności do dyspozycji, to maksimum opcji dla Twojego klienta.</h4></div>
        </div>
        <div class="row button-row">
            <div class="col-12 text-center"><a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-medium">Załóż konto!</a></div>
        </div>
    </div>
</div>
<div class="container1920">
    <?php get_template_part("logo_rotator_component/logo_rotator_component"); ?>
</div>
