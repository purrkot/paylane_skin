<?php 
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$plugins_url = rwmb_meta( $plugins, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>
<div id="plugins" class="home_business_integrations_component">
    <div class="container1920 container">
        <div class="row justify-content-center">
            <div class="col-8 col-sm-8 col-md-6 col-lg-6 image-col justify-content-center icon-holder">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_integracja.svg" alt="Pluginy i integracje | Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 description-col claim-holder">
                <h3>Integracje dostosowane do Twojego biznesu!</h3>
                <p>Stawiamy na elastyczność i komfort wdrożenia. Oferujemy szereg pluginów oraz integracji dla platform e-commerce, dzięki którym implementacja płatności na Twojej stronie zajmie tylko chwilę!</p>
                <div class="button-bar">
                    <a href="<?php the_permalink($register_url);?>" class="btn btn-yellow btn-fixed-big">Załóż konto</a>
                    <span>albo</span>
                    <a href="<?php the_permalink($plugins_url);?>" class="btn btn-blue btn-fixed-big">Pobierz wtyczki</a>
                </div>
                <div class="logo-bar">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-home/logo_prestashop.svg" alt="Pluginy i integracje | Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-home/logo_magento.svg" alt="Pluginy i integracje | Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-home/logo_woocommerce.svg" alt="Pluginy i integracje | Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                </div>
            </div>
        </div>
    </div>
</div>