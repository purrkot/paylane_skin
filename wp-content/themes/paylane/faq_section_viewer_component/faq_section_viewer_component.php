<?php
require_once('faq_menu_walker.php');


function generateNavMenuWidget(){
    $o='<div class="NavMenuWidget">';
    $o.='<div id="devopener"><img src="'.get_template_directory_uri().'/assets-devzone/mobile-chevron.svg" alt="Rozwiń | PayLane - Płatności elektroniczne"></div>';
    $o.=wp_nav_menu( 
        array( 
            'theme_location' => 'faq-menu',
            'menu_class'=>'faq_section_viewer_root',
            'echo'=>false,
            'walker'  => new FaqMenuWalker() //use our custom walker
        ) 
    );
    $o.='</div>';
    return $o;
}


function generatePostView($tmpPage){
    $o='';
    $o.='<div class="drawer-article">';
        $o.='<div class="drawer-article-content">';
            $o.='<h3 id="'.$tmpPage->post_name.'">'.$tmpPage->post_title.'</h3>';
            $o.=wp_filter_nohtml_kses($tmpPage->post_content);
        $o.='</div>';
        $o.='<div class="drawer-article-up text-right">';
            $o.='<a href="#">Do góry <img src="'.get_template_directory_uri().'/assets-faq/up-arrow.svg" alt="Do góry | PayLane - Płatności elektroniczne"></a>';
        $o.='</div>';
    $o.='</div>';
    return $o;
}

?>


<div class="faq_section_viewer_component">
    <div class="container1920 container">
        <div class="row">
            <div class="col col-nav-menu" ss-container>
                <?php echo generateNavMenuWidget(); ?>
            </div>
            <div id="openerfilter"></div>
            <div class="col col-content">
                <?php
                    global $faqIds;
                    if(!is_array($faqIds)){ $faqIds = array(); }
                    foreach ($faqIds as $post_id): 
                        $currentLoopPage = get_post($post_id);
                        echo generatePostView($currentLoopPage);
                    endforeach;
                    ?>
            </div>
        </div>
    </div>
</div>