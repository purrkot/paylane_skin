jQuery(document).ready(function ($) {

function FaqSectionViewerController() {
    this.trunks = $('.faq_section_viewer_root .menu-item-type-taxonomy');
    this.initMenu();
    this.initMenuDrawer();
}

FaqSectionViewerController.prototype.initMenuDrawer = function(){
    //activate menu actions
    if($('#devopener').length){
        $('#devopener').on('click',function(e){
            console.log('click');
            $('.faq_section_viewer_component').toggleClass('nav-shown');
            $('.faq_header_component ').toggleClass('nav-shown');
            $('.faq_call_us_component ').toggleClass('nav-shown');
            $('.footer_component.faq ').toggleClass('nav-shown');
        })
    }
}

FaqSectionViewerController.prototype.initMenu = function () {
     var self = this;
     for (let i = 0; i < this.trunks.length; i++) {
         //generate .count-circle numbers
         let countCircle = $(self.trunks[i]).find('.sub-menu .menu-item').length;
         $(self.trunks[i]).find('.count-circle').append(countCircle);
     }
}

    
    if($('.faq_section_viewer_component').length){
    let faqSectionViewerController = new FaqSectionViewerController();
    }
});