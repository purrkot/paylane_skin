    <div id="payments" class="logo_rotator_component glide">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <?php $the_query = new WP_Query(array(
                        'posts_per_page'   => -1,
                        'post_type'=>'metody-platnosci',
                        'orderby' => 'rand',
                    ));
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <li class="glide__slide">
                        <?php if( '' !== get_post()->post_content ){?><a href="<?php the_permalink();?>"><?php }?><img class="bankster" src="<?php the_post_thumbnail_url('medium'); ?>"><?php if( '' !== get_post()->post_content ){?></a><?php }?>
                    </li>
                <?php endwhile; endif; wp_reset_postdata(); wp_reset_query();?>
            </ul>
            <div class="mask-box"></div>
        </div>
    </div>
    <script>
    var glide = new Glide('.glide', {
        autoplay: 3000,
        animationDuration: 750,
        animationTimingFunc: 'ease-in-out',
    type: 'carousel',
    perView: 7,
    breakpoints: {
        1440: {
        perView: 5
        },
        960: {
        perView: 3
        },
        500: {
        perView: 2
        }
    }
    });
    glide.mount();
    </script>