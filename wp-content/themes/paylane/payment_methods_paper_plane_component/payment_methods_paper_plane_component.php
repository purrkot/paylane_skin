<?php   
        $register_id = 'register-link';
		$contact_id = 'contact-link';
		$payments_id = 'payments-link';
		$option_name = 'paylane';
		$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
		$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );	
?>
<div class="payment_methods_paper_plane_component">
	<div class="container">
		<div class="row paper-plane-row justify-content-center">
			<div class="col-12 d-lg-none justify-content-center d-flex">
				<div class="paper-plane-image">	</div>
			</div>				
			<div class="col-12  paper-plane-description">
							
				<div class="row">									
					<div class="description-wrapper col-md-7 col-lg-6 col-xl-6">
						<h2 class="first-title">Szybkie przelewy to metoda, którą Twój klient łapie w lot!</h2>						
					</div>
					<div class="col-12">
						<div class="row justify-content-center">						
							<div class="col-12 col-md-auto">
								<div class="row justify-content-center">
									<div class="text-wrapper col-md-7 col-xl-6 d-sm-none">Szybkie przelewy elektroniczne to jedna z najpopularniejszych form płatności online w Polsce. Formularz płatności zostaje wypełniony automatycznie, a kupujący może potwierdzić płatność poprzez logowanie w swoim banku.</div>
								</div>
								<div class="buttons-row row">
									<div class="paper-plane-image show-absolute d-none d-lg-block">	</div>
									<div  class="col-auto">
										<a class="btn btn-blue" href="<?php the_permalink($post_id);?>">Wypróbuj</a>
									</div>
									<div class="col-12 col-sm-auto col-break d-none d-sm-flex">
										albo
									</div>
									<div  class="col-auto">
										<a class="btn btn-yellow" href="<?php the_permalink($post_id);?>">Załóż konto!</a>
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<div class="col-12 paper-plane-text-bottom">
				<div class="row justify-content-center">
					<div class="text-wrapper col-md-7 col-xl-6 d-none d-sm-flex">Szybkie przelewy elektroniczne to jedna z najpopularniejszych form płatności online w Polsce. Formularz płatności zostaje wypełniony automatycznie, a kupujący może potwierdzić płatność poprzez logowanie w swoim banku.</div>
				</div>
			</div>
		</div>
	</div>
</div>