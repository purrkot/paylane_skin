module.exports = function(grunt) {

    grunt.initConfig({
      sass: {
        dist: {
          options: {
            style: 'expanded'
          },
          files: {
            'style.css': 'main.scss',
          }
        }
      },
      autoprefixer:{
        dist:{
          files:{
            'style.css':'style.css'
          }
        }
      },
      watch: {
        scripts: {
          files: [
            '*.scss',
            '*/*.scss'
          ],
          tasks: ['sass'],
        }
      }
    });
    
    
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.registerTask('default', ['sass','autoprefixer']);
    };
    