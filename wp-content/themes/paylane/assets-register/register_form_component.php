<?php

class RegisterFormComponent{
    public function show(){
        $output = array();
        array_push($output,'<div class="register_form_component">');
        array_push($output,'    <div class="container1440 container">');
        array_push($output,'        <div class="row title-column">');
        array_push($output,'            <div class="col-4 text-center">');
        array_push($output,'                <img src="'.get_template_directory_uri().'/assets-register/cloud-left.svg">');
        array_push($output,'            </div>');
        array_push($output,'            <div class="col-4 text-center">');
        array_push($output,'                <h2>Cały świat w zasięgu<br> Twojego biznesu!</h2>');
        array_push($output,'            </div>');
        array_push($output,'            <div class="col-4 text-center">');
        array_push($output,'                <img src="'.get_template_directory_uri().'/assets-register/cloud-right.svg">');
        array_push($output,'            </div>');
        array_push($output,'        </div>');
        array_push($output,'        <div class="row middle-column">');
        array_push($output,'            <div class="col-4 text-center recommendation-box">');
        array_push($output,'                <img class="img-fluid" src="'.get_template_directory_uri().'/assets-home/face.jpg">');
        array_push($output,                 "Dziękuję za waszą znakomitą firmę, usługi oraz - przede wszystkim - za wspaniałą obsługę klienta.");
        array_push($output,                 "Mateusz Chochla");
        array_push($output,                 "Allegro.pl");
        array_push($output,'            </div>');
        array_push($output,'            <div class="col-4 text-center">');
        array_push($output,'                <div class="login-box">');
        array_push($output,'                    <h3>Zarejestruj się</h3>');
        array_push($output,'                    <form>');
        array_push($output,'                        <div class="form-group">');
        array_push($output,'                            <div class="input-group">');
        array_push($output,'                                <div class="input-group-prepend">');
        array_push($output,'                                <span class="input-group-text" id="inputGroupPrepend">@</span>');
        array_push($output,'                                </div>');
        array_push($output,'                                <input type="text"  placeholder="Imię i Nazwisko" class="form-control">');
        array_push($output,'                            </div>');
        array_push($output,'                        </div>');
        array_push($output,'                        <div class="form-group">');
        array_push($output,'                            <div class="input-group">');
        array_push($output,'                                <div class="input-group-prepend">');
        array_push($output,'                                <span class="input-group-text" id="inputGroupPrepend">@</span>');
        array_push($output,'                                </div>');
        array_push($output,'                                <input type="email" placeholder="E-mail" class="form-control" >');
        array_push($output,'                            </div>');
        array_push($output,'                        </div>');
        array_push($output,'                        <div class="form-group">');
        array_push($output,'                            Mam kod promocyjny');
        array_push($output,'                        </div>');
        array_push($output,'                        <div class="form-group">');
        array_push($output,'                             <button type="submit" class="btn btn-primary">Zarejestruj się</button>');
        array_push($output,'                        </div>');
        array_push($output,'                    </form>');
        array_push($output,'                    <img class="rocket-overlay" src="'.get_template_directory_uri().'/assets-register/rocket.svg">');
        array_push($output,'                </div>');
        array_push($output,'            </div>');
        array_push($output,'        </div>');
        array_push($output,'        <div class="row">');
        array_push($output,'            <div class="logo-block">');
        array_push($output,'                <div class="col-12">');
        array_push($output,'                    <h5>Zaufali nam</h5>');
        array_push($output,'                </div>');
        array_push($output,'                <div class="col-12">');
        array_push($output,'                    <div class="row justify-content-between">');
        array_push($output,'                        <div class="col-lg-3">');
        array_push($output,'                            <img class="img-fluid" src="'.get_template_directory_uri().'/assets-home/corsair_logo_white.svg">');
        array_push($output,'                        </div>');     
        array_push($output,'                        <div class="col-lg-3">');   
        array_push($output,'                            <img class="img-fluid" src="'.get_template_directory_uri().'/assets-home/gigabyte_logo_white.svg">');
        array_push($output,'                        </div>');     
        array_push($output,'                        <div class="col-lg-3">');
        array_push($output,'                            <img class="img-fluid" src="'.get_template_directory_uri().'/assets-home/brembo_logo_white.svg">');
        array_push($output,'                        </div>');     
        array_push($output,'                        <div class="col-lg-3">');
        array_push($output,'                            <img class="img-fluid" src="'.get_template_directory_uri().'/assets-home/zendesk_logo_white.svg">');
        array_push($output,'                        </div>');
        array_push($output,'                    </div>');
        array_push($output,'                </div>');
        array_push($output,'            </div>');
        array_push($output,'        </div>');
        array_push($output,'    </div>');
        array_push($output,'</div>');
        return implode('',$output);
    }
}

?>