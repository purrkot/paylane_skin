<div class="work_single_job_descryption">
	<div class="container container1440">
		<div class="row justify-content-center text-center">
			<div class="col-12 col-lg-8">
				<h1 class="job-title"><?php the_title();?></h1>
				<p class="job-location"><?php $terms = get_the_terms( $post->ID, 'miasta' ); $term = array_pop($terms); echo $term->name;?></p>
			</div>
		</div>
		<?php get_template_part('full_width_stripes_grey/full_width_stripes_grey');?>
		<div class="job-details-row row justify-content-center text-center">
			<div class="col-12 col-lg-6">
				<h2 >Kogo szukamy?</h2>
				<h3>Umiejętności</h3>
				<p class="skills">
					<?php $skills = rwmb_meta('work-skills');
                        	foreach($skills as $skill){
                            	echo $skill,",&nbsp;";
					};?>
				</p>
				<div class="job-details">
					<?php the_content();?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="btn-bar col-12">
				<a href="mailto:<?php echo rwmb_meta('work-email');?>?subject=<?php the_title();?>"  class="btn btn-blue btn-big">Aplikuj</a>
				<a "mailto:<?php echo rwmb_meta('work-email');?>?subject=<?php the_title();?>"  class="btn btn-yellow btn-big">Napisz do nas</a>
            </div>
		</div>
	</div>
</div>
