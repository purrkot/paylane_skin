<!DOCTYPE html>
<html <?php language_attributes()?> <?php body_class(); ?>>
<head>
    <?php echo get_template_part("header_meta");?>
    <?php wp_head() ?>
</head>
<body <?php body_class(); ?>>
    <?php echo get_template_part("facebook-sdk");?>
    <?php if (!wp_is_mobile()) {
            echo get_template_part("top_menu_component/top_menu_dziubek");
        } else {
            echo get_template_part("top_menu_component/top_menu_component");
        }?>
    
    <?php 
        if (is_page_template('page-main.php')){
            echo "<div class='main-body'>";
        }
        else if (is_page_template('page-platnosci.php')){
            echo "<div class='overflowed'>";
        }
        else if(is_admin_bar_showing()) {
            echo "<div class='main-body admin-corrector'>";
        } else {
            echo "<div class='main-body corrector'>";
        }
    ;?>