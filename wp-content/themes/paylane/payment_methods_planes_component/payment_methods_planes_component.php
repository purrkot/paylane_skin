<div class="payment_methods_planes_component">
	<div class="container">
		<div class="col-12 clouds-row">
			<?php get_template_part('payment_methods_clouds_component/payment_methods_clouds_component');?>
		</div>		
		<div class="row justify-content-center">
			<div class="plane left col-sm-12 col-md-12 col-lg-5 text-center text-sm-center text-md-center text-lg-left"><img src="<?php echo get_template_directory_uri();?>/assets-platnosci/plane1.svg"></div>
			<div class="plane d-none d-sm-none d-md-none d-lg-block right col-sm-12 col-md-6 col-lg-5"><img src="<?php echo get_template_directory_uri();?>/assets-platnosci/plane2.svg"></div>
		</div>
	</div>
</div>
