<div class="payment_methods_banks_logos_component">
	<div class="container container1920">
		<div class="row logos-wrapper text-center justify-content-center">
			<div class="col-12 col-md-7 col-xl-6">
				<div class="row">
				<?php $args = array(
                        'post_type' =>  'metody-platnosci',
                        'posts_per_page'=>-1,
                        'orderby' =>  'rand',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'metody-platnosci_categories',
                                'field'    => 'slug',
                                'terms'    => array( 'banki'),
                            ),
                        ),
                    );
                    $payment = new WP_Query($args);
				    if ($payment->have_posts() ) : while ($payment->have_posts() ) : $payment->the_post();?>
					<div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3 logo"><img src="<?php the_post_thumbnail_url('full');?>"></div>
				<?php endwhile; endif; ?>		
			</div>
		</div>
	</div>
</div>