<div class="work_single_members_img">
	<div class="container container1440">
		<div class="row justify-content-center text-center">
			<div class="col-12 col-lg-6">
				<h1 class="slogan">PayLane to prężnie działający biznes i zespół, w którym rozwijasz się i pracujesz z przyjemnością.</h1>	
			</div>
		</div>		
		<div class="row justify-content-center">	
			<div class="members col-12 col-lg-10">
				<div class="row">
					<?php 
					$loop = new WP_Query(array('post_type'=>'nasz-zespol', 'posts_per_page'=>'4' ));
					if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="member-img col-12 col-md-6 col-lg-3">
							<div class="team-member" style="background:url('<?php the_post_thumbnail_url();?>');">
								<div class="overlay">
									<div class="content text-left">
										<?php the_content();?>
									</div>
									<?php 
									$facebook = rwmb_meta('url-facebook');
									$twitter = rwmb_meta('url-twitter');
									$linkedin = rwmb_meta('url-linked-in');
									$gitlab = rwmb_meta('url-gitlab');
									$github = rwmb_meta('url-github');
									?>
									<div class="mini-social text-center">
										<?php if(!empty($github)){?>
										<a href="<?php echo $github ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/github.svg" alt="GitHub"></a>
										<?php }?>
										<?php if(!empty($gitlab)){?>
										<a href="<?php echo $gitlab ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/gitlab.svg" alt="GitLab"></a>
										<?php }?>
										<?php if(!empty($facebook)){?>
										<a href="<?php echo $facebook ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/facebook.svg" alt="Facebook"></a>
										<?php }?>
										<?php if(!empty($twitter)){?>
										<a href="<?php echo $twitter ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/twitter.svg" alt="Twitter"></a>
										<?php }?>
										<?php if(!empty($linkedin)){?>
										<a href="<?php echo $linkedin ;?>"><img src="<?php echo get_template_directory_uri();?>/assets-social/linked-in.svg" alt="Linked In"></a>
										<?php }?>
									</div>
								</div>
							</div>	
							<h2 class="name"><?php the_title();?></h2>
							<p class="position"><?php echo rwmb_meta('textarea_2');?></p>
					</div>
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
</div>