<?php 
/**
 * Template Name: metody platnosci
 *
 */
get_header('arrow');
	get_template_part('payment_methods_rocket_component/payment_methods_rocket_component');
	get_template_part('payment_methods_planes_component/payment_methods_planes_component');
	get_template_part('payment_methods_pay_card_component/payment_methods_pay_card_component');
	get_template_part('payment_methods_paper_plane_component/payment_methods_paper_plane_component');
	get_template_part('payment_methods_banks_logos_component/payment_methods_banks_logos_component');
	get_template_part('payment_methods_launchpad_component/payment_methods_launchpad_component');
get_footer('wave');
?>