<div class="container1440 container">
    <div class="ebook_preview_component">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8">
                <div class="row">
                    <div class="col-12 col-md-4 image-col">
                        <img class="img-fluid" src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php the_title();?> | PayLane - Płatności elektroniczne" >
                    </div>
                    <div class="col-12 col-md-8 description-col d-flex flex-column">
						<div>
							<span class="author"><?php the_author();echo ", "; the_date();?></span>
							<h3>
								<?php the_title();?>
							</h3>
								<?php the_content();?>
							<div class="btn-row">
								<a href="<?php echo get_post_permalink(); ?>" class="btn btn-small btn-blue">Czytaj więcej</a>
							</div>
						</div>
						<div class="social-btn-row">
								<a class="social-icon" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets-blog/twitter.svg" alt="Twitter | PayLane - Płatności elektroniczne"></a>
								<a class="social-icon" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets-blog/facebook.svg" alt="Facebook | PayLane - Płatności elektroniczne"></a>
								<a class="social-icon" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets-blog/linked-in.svg" alt="Linked In | PayLane - Płatności elektroniczne"></a>						
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
