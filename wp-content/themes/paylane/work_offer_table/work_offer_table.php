<?php 
$about = 'about-link';
$option_name = 'paylane';
$about_link = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
$email = 'email-link';
$email_link = rwmb_meta( $email, array( 'object_type' => 'setting' ), $option_name );?>
<div class="work_offer_table">
    <div class="container container1440">
        <?php get_template_part('full_width_stripes_grey/full_width_stripes_grey');?>
        <div class="table-header-row row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <h1 class="table-title">Zobacz na jakie stanowiska prowadzimy rekrutacje</h1>
            </div>
        </div>
        <div class="table-body-row row justify-content-center">
            <div class="col-12 col-lg-10">  
                <div class="table-row row table-header">
                    <div class="col-4 col-sm-2 col-lg-2">Miasto</div>
                    <div class="col-8 col-sm-4 col-lg-3">Stanowisko</div>
                    <div class="col-sm-6 col-lg-7 d-none d-sm-flex">Umiejętności</div>
                </div>
                <?php $loop = new WP_Query(array('post_type'=>'oferty-pracy', 'posts_per_page'=>-1, 'orderby'=>'date' ));
				if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>								
                    <div class="table-row row">
                        <div class="col-4 col-sm-2 col-lg-2 bolded cell"><?php $terms = get_the_terms( $post->ID, 'miasta' ); $term = array_pop($terms); echo $term->name;?></div>
                        <div class="col-8 col-sm-4 col-lg-3 cell"><?php the_title();?></div>
                        <div class="col-12 d-sm-none bolded hide-label">Umiejętności:</div>
                        <div class="col-12 col-sm-6 col-lg-3 cell">
                            <?php $skills = rwmb_meta('work-skills');
                            foreach($skills as $skill){
                                echo "<span>",$skill,",&nbsp;</span>";
                            };?>
                        </div>
                        <div class="col-12 col-lg-4 btn-bar">
                            <a href="mailto:<?php echo rwmb_meta('work-email');?>?subject=<?php the_title();?>" class="btn btn-blue btn-medium">
                                <?php if(!empty(rwmb_meta('button-name'))){echo rwmb_meta('button-name');}else{echo "Aplikuj";};?>
                            </a>
                            <a href="<?php the_permalink();?>" class="btn btn-transparent btn-medium">Dowiedz się więcej</a>
                        </div>
                    </div>
                <?php endwhile; endif;?>
            </div>
        </div>
        <div class="action-row row justify-content-center">
			<div class="col-12 text-center">
				<h1 class="action-title">Wyślij nam swoje CV i razem twórzmy nową jakość w płatnościach online</h1>
			</div>
            <div class="btn-bar col-12 d-flex justify-content-center">
				<a href="<?php the_permalink($about_link);?>" class="btn btn-blue btn-big">Poznaj zespół</a>
				<span>albo</span>
				<a href="mailto:<?php echo $email_link;?>" class="btn btn-yellow btn-big">Napisz do nas</a>
            </div>
        </div>
    </div>
</div>
