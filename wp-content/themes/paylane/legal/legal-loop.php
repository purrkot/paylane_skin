<?php 
$devzone = 'devzone-link';
$knowledge = 'knowledge-link';
$privacy = 'privacy-link';
$register = 'register-link';
$offer = 'offer-link';
$captcha = 'captcha';
$option_name = 'paylane';
$post_id = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$post_id_2 = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$privacy_url = rwmb_meta( $privacy, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$knowledge_ar = rwmb_meta( $knowledge, array( 'object_type' => 'setting' ), $option_name );
$site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
$category_id = get_cat_ID( $knowledge_ar->name );
$category_link = get_category_link( $category_id );
$args = array(
    'posts_per_page' =>  -1,
    'post_type' => 'legal',
);
$counter = 0;
$legal = new WP_Query($args);
?>
<div class="container legal-loop">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-10">
            <h1>Dokumenty <br>prawne</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-6 col-lg-5 box">
            <div class="holder">
            <?php 
                if ($legal->have_posts() ) : while ($legal->have_posts() ) : $legal->the_post();
                $type = rwmb_meta('legal_file');
                $even = false;
                $counter++;
                if (get_the_ID() % 2 == 0) { /* use modulus operator here to determine if id divided by 2 is a whole numer ;-) */
                    $even = true;
                }
                if (!$even){
                ?>
                    <div>
                        <?php if(!empty($type)){?>
                            <a class="small-payment" href="<?php echo $type;?>" download>
                            <img src="<?php echo get_template_directory_uri();?>/assets-legal/download.svg" alt="PayLane - Ściągnij plik">
                            <?php } else {?>
                            <a class="small-payment" href="<?php the_permalink();?>">
                            <img src="<?php echo get_template_directory_uri();?>/assets-legal/link.svg" alt="PayLane - Przejdź do strony">
                            <?php };?>
                            <span><?php the_title();?></span>
                        </a>
                    </div>

                <?php } endwhile; endif;?>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-5 box">
            <img class="lex" src="<?php echo get_template_directory_uri();?>/assets-legal/lex.svg" alt="">
            <img class="cloud-right" src="<?php echo get_template_directory_uri();?>/assets-legal/cloud-right.svg" alt="">
            <div class="holder">
            <?php 
                    if ($legal->have_posts() ) : while ($legal->have_posts() ) : $legal->the_post();
                    $type2 = rwmb_meta('legal_file');
                    $even = false;
                    $counter++;
                    if (get_the_ID() % 2 == 0) { /* use modulus operator here to determine if id divided by 2 is a whole numer ;-)*/
                        $even = true;
                    }
                    if ($even){
                    ?>
                    <div>
                        <?php if(!empty($type2)){?>
                            <a class="small-payment" href="<?php echo $type2;?>" download>
                                <img src="<?php echo get_template_directory_uri();?>/assets-legal/download.svg" alt="Ściągnij plik">
                            <?php } else {?>
                            <a class="small-payment" href="<?php the_permalink();?>">
                                <img src="<?php echo get_template_directory_uri();?>/assets-legal/link.svg" alt="Przejdź do strony">
                            <?php };?>
                            <span><?php the_title();?></span>
                        </a>
                    </div>
                <?php } endwhile; endif; ?>
            </div>
        </div>
    </div>
    <div class="row justify-content-center contact-boxes">
        <div class="col-12 col-sm-12 col-md-6 col-lg-5 sales-wrapper text-center">
            <div class="subtitle">Masz pytania? Zostaw swój numer <br>a oddzwonimy.</div>
            <form action="" class="sales-form text-center">
                <?php echo get_template_part( 'register_form_component/phone_thanks' );?>
                <div class="phone-icon">
                    <input type="tel" class="phone" placeholder="Numer telefonu">
                </div>
                <p class="text-center" >Akceptuję <a href="<?php the_permalink($privacy_url);?>">politykę prywatności</a></p>
                <input type="hidden" class="formtype-input" value="sales-contact">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key;?>"></div>
                <button type="submit" class="submit submit-button" value="Dodaj numer telefonu">Dodaj numer telefonu</button>
            </form>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-5 sales-wrapper text-center">
            <div class="subtitle">Skorzystaj z naszej <br>bazy wiedzy</div>
            <div class="dev-book">
                <a href="<?php the_permalink($devzone_url);?>" class="submit cpu">Devzone - strefa developera</a>
                    <div class="middle-text">albo</div>
                <a href="<?php echo $category_link;?>" class="submit book-white">Księga wiedzy</a>
            </div>
        </div>
    </div>
    <img class="cloud-left" src="<?php echo get_template_directory_uri();?>/assets-legal/cloud-left.svg" alt="">
</div>
<div class="wave-holder">
        <img class="wave-grey" src="<?php echo get_template_directory_uri();?>/assets-legal/wave-grey.svg" alt="">
</div>
<div class="container social-icons legal-loop">
    <div class="row">
            <div class="col-12 social-wrapper">
                <a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets-blog/twitter.svg" alt="Twitter"></a>
                    <img class="social-icon" id="facebook-share-<?php the_ID();?>" src="<?php echo get_template_directory_uri();?>/assets-blog/facebook.svg" alt="Facebook">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets-blog/linked-in.svg" alt="Linked-in"></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="separator-gradient--full-width"></div>
            </div>
        </div>
        <div class="row call-to-action">
            <div class="col-sm-12 sol-md-6 col-lg-6 col-xl-6 text-center">
                <h2>Poznaj szerzej naszą ofertę</h2>
            </div>
            <div class="col-sm-12 sol-md-6 col-lg-6 col-xl-6 text-center button-bar">
                <a class="btn btn-blue btn-fixed-medium" href="<?php the_permalink($post_id);?>">Załóź konto</a>
                    <span>albo</span>
                <a class="btn btn-yellow btn-fixed-medium" href="<?php the_permalink($post_id_2);?>">Dowiedz się więcej</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="separator-gradient--full-width"></div>
            </div>
        </div>
</div>
<script>
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '964421610270873',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>
