<div class="container-fluid about-video">
<?php $mp4 = rwmb_meta('about-mp4'); $ogg = rwmb_meta('about-ogg'); $webm = rwmb_meta('about-webm');?>
    <video id="video-header" class="video-header" autoplay muted>
        <?php if(!empty($mp4)){ ?>
            <source src="<?php echo $mp4;?>" type="video/mp4">
        <?php };?>
        <?php if(!empty($ogg)){ ?>
            <source src="<?php echo $ogg;?>" type="video/ogg">
        <?php };?>
        <?php if(!empty($ogg)){ ?>
            <source src="<?php echo $webm;?>" type="video/webm">
        <?php };?>
    </video>
</div>