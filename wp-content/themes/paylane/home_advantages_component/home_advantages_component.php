<?php 
/**
 * Metaboxes for backend rendering
 */
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>

<div class="home_advantages_component">
            <div class="container1920 container">
                <div class="row justify-content-center align-content-center align-items-center row-benefits">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-content-center align-items-center">

                            <div class="d-block d-sm-block d-md-none d-lg-none d-xl-none col-12 col-sm-12 col-md-4 text-right text-md-center icon">
                                <img class="img-fluid " src="<?php echo get_template_directory_uri(); ?>/assets-home/secure-form.svg" alt="PayLane - Płatności elektroniczne">
                            </div>


                            <div class="col-12 col-sm-12 col-md-7 text-right">
                                <h4>Secure form</h4>
                                <p>Przejrzysty i łatwy w obsłudze formularz przenosi Twojego klienta prosto na stronę, skąd błyskawicznie może dokonać płatności.</p>
                            </div>


                            <div class="d-none d-sm-none d-md-block col-12 col-sm-12 col-md-4 text-center">
                                <img class="img-fluid " src="<?php echo get_template_directory_uri(); ?>/assets-home/secure-form.svg" alt="PayLane - Płatności elektroniczne">
                            </div>


                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-content-center align-items-center">

                            <div class="col-12 col-sm-12 col-md-4 text-left text-md-center icon">
                                <img class="img-fluid " src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_coffe.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                            <div class="col-12 col-sm-12 col-md-7 text-left">
                                <h4>Komfort dla Twojego klienta</h4>
                                <p>Umożliwiamy Twojemu klientowi płacić szybko i wygodnie!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center align-content-center align-items-center row-benefits">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-content-center align-items-center">

                            <div class="d-block d-sm-block d-md-none d-lg-none d-xl-none col-12 col-sm-12 col-md-4 text-right icon">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_armchair.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                            <div class="col-12 col-sm-12 col-md-7 text-right">
                                <h4>Dostosowane do Ciebie</h4>
                                <p>Mamy rozwiązania dla każdego, bez względu na skalę biznesu czy profil działalności!</p>
                            </div>

                            <div class="d-none d-sm-none d-md-block col-12 col-sm-12 col-md-4 text-center">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_armchair.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-content-center align-items-center">

                            <div class="col-12 col-sm-12 col-md-4 text-left text-md-center icon">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_sliders.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                            <div class="col-12 col-sm-12 col-md-7 text-left">
                                <h4>Pełna kontrola</h4>
                                <p>Oddajemy w Twoje ręce szereg narzędzi administatorskich, dzięki którym zawsze będziesz miał pełną kontrolę nad swoimi płatnościami.</p>
                            </div>

                        </div>
                    </div>
                </div>  
                <div class="row align-content-center justify-content-center align-items-center row-benefits">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-content-center align-items-center">

                        <div class="d-block d-sm-block d-md-none d-lg-none d-xl-none col-12 col-sm-12 col-md-4 text-right text-md-center icon">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_360.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                            <div class="col-12 col-sm-12 col-md-7 text-right">
                                <h4>Wszechstronne rozwiązania</h4>
                                <p>Zapewniamy pełną obsługę płatności, dlatego oprócz nowoczesnych rozwiązań e-commerce obsługujemy również tradycyjne jak i nietypowe metody płatności.</p>
                            </div>

                            <div class="d-none d-sm-none d-md-block col-12 col-sm-12 col-md-4 text-center">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_360.svg" alt="PayLane - Płatności elektroniczne">
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 benefit-container">
                        <div class="row align-content-center justify-items-center justify-content-center align-items-center">
                            <div class="col-12 col-sm-12 col-md-4 text-left text-md-center icon">
                                <img class="img-fluid " src="<?php echo get_template_directory_uri(); ?>/assets-home/icon_shield.svg" alt="PayLane - Płatności elektroniczne">
                            </div>
                            <div class="col-12 col-sm-12 col-md-7 text-left">
                                <h4>Jesteśmy z Tobą!</h4>
                                <p>Stawiamy na bezpieczeństwo i jakość obsługi. Śmiało, odezwij się do nas!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-content-center justify-content-center align-items-center">
                    <div class="col-12">
                        <div class="row align-content-center justify-content-center align-items-center">
                            <div class="col-lg-6 text-center">
                                <h2>Odkryj naszą pełną ofertę, zarejestruj się i wybierz płatności skrojone do Twoich potrzeb!</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row justify-content-center align-content-center">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center button-bar center">
                                <a href="<?php the_permalink($offer_url);?>" class="btn btn-blue btn-fixed-big">Dowiedz się więcej</a><span>albo</span> <a href="<?php the_permalink($register_url);?>" class="btn btn-yellow btn-fixed-big">Załóż konto!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid pozdrowienia">
            <div class="row">
                <div class="wrapper">
                    
                    <div class="greet-wrapper text-center text-md-right">
                        <a href="<?php the_permalink($about_url);?>">
                            <h1 class="greetings">Pozdrowienia z Gdańska</h1>
                            <h3 class="more">Dowiedz się o nas więcej</h3>
                        </a>
                    </div>
                    <div class="arrow">
                        <a href="<?php the_permalink($about_url);?>">
                            <img src="<?php echo get_template_directory_uri();?>/assets-home/arrow.svg" alt="Sztrzałka | PayLane - Płatności elektroniczne">
                        </a>
                    </div>
                    
                </div>
                <img class="pozdro" src="<?php echo get_template_directory_uri(); ?>/assets-home/wave_no_shadow.svg" alt="Zdjęcie żurawia Gdańskiego | PayLane - Płatności elektroniczne">
            </div>
            <div class="row">
                <div class="gdansk" style="background:url('<?php the_post_thumbnail_url('footer-feature');?>');"></div>
            </div>
        </div>