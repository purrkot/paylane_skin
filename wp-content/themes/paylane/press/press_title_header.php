<?php 
/**
 * Only use component inside the pressroom-single post loop
 * Don't render component outside the pressroom-single post loop as it will break apart
 */
?>

<div class="press_header container">
    <div class="row">
        <div class="col-12 text-center">
            <div class="container">
                        <h2 class="title"><?php the_title();?></h2>
                </div>  
            </div>
        </div>
    <div class="row">
        <div class="col-12 author-wrapper text-center justify-content-center">
            <div class="date"><?php the_date();?></div>
            <div class="name"><?php the_author();?></div>
        </div>
    </div>  
</div>
