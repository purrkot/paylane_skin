<?php

/**
 * Page metabox buttons for download and boxes for printing data
 */
$phone = rwmb_meta('press-phone');
$mail = rwmb_meta('press-mail');
$name = rwmb_meta('press-name');
$group_values = rwmb_meta( 'press-group' );

?>
<div class="press_submenu">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-lg-10 justify-content-center text-center download">
            <div class="press">Do pobrania:</div>
            <?php 
                if ( ! empty( $group_values ) ) {
                    foreach ( $group_values as $group_value ) {
                        echo '<a href="',$group_value['press-file'],'" class="btn btn-blue btn-small" download>',$group_value['press-button'],'</a>';
                    }
                }
            ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-lg-10 separator-gradient--full-width"></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-lg-10 justify-content-center text-center">
                <img class="mailer" src="<?php echo get_template_directory_uri();?>/press/mail.svg" alt="">
                <div class="press">Kontakt dla mediów:</div><div class="press"><?php echo $name;?></div><div class="press"><?php echo $mail;?></div><div class="press"><?php echo $phone;?></div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-lg-10 separator-gradient--full-width"></div>
        </div>
    </div>
</div>
