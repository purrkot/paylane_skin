<div class="legal_single_component">
	<div class="container container1920">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 main-content">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-10">
						<div class="row">
							<div class="col-12 main-title">
								<h1 >
									<?php the_title();?>
								</h1>
							</div>	
						</div>
						<div class="row">
							<div class="col-12 col-lg-3 d-none d-lg-block">
								<div id="goback" class="btn-back">
									<div class="chevrons-left"> </div>
									<div class="title">Wstecz</div>
								</div>
							</div>
							<div class="col-12 col-lg-9 right-content">
								<?php the_content();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(){
        var backward = document.getElementById('goback');
        backward.addEventListener('click', function(){
            window.history.back();
        });
    });
</script>