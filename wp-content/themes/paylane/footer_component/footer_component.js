jQuery(document).ready(function ($) {
    let FooterComponent = function(){
        var self=this;
        self.valid = false;
    
        //init events
        $('.footer-sign-up-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData();
        });

        $('#footer-sign-up-form').on('validation', function(evt, valid){
            self.valid = valid;
        });

        // $('#footer-sign-up-form .code-group span').on('click',function(e){
        //     $('#footer-sign-up-form .promo-icon').toggleClass('visible');
        // });
    
    }
    
    FooterComponent.prototype.sendFormData = function(){
        var self = this;
        if(self.valid){
            let name = $('#footer-sign-up-form .name-input').val();
            let email = $('#footer-sign-up-form .email-input').val();
            let formtype = $('#footer-sign-up-form .formtype-input').val();
            // let g_recaptcha_response = grecaptcha.getResponse();
            $.post('/ajax.php',{
                'formtype': formtype, 
                'name': name, 
                'email': email, 
                // 'g-recaptcha-response': g_recaptcha_response
            } ).fail(function(xhr, status, error) {
                    console.log(error);
                    console.log(status);
            }).done(function(data){
                    data = JSON.parse(data);
                    if(data.info==200){
                        $('#footer-sign-up-form .thanks').removeClass('invisible');
                    }else{
                        $('#footer-sign-up-form .validation-errors').empty().append(data.errormessage);
                    }

            });
        }
    }


    //init
    let oFooterComponent = new FooterComponent();
    $.validate({
        form : '#footer-sign-up-form',
        lang: 'pl',
        modules : 'html5',
    });
});