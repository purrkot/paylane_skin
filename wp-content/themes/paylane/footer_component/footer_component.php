<?php $args1 = array(
        'container'=>false,
        'theme_location' => 'footer-menu-1',
        'menu_class' => 'footer-menu 1st'
    ); 
    $args2 = array(
        'container'=>false,
        'theme_location' => 'footer-menu-2',
        'menu_class' => 'footer-menu 2nd'
    ); 
    $args3 = array(
        'container'=>false,
        'theme_location' => 'footer-menu-3',
        'menu_class' => 'footer-menu 3rd'
    ); ?>
<div class="footer_component container">                     
    <div class="row justify-content-center footer-column">
        <div class="col-12 logo-col d-flex justify-content-center logos align-items-center">
        <img src="<?php echo get_template_directory_uri();?>/assets-home/paylane-logo.svg" alt="PayLane - Płatności elektroniczne">
        <img class="pep" src="<?php echo rwmb_meta( 'pep-logo-orange', array( 'object_type' => 'setting', 'size'=>'large' ), 'paylane' )['url'];?>" alt="Polskie ePłatności | Terminale Płatnicze - Akceptuj Płatności Kartą">
        </div>
    </div>
                        <div class="row justify-content-center footer-column">
                            <div class="col-12 col-sm-6 col-lg-2">
                                <?php wp_nav_menu($args1);?>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <ul class="footer-menu 2nd">
                                    <?php wp_nav_menu($args2);?>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <ul class="footer-menu 3rd">
                                    <?php wp_nav_menu($args3);?>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 text-center">
                                <form class="footer-sign-up-form" id="footer-sign-up-form">
                                    <?php echo get_template_part( 'register_form_component/newsletter_thanks' );?>
                                    <div class="feather-ico">
                        				<img src="<?php echo get_template_directory_uri();?>/assets-home/feather-icon.svg" alt="PayLane - Płatności elektroniczne">
                                    </div>
                                    <h5 class="form-title">Zapisz się do newslettera</h5>
                                    <div class="form-element-ico name-ico">
                                    	<input class="name-input form-element" type="text" placeholder="Imię" >
                                    </div>
                                    <div class="form-element-ico email-ico">
                                    	<input class="email-input form-element" type="email" placeholder="Email" required >
                                    </div>
                                    <div class="form-element-ico submit-ico">
                                        <input type="hidden" class="formtype-input" value="newsletter">
                                        <?php $privacy_id = 'privacy-link';
                                            $terms_id = 'terms-link';
                                            $captcha = 'captcha';
                                            $option_name = 'paylane';
                                            $post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
                                            $post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
                                            $site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
                                        ?>
                                        <div class="terms">Akceptuję <a href="<?php echo get_the_permalink($post_id_2);?>">regulamin</a> i <a href="<?php echo get_the_permalink($post_id);?>">politykę prywatności</a></div>
                                        <!-- <div class="g-recaptcha" data-sitekey="<?php echo $site_key;?>"></div> -->
                                    	<button class="submit-button form-element">Zarejestruj się</button>
                                        <div class="form-group validation-errors">
                                        </div>
                                    </div>
                                </form>
                            </div>
            </div>
</div>