<meta property="og:url"                content="<?php the_permalink();?>" />
<meta property="og:type"               content="<?php if(is_archive()){echo 'page';} elseif(is_page()) {echo 'page';} elseif(is_single()){echo 'article';} else {echo 'page';}?> " />
<meta property="og:title"              content="<?php the_title();?>" />
<meta property="og:description"        content="<?php //**TODO Error from Apache the_excerpt();?>" /> 
<meta property="og:image"              content="<?php if(has_post_thumbnail()){the_post_thumbnail_url();}else{echo 'use default link for default image';}?>"/>



<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '1839965236244724'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=1839965236244724&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
