<div class="col-sm-12 col-md-6 col-lg-4">
    <a href="<?php the_permalink();?>">
        <article class="featured-single text-center" style="background:url(<?php the_post_thumbnail_url('large');?>);background-size: cover;">
            <div class="cover"></div>
            <h5><?php the_title();?></h5>
            <span><?php echo get_the_date(); ?></span>
        </article>
    </a>
</div>