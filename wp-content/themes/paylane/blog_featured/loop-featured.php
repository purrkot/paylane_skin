
<div class="container1440 featured text-center">
    <h4>Prosto z naszego bloga</h4>
    <div class="row">
        <?php 
            if (!wp_is_mobile()){
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'orderby'=> 'rand',
                );
            }  else {
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 2,
                    'orderby'=> 'rand',
                );
            }
            $featured = new WP_Query($args);
            if ( $featured->have_posts() ) : while ( $featured->have_posts() ) : $featured->the_post();
                echo get_template_part('blog_featured/single-featured');
            endwhile; endif; wp_reset_postdata(); 
        ?>
    </div>
</div>