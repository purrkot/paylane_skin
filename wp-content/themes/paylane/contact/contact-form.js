var response1
var response2
var contactRecaptcha1 = function(a){
    response1 = a
}
var contactRecaptcha2 = function(a){
    response2 = a
}

var resetCaptcha = function(){
    try{
        grecaptcha.reset(0)
        grecaptcha.reset(1)
    }catch(e){}
}

jQuery(document).ready(function ($) {
    let ContactFormComponent = function(){
        var self=this;
        //init events
        $('.support-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData(e);
        });

        $('.sales-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData(e);
        });

    }
    
    ContactFormComponent.prototype.sendFormData = function(e){

        if($(e.target).closest('form').hasClass('support-form')){
            let formClass = '.support-form'
            let name = $(formClass+' .name').val();
            let surname = $(formClass+' .surname').val();
            let email = $(formClass+' .email').val();
            let company = $(formClass+' .company').val();
            let website = $(formClass+' .website').val();
            let message = $(formClass+' .message').val();
            let phone = $(formClass+' .phone').val();
            let formtype = $(formClass+' .formtype-input').val();

            $.post('/ajax.php',{
                'formtype': formtype, 
                'name': name, 
                'surname': surname, 
                'email': email, 
                'company': company,
                'phone' : phone, 
                'website': website, 
                'message': message,
                'g-recaptcha-response': response2
            } ).done(function(data){
                    data = JSON.parse(data);
                    if(data.info==200){
                        $('.support-form .thanks').removeClass('invisible');
                    }else{
                        $('.support-form .validation-errors').empty().append(data.errormessage);
                    }
            });

        } else if($(e.target).closest('form').hasClass('sales-form')){

            let phone = $('.sales-form .phone').val();
            let formtype = $('.sales-form .formtype-input').val();
            $.post('/ajax.php',{
                'formtype': formtype, 
                'phone': phone,
                'g-recaptcha-response': response1
            } ).done(function(data){
                resetCaptcha()
                data = JSON.parse(data);
                if(data.info==200){
                    $('.sales-form .validation-errors').empty().append('Wiadomość wysłana pomyślnie');
                }else{
                    $('.sales-form .validation-errors').empty().append(data.errormessage);
                } 
            })

        }

    }


    //init
    let oContactFormComponent = new ContactFormComponent();
    $.validate({
        form : '.sales-form',
        lang: 'pl',
        modules : 'html5',
    });
    $.validate({
        form : '.support-form',
        lang: 'pl',
        modules : 'html5',
    });
});