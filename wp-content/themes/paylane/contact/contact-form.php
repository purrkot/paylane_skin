<?php 
$devzone_id = 'devzone-link';
$knowledge_id = 'knowledge-link';
$privacy_id = 'privacy-link';
$terms_id = 'terms-link';
$captcha = 'captcha';
$option_name = 'paylane';
$post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
$post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
$site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
$knowledge = rwmb_meta( $knowledge_id, array( 'object_type' => 'setting' ), $option_name );
$devzone = rwmb_meta( $devzone_id, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="col-xs-12 col-sm-12 co-md-6 col-lg-6 sales-wrapper">
    <div class="form-title"><?php echo rwmb_meta('sales-title');?></div>
    <form action="" class="sales-form text-center">
        <?php echo get_template_part( 'register_form_component/phone_thanks' );?>
        <div class="phone-icon">
            <input type="tel" class="phone" placeholder="Numer telefonu">
        </div>
        <p class="text-center" >Akceptuję <a href="<?php the_permalink($post_id_2);?>">regulamin</a> i <a href="<?php the_permalink($post_id);?>">politykę prywatności</a></p>
            <input type="hidden" class="formtype-input" value="sales-contact">
            <div class="g-recaptcha" id="recap1" data-sitekey="<?php echo $site_key;?>"  data-callback="contactRecaptcha1"></div>
        <button type="submit" class="submit submit-button" value="Dodaj numer telefonu"><?php echo rwmb_meta('sales-form-button');?></button>
        <div class="form-group validation-errors"></div>
    </form>
    <div class="subtitle">Skorzystaj z naszej bazy wiedzy</div>
    <div class="dev-book">
        <a href="<?php the_permalink($devzone);?>" class="submit cpu">Devzone - strefa developera</a>
        <div class="middle-text">albo</div>
        <a href="<?php the_permalink($knowledge);?>" class="submit book-white">Księga wiedzy</a>
    </div>
</div>
<div class="col-xs-12 col-sm-12 co-md-6 col-lg-6 contact-wrapper">
    <div class="form-title"><?php echo rwmb_meta('support-title');?></div>
    <div class="subtitle"><?php echo rwmb_meta('support-subtitle');?>.</div>
    <form action="" class="support-form text-center" data-validation="recaptcha">
        <?php echo get_template_part( 'register_form_component/contact_thanks' );?>
        <div class="name-icon">
            <input type="text" class="name" placeholder="Imię" required data-validation="required">
        </div>
        <div class="surname-icon">
            <input type="text" class="surname" placeholder="Nazwisko" required data-validation="required">
        </div>
        <div class="email-icon">
            <input type="email" class="email" placeholder="Email" required data-validation="required">
        </div>
        <div class="phone-icon">
            <input type="text" class="phone" placeholder="Telefon" required data-validation="required">
        </div>
        <div class="company-icon">
            <input type="text" class="company" placeholder="Nazwa firmy" required data-validation="required">
        </div>
        <div class="website-icon">
            <input type="url" class="website" placeholder="Strona firmy">
        </div>
        <div class="message-icon">
            <textarea name="message" id="message" class="message" placeholder="Wiadomość"></textarea>
        </div>
        <p class="text-center" >Akceptuję <a href="<?php the_permalink($post_id_2);?>">regulamin</a> i <a href="<?php the_permalink($post_id);?>">politykę prywatności</a></p>
        <input type="hidden" class="formtype-input" value="contact">
        <div class="g-recaptcha" id="recap2" data-sitekey="<?php echo $site_key;?>" data-callback="contactRecaptcha2"></div>
        <button type="submit" class="submit submit-button" value="Napisz do nas"><?php echo rwmb_meta('support-form-button');?></button>
        <div class="form-group validation-errors"></div>
    </form>
</div>