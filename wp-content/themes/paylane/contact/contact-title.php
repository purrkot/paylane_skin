<div class="cloud left">
    <img src="<?php echo get_template_directory_uri();?>/oferta_single/cloud-right.svg" alt="Chmura | PayLane - Płatności elektroniczne">
</div>
<div class="cloud right">
    <img src="<?php echo get_template_directory_uri();?>/oferta_single/cloud-left.svg" alt="Chmura | PayLane - Płatności elektroniczne">
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 contact-title text-center justify-content-center justify-items-center">

    <img src="<?php echo get_template_directory_uri();?>/contact/stripes.svg" class="Paski, separator | PayLane - Płatności elektroniczne" alt="wings">   

    <h1>
        <?php echo rwmb_meta('contact-claim');?>
    </h1>
    <p>
        <?php echo rwmb_meta('contact-subtitle');?>
    </p>
    
</div>
