<div class="col-12 contact-box-title  text-center">
    <h1>Odwiedź nas w trójmieście</h1>
</div>
<div class="contact-box col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center text-sm-center text-md-right">
    <div class="box-city"></div>
    <div class="box text-left">
        <div class="box-view" style="background:url(<?php echo rwmb_meta('box-view')['url'];?>);"></div>
        <div class="cover"></div>
        <div class="box-adress"><?php echo rwmb_meta('contact-adress');?></div>
    </div>
</div>
<div class="contact-box col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center text-sm-center text-md-left">
    <div class="box">
    <iframe width="377" height="377" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo rwmb_meta('map-address');?>" style="border: 1px solid black"></iframe>
    </div>
</div>