<div class="container learn-loop">
        <div class="row justify-content-center">
        <?php   $taxonomy     = 'category';
                    $orderby      = 'name'; 
                    $show_count   = false;
                    $pad_counts   = false;
                    $hierarchical = true;
                    $title        = '';
                    
                    $args = array(
                    'child_of' => '32',
                    'taxonomy'     => $taxonomy,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty' => false,
                    'depth' => 1,
                    );
            ?>
 
                <ul class="learn-menu col-12">
                    <?php wp_list_categories( $args ); ?>
                </ul>
        </div>
        <div class="row justify-content-center">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php if( $wp_query->current_post == 0 ) : ?>
        
            <div class="ol-xs-12 col-sm-12 col-md-12 col-lg-8 text-center article-wrapper">
            <a href="<?php the_permalink();?>">
                <?php if(has_post_format('video')){?>
                    <div class="icon-video icon"></div>
                    <?php } else { ?>
                    <div class="icon-normal-green icon"></div>
                <?php };?>
                <div class="article" style="background:url(<?php the_post_thumbnail_url("large");?>);">
                    <div class="cover">
                        <div class="title"><?php the_title();?></div>
                    </div>
                </div> 
                </a>
                <div class="name"><?php the_author();?></div>
                <div class="author" style="background:url(<?php echo get_wp_user_avatar_src($user_id, 'original'); ?>);">     
                </div>
            </div>
            
        <?php else : ?>
        
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center article-wrapper">
            <a href="<?php the_permalink();?>">
            <div class="icon-normal-green icon"></div>
                <div class="article" style="background:url(<?php the_post_thumbnail_url("large");?>);">
                    <div class="cover">
                    <div class="title"><?php the_title();?></div>
                    </div>
                </div>
            </a>
            <div class="name"><?php the_author();?></div>
            <div class="author" style="background:url(<?php echo get_wp_user_avatar_src($user_id, 'original'); ?>);">
            
            </div>
            </div>
        
        <?php endif; ?>
        
<?php endwhile;
    echo "<div class='col-sm-12 col-md-12 col-lg-12 pagination'>";
    echo paginate_links( array(
    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
    'current'      => max( 1, get_query_var( 'paged' ) ),
    'format'       => '?paged=%#%',
    'show_all'     => false,
    'type'         => 'plain',
    'end_size'     => 2,
    'mid_size'     => 1,
    'prev_next'    => true,
    'prev_text'    => '',
    'next_text'    => sprintf( '%1$s <i></i>', __( 'Starsze', 'text-domain' ) ),
    'add_args'     => false,
    'add_fragment' => '',
    ) );
    echo "</div>";
endif;
?>
<div class="separator-deepgrey--full-width separator"></div>
</div>
    </div>
