<div class="learn-header container-fluid">
            <div class="row cover">
                <div class="col-12 text-center align-self-center">
                    <div class="container1440 container">
                       <div class="row">
                           <div class="col-12 text-center">
                               <h1 class="title">
                                   <?php if(is_category()){single_cat_title();}else if (is_archive()){echo post_type_archive_title( '', false );}else{the_title();};?>
                                </h1>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
</div>