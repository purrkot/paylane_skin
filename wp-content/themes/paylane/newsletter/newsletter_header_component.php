<div class="newsletter_header_component">
	<div class="container container1440">
		<div class="row justify-content-center">			
			<div class="col-12 col-lg-10">
				<div class="row">
					<div class="col-6 col-lg-3 cloud left-cloud">			
					</div>
					<div class="col-12 col-lg-6 order-3 order-lg-2 descryption-section">
						<h1>Bądź na bieżąco!</h1>
						<p>W biznesie trzeba mieć refleks i stale się rozwijać. Dlatego w newsletterze 
						PayLane dzielimy się z Tobą świeżymi informacjami o najnowszych rozwiązaniach
						w świecie e-płatności oraz naszą wiedzą ekspercką.</p>
					</div>
					<div class="col-6 col-lg-3 order-2 order-lg-3 cloud right-cloud">			
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>