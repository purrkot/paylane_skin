<div class="newsletter_footer_component">
    <div class="container container1440">
        <div class="row justify-content-center">
			<div class="col-12 text-center">
                <h5>Zaufali nam</h5>
            </div>
            <div class="col-12 text-center">
                <div class="row justify-content-center align-content-center align-items-center">
                <?php
					$args = array(
						'post_type' => 'klient',
						'posts_per_page' => 4,
						'orderby'=>'rand'
						);
				
					query_posts($args);
					if (have_posts() ) : while (have_posts() ) : the_post(); ?>	
                        <img class="img-fluid client" src="<?php echo get_the_post_thumbnail_url();?>">
                    <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                </div>
            </div>
        </div>
    </div>
</div>
