<div class="newsletter_form_component">
    <div class="container container1440">
        <div class="row justify-conentent-center">
			<div class="col-12 main-wrapper ">
				<div class="row text-center">
					<div class="col-12 d-md-none plane">
						<img src="<?php echo get_template_directory_uri(); ?>/assets_newsletter/paper-plane.svg" alt="" class="img-fluid">
					</div>
					<div class="col-12">
						<h3 class="slogan">
								Zapisz się i trzymaj rękę na pulsie!
						</h3>
					</div>
				</div>
				<div class="row no-gutters">					
					<div class="col-12 col-lg-4 offset-lg-4 d-flex justify-content-center">				
						<div class="login-box">
							<div class="plane-sticky d-none d-md-block">
							</div>					
							<form>
								<div class="form-group">
									<div class="input-group input-group-text">
										<div class="input-group-prepend">
											<span class="input-group-icon" id="inputGroupPrepend">
												<img src="<?php echo get_template_directory_uri(); ?>/assets-register/user.svg">
											</span>
										</div>
										<input type="text" placeholder="Imię" class="form-control ">
									</div>
								</div>
								<div class="form-group">
									<div class="input-group input-group-text">
										<div class="input-group-prepend">
											<span class="input-group-icon" id="inputGroupPrepend">
												<img src="<?php echo get_template_directory_uri(); ?>/assets_newsletter/mail.svg">
											</span>
										</div>
										<input type="email" placeholder="E-mail" class="form-control">
									</div>
								</div>
								<?php 
								$privacy_id = 'privacy-link';
								$terms_id = 'terms-link';
								$option_name = 'paylane';
								$post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
								$post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
								?>
								<div class="form-group policy-link">
									<span>Akceptuję </span><a href="<?php echo get_the_permalink($post_id_2);?>">regulamin</a> i <a href="<?php echo get_the_permalink($post_id);?>">politykę prywatności</a>
								</div>
								<div class="form-group send-button-group">
									<button type="submit" class="btn">
										<img src="<?php echo get_template_directory_uri(); ?>/assets_newsletter/white-feather.svg">Zapisz się do newslettera
									</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					$args = array(
						'post_type' => 'klient',
						'posts_per_page' => 1,
						'orderby'=>'rand',
						//Important lines below, checks inthe query if the Meta Values exists
						'meta_query' => array( 
							array( 
								'key'     => 'client-photo',
								'compare' => 'EXISTS'
							),
							array( 
								'key'     => 'client-test',
								'compare' => 'EXISTS'
							)
						)
						);
				
					query_posts($args);
					if (have_posts() ) : while (have_posts() ) : the_post(); ?>				
					<div class="col-12 col-lg-3 recomendation-column">
						<div class=" text-center recommendation-box">
						<img class="img-fluid" src="<?php echo rwmb_meta('client-photo')['url'];?>">
							<p class="s1"><?php echo rwmb_meta('client-test');?></p>
							<p class="s2"><?php echo rwmb_meta('testimonial-name');?></p>
							<p class="s3"><?php the_title();?></p>							
						</div>							
					</div>		
					<?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>			
				</div>
			</div>			
        </div>
    </div>
</div>
