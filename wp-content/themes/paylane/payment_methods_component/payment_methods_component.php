<?php 
$register_id = 'register-link';
$contact_id = 'contact-link';
$option_name = 'paylane';
$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="payment_methods_component">
	<div class="container container1920">		
		<?php get_template_part('logo_rotator_component/logo_rotator_component');?>
		<div class="row clouds-row justify-content-center">
			<div class="cloud-1 d-none d-lg-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud1.svg"></div>
			<div class="cloud-2 d-none d-md-none d-xl-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud2.svg"></div>
			<div class="cloud-3 d-lg-none d-xl-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud5.svg"></div>
		</div>
		<div class="row planes-row justify-content-center">
			<div class="plane-1 d-none d-lg-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/plane1.svg"></div>
			<div class="plane-2"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/plane2.svg"></div>
		</div>
		<div class="row pay-cards-row justify-content-center">
			<div class="col-12 col-lg-6 pay-card-image">
				<div class="logo-container">
					<img  class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/karta.svg">
				</div>
			</div>
			<div class="col-12 col-lg-6 order-1 order-lg-0 pay-card-description">
				<div class="row">
					<div class="description-wrapper col-md-7 col-lg-8">
						<h3>Obsługuj karty płatnicze z całego świata i niech Twój biznes leci tam, gdzie chcesz!</h3>
						<p>Karty płatnicze MasterCard wydawane są przez ponad 25 000 instytucji finansowych na świecie. To jedna z podstawowych form płatności, której obsługa jest dziś absolutnym standardem.</p>
					</div>
					<div class="col-12">
						<div class="buttons-row row">
							<div  class="col-auto">
								<a class="btn btn-yellow" href="#">Załóż konto</a>
							</div>
							<div  class="col-auto">
								<a class="btn btn-outline" href="#">Więcej o karcie MasterCard</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 card-logos-row">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-auto card-logos-wrapper">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Discover.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Mastercard-logo.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Mastercard-logo.svg" alt="">
							<img class="img-fluid active" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Mastercard-logo.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Maestro_2016.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/visa.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/American_Express_logo.svg" alt="">
							<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Diners_Club_Logo3.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row clouds-row-next justify-content-center">
			<div class="cloud-1 d-none d-lg-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud1.svg"></div>
			<div class="cloud-2 d-none d-md-none d-xl-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud2.svg"></div>
			<div class="cloud-3 d-none d-xl-block"><img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Cloud5.svg"></div>
		</div>
		<div class="row paper-plane-row justify-content-center">			
			<div class="col-12  paper-plane-description">
				<div class="row">				
					<div class="description-wrapper col-md-7 col-lg-auto">
						<h3 class="first-title">Szybkie przelewy to metoda, którą Twój klient łapie w lot!</h3>						
					</div>
					<div class="col-12">
						<div class="buttons-row row">
							<div class="paper-plane-image">	</div>
							<div  class="col-auto">
								<a class="btn btn-blue" href="#">Wypróbuj</a>
							</div>
							<div class="col-12 col-sm-auto col-break">
								albo
							</div>
							<div  class="col-auto">
								<a class="btn btn-yellow" href="#">Załóż konto!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 paper-plane-text-bottom">
				<div class="row justify-content-center">
					<div class="text-wrapper col-md-7">Szybkie przelewy elektroniczne to jedna z najpopularniejszych form płatności online w Polsce. Formularz płatności zostaje wypełniony automatycznie, a kupujący może potwierdzić płatność poprzez logowanie w swoim banku.</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row logos-wrapper text-center">
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
				<div class="col-3 logo"> </div>
			</div>
		</div>
		<div class="row rocket-launcher-row">
			<div class="col-12 col-lg-6 rocket-launcher-image">
			</div>
			<div class="col-12 col-lg-6 rocket-launcher-description">
				<div class="row">			
					<div class="description-wrapper col-md-7 col-lg-8">
						<h3>Z e-portfelami startujesz po więcej!</h3>
						<p>PayPal jest jedną z najpopularniejszych mateod płatności na świecie. Małe i średnie biznesy zwykle
						 wybierają go ze względu na szeroki wybór usług - płatność kartami, e-portfel, 
						 płatności cykliczne. PayPal jest znany i wykorzystywany niemal na całym świecie i 
						 doskonale sprawdza się jako opcja płatności bez względu na skalę biznesu.</p>
					</div>
					<div class="col-12">
						<div class="logos-row">
							<div class="col-auto logos-inner-row">
								<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Giropay.svg" alt="">
								<img class="img-fluid active" src="<?php echo get_template_directory_uri() ?>/assets-home/bank_paypal.svg" alt="">
								<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Giropay.svg" alt="">
								<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Giropay.svg" alt="">
								<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/Giropay.svg" alt="">
								<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/ideal.svg" alt="">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="buttons-row row">
							<div  class="col-auto">
								<a class="btn btn-blue" href="#">Wypróbuj</a>
							</div>
							<div class="col-12 col-sm-auto col-break">
								albo
							</div>
							<div  class="col-auto">
								<a class="btn btn-yellow" href="#">Załóż konto!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
