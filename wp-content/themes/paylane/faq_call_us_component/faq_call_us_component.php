
<?php 
    $contact = 'contact-link';
    $option_name = 'paylane';
    $post_id = rwmb_meta( $contact, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="faq_call_us_component">
    <div class="row justify-content-center">
        <div class="col-8 col-sm-8 col-md-12 col-lg-12 text-center call-us">
            <a href="<?php the_permalink($post_id);?>">
                <h3>Masz więcej pytań? Skontaktuj się z nami!</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/assets-faq/life-buoy.svg" alt="Skontaktuj się z nami | PayLane - Płatności elektroniczne">
            </a>
        </div>
    </div>
</div>
