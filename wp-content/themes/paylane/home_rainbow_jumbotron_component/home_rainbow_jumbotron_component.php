<?php 
    $apple_header = rwmb_meta('apple-enable');
    $apple_title = rwmb_meta('apple-title');
    $apple_icon = rwmb_meta('apple-icon');
    $apple_content = rwmb_meta('apple-content');
    $apple_post = rwmb_meta('apple-post');

    $register = 'register-link';
    $devzone = 'devzone-link';
    $payments = 'payments-link';
    $plugins = 'plugins-link';
    $about = 'about-link';

    $option_name = 'paylane';
    $register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
    $devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );

    //Define randomizer and counter
    $rand_controler = mt_rand(1,3);
    $loop_controler = 0;
?>

<div class="home_rainbow_jumbotron_component <?php if($apple_header != true) { ?>normal<?php }else{?>grey<?php };?>">
    <div class="container1920 container">
        <div class="row <?php if($apple_header == true) { ?>justify-content-center<?php };?>">
            <?php if($apple_header != true) { ?>
            <div class="col-12">
                <div class="row top">
                <?php $group_values = rwmb_meta( 'jumbo-header' ); // Group is cloneable - watch out!
                    foreach ( $group_values as $group_value ) { 
                            $loop_controler++;
                            $image_ids = $group_value['jumbo-icon'];
                            $title_ids = isset( $group_value['jumbo-title'] ) ? $group_value['jumbo-title'] : array();
                            $subtitle_ids = isset( $group_value['jumbo-content'] ) ? $group_value['jumbo-content'] : array();?>
                            <?php if($rand_controler === $loop_controler){ ?>
                            <div class="col-sm-12 col-md-6 col-lg-6 text-center top-1st">
                                <div class="big-icon-holder">
                                    <?php echo '<img class="big-icon" src="' . $image_ids . '" alt="PayLane | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza">';?>
                                </div>
                            </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 text-left text-sm-left text-md-left top-2nd">
                            <h3><?php echo $title_ids;?></h3>
                            <div class="underline"></div>
                            <p><?php echo $subtitle_ids;?><p>
                            <div class="button-bar">
                                    <a class="btn btn-transparent btn-fixed-medium" href="<?php the_permalink($register_url);?>">Załóż konto</a>
                                    <a class="btn btn-white btn-fixed-medium" href="#plugins">Dowiedz się więcej</a>
                            </div>
                            <div class="d-none d-sm-none d-md-none d-lg-block text-sm-center text-md-left text-lg-left">
                                <p class="pep-title">O Wasze pieniądze dbamy wraz z<p>
                                <img class="pep-logo img-img-fluid" src="<?php $pep_white = rwmb_meta( 'pep-logo-white', array( 'object_type' => 'setting' ), 'paylane' ); echo $pep_white['full_url'];?>" alt="PEP Polskie ePłatności | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                            </div>
                        </div>
                        <?php } else {};?>
                    <?php };?>
                </div>

                <div class="d-block d-sm-block d-md-block d-lg-none d-xl-none text-center">
                            <p class="pep-title">O Wasze pieniądze dbamy wraz z<p>
                            <img class="pep-logo img-img-fluid" src="<?php echo $pep_white['full_url'];?>" alt="PayLane | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                </div>
                <div class="row bottom">
                    <div class="col-lg-6 d-none d-sm-none d-md-none d-lg-inline bottom-1st">
                        <div class="row justify-content-center align-items-center align-content-center">
                        <?php
					$args = array(
						'post_type' => 'klient',
						'posts_per_page' => 1,
						'orderby'=>'rand',
						//Important lines below, checks if the query has the Meta Values, if not, goes to another database entry
						'meta_query' => array( 
							array( 
								'key'     => 'client-photo',
								'compare' => 'EXISTS'
							),
							array( 
								'key'     => 'client-test',
								'compare' => 'EXISTS'
							)
						)
						);
				
					query_posts($args);
                    if (have_posts() ) : while (have_posts() ) : the_post(); 
                            $client_photo = rwmb_meta('client-photo',array( 'size' => 'full' ));
                            $client_name = rwmb_meta('testimonial-name');
                            $client_position = rwmb_meta('client-position');
                            $client_content  = rwmb_meta('client-test');?>	

					        <div class="col-lg-4 text-center text-xs-center text-sm-center text-md-right">
                                <div class="client-photo" style="background:url(<?php echo $client_photo['url'];?>);"></div>
                            </div>

                            <div class="comment-col col-lg-6">
                                <h5><?php echo $client_name; echo ', '; echo get_the_title();?></h5>
                                <p><?php echo $client_content;?></p>
                            </div>	

					<?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>	
                            
                        </div>
                    </div>
                    <div class="col-lg-6 bottom-2nd">
                        <div class="row">
                            <div class="col-12">
                                <h5>Zaufali nam</h5>
                            </div>
                            <div class="col-12">
                                <div class="row align-items-center justify-content-xs-center justify-content-sm-center justify-content-md-center justify-content-lg-start align-content-center">
                                <?php
                                    $args = array(
                                        'post_type' => 'klient',
                                        'posts_per_page' => 4,
                                        'orderby'=>'rand',
                                        //Important lines below, checks inthe query if the Meta Values exists
                                        'meta_query' => array( 
                                                array( 
                                                    'key'     => 'client-logo-white',
                                                    'compare' => 'EXISTS'
                                                ),
                                            )
                                        );
                                    query_posts($args);
                                    if (have_posts() ) : while (have_posts() ) : the_post(); $white_icon = rwmb_meta('client-logo-white',array( 'size' => 'medium' ));?>				
                                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 clients text-center">
                                        <img class="img-fluid" src="<?php echo $white_icon['url'];?>" alt="PayLane | Karty płatnicze, płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                                    </div>  	
                                    <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else { ?>
                <div class="col-12 col-sm-12 col-md-8 col-lg-6 apple-title text-center">
                    <h1><?php echo $apple_title;?></h1>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-8 col-lg-6 text-center apple-icon">
                    <img src="<?php echo $apple_icon;?>" alt="Apple Pay">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-8 col-lg-6 apple-content text-left">
                    <p><?php echo $apple_content;?></p>
                </div>
            </div>
            <div class="button-bar center">
                <a href="<?php the_permalink($register_url);?>" class="btn btn-fixed-medium btn-blue">Załóź konto</a>
                <a href="<?php the_permalink($apple_post);?>" class="btn-fixed-medium btn-transparent">Dowiedz się więcej</a>
            </div>
            <div class="row">
                <div class="col-12 text-center peplane">
                    <p>O wasze pieniądze dbamy wraz z</p>
                    <img class="pep" src="<?php echo rwmb_meta( 'pep-logo-black', array( 'object_type' => 'setting','size' => 'large' ), 'paylane' )['url'];?>" alt="Polskie ePłatności">
                </div>
            </div>
            <?php }; ?>
        </div>
    </div>
</div>
<?php if($apple_header == true) { ?>
    <div class="logo-block container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h5>Poznaj firmy które korzystają z naszej technologii</h5>
                </div>
                <div class="col-12 text-center">
                    <div class="row justify-content-center align-content-center align-items-center">
                        <?php
                        $args = array(
                            'post_type' => 'klient',
                            'posts_per_page' => 4,
                            'orderby'=>'rand'
                            );
                        query_posts($args);
                        if (have_posts() ) : while (have_posts() ) : the_post(); ?>	
                            <img class="img-fluid clients_alter" src="<?php the_post_thumbnail_url('medium');?>">
                        <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                     </div>
                </div>
            </div>
        </div>
<?php };?>
