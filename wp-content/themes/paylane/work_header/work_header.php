<div class="work_header">
	<div class="container-fluid">
		<div class="cloud-row row  justify-content-center">
			<div class="cloud-left">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_work/cloud-left.svg" class="img-fluid">
			</div>
			<div class="cloud-right">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_work/cloud-right.svg" class="img-fluid">
			</div>
			<div class="container container1440">
				<?php if(!wp_is_mobile()){
				get_template_part('full_width_stripes_grey/full_width_stripes_grey');
				}?>
				<div class="row justify-content-center">					
					<div class="col-12 col-lg-7 text-center">
						<h1 class="title">Dołącz do naszego zespołu i stań się częścią zgranej ekipy pełnych pasji profesjonalistów!</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="separator-gradient--full-width"></div>	
	<div class="container-fluid">
		<div class="row full-width-picture-row ">
			<?php if(wp_is_mobile()){
						$loop = new WP_Query(
							array(
								'post_type'=>'nasz-zespol', 
								'posts_per_page'=>	4, 
								'orderby'=>'rand',
								'meta_query' => array(
									array(
									 'key' => 'american',
									 'compare' => 'EXISTS' // this should work...
									),
								),
							)
						);
						if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
							<div class="member-img col-3 col-sm-3 col-md-3" style="background:url('<?php echo rwmb_meta('american')['full_url'];?>');">	
							</div>
						<?php endwhile; endif;?>	
					<?php	} else {
						$loop = new WP_Query(
							array(
								'post_type'=>'nasz-zespol', 
								'posts_per_page'=>8, 
								'orderby'=>'rand',
								'meta_query' => array(
									array(
									 'key' => 'american',
									 'compare' => 'EXISTS' // this should work...
									),
								),
							)
						);
						if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
							<div class="member-img" style="background:url('<?php echo rwmb_meta('american')['full_url'];?>');"></div>
						<?php endwhile; endif;?>	
					<?php }?>		
		</div>
	</div>
	<div class="separator-gradient--full-width"></div>
	<div class="container container1440">
		<div class="subtitle-row row justify-content-center text-center">
			<div class="col-12 col-md-8 col-lg-5">
				<h2 class="subtitle">PayLane to prężnie działający biznes i zespół, w którym rozwijasz się i pracujesz z przyjemnością.</h2>
			</div>
		</div>
	</div>
</div>
