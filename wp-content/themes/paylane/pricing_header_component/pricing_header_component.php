<?php $title = rwmb_meta('pricing-title');
		$subtitle = rwmb_meta('subtitle');?>

<div class="pricing_header_component">
	<div class="container container1440">
		<div class="row justify-content-center no-gutters">
			<div class="cloud-left col-7 col-md-3 text-right">
				<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-pricing/cloud-left.svg" alt="">
			</div>
			<div class="col-12 col-md-6 header-descryption order-3 order-md-2 text-center">
				<div class="row justify-content-center no-gutters">
					<div class="col-12 col-sm-10  ">
						<h1><?php echo $title;?></h1>
						<p><?php echo $subtitle;?></p>
					</div>
				</div>
			</div>
			<div class="cloud-right col-5 col-md-3 order-2 order-md-3">
				<div class="small-cloud">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-pricing/cloud-right.svg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>