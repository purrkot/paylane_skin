<?php 
/**
 * Template Name: Newsletter
 *
 */
get_header();
	get_template_part("newsletter/newsletter_header_component");
    get_template_part("newsletter/newsletter_form_component");
	get_template_part("newsletter/newsletter_footer_component");
get_footer();
?>