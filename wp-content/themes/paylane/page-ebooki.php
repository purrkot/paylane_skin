<?php
/**
 * Template Name: e-booki
 *
 */ 
get_header();
	require("custom_pagination_component/custom_pagination_component.php");

	echo get_template_part("full_image_title_header/full_image_title_header");
	$loop = new WP_Query(array('post_type'=>'ebook' ));
    if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
		echo get_template_part('ebook_preview_component/ebook_preview_component');
    endwhile;
        the_posts_pagination();
    endif;

get_footer();
?>