<?php   
        $register_id = 'register-link';
		$contact_id = 'contact-link';
		$payments_id = 'payments-link';
		$option_name = 'paylane';
		$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
		$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );	
?>
<div class="payment_methods_rocket_component">
    <div class="container container1920">
        <div class="row">
            <div class="col-12 col-lg-6 text-center text-lg-right  rocket-row-image">
				<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/assets-platnosci/rocket_planet.svg">
			</div>
            <div class="col-12 col-lg-6 rocket-row-description">
                <div class="row">
                    <div class="description-wrapper col-md-7 col-lg-8">
                        <h2>Cały kosmos płatności online o globalnym zasięgu!</h3>
                            <p>
                                Oddajemy do Twojej dyspozycji ponad 30 metod płatności obsługiwanych na całym świecie. Sprzedajesz szybko i bezpiecznie, w sposób dobrany pod Twojego klienta i region, z którego dokonuje zakupu.
                            </p>
                    </div>
                    <div class="col-12">
                        <div class="buttons-row row">
                            <div class="col-auto">
                                <a class="btn btn-yellow" href="<?php the_permalink($post_id);?>">Załóż konto</a>
                            </div>
                            <div class="col-auto">
                                <a class="btn btn-yellow-dashed dashed-gradient" href="#wiecej">Dowiedz się więcej</a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
<div>
	<?php get_template_part('logo_rotator_component/logo_rotator_component');?>
</div>
