<?php if(is_page() && has_post_thumbnail()){?>
    <div class="full_image_title_header container-fluid" style="background:url(<?php the_post_thumbnail_url('full');?>);">
<?php } else { ?>
    <div class="full_image_title_header container-fluid no-thumbnail">
<?php } ?>
            <div class="row cover">
                <div class="col-12 text-center align-self-center">
                    <div class="container1440 container">
                       <div class="row">
                           <div class="col-12 text-center">
                               <h2 class="title"><?php if(is_category()){single_cat_title();}else if (is_archive()){echo post_type_archive_title( '', false );}else{the_title();};?></h2>
                           </div>
                       </div>
            </div>
        </div>
    </div>
</div>