
<?php 
		$left_title = rwmb_meta('left-title');
		$right_title = rwmb_meta('right-title');
		$left_inside_title = rwmb_meta('left-inside-title');
		$right_inside_title = rwmb_meta('right-inside-title');
		$pricing = rwmb_meta('left-pricing');
		$benefit_1 = rwmb_meta('left-benefit-1');
		$benefit_2= rwmb_meta('left-benefit-2');
		$benefits = rwmb_meta('right-benefits');
		$register_id = 'register-link';
		$contact_id = 'contact-link';
		$payments_id = 'payments-link';
		$option_name = 'paylane';
		$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
		$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );
		
?>
<div class="pricing_train_component">
	<div id="register-over" class="displayover"><div id="close-reg" class="close"><img src="<?php echo get_template_directory_uri();?>/assets-pricing/close.svg" alt=""></div><?php echo get_template_part('register_form_component/register_form_component');?></div>
	<div id="contact-over" class="displayover"><div id="close-con" class="close"><img src="<?php echo get_template_directory_uri();?>/assets-pricing/close.svg" alt=""></div><?php echo get_template_part('register_form_component/contact_form_component');?></div>
	<div class="contaniner container1440">
		<div class="row no-gutters justify-content-center">
			<div class="col-12 col-md-8">			
				<div class="row no-gutters justify-content-center">
					<div class="col-12 col-md-6 left">					
						<div class="left-description top-description">
							<h5 class="text-center">
								<?php echo $left_title;?>
							</h5>
						</div>					
						<div class="left-table">
							<div class="wind d-none d-md-block">
								<img src="<?php echo get_template_directory_uri(); ?>/assets-pricing/wind.svg" alt="">
							</div>
							<p class="slogan"><?php echo $left_inside_title;?></p>
							<p class="price"><?php echo $pricing;?></p>
							<p class="price-description"><?php echo $benefit_1;?></p>
							<p class="price-description"><?php echo $benefit_2;?></p>
							<p class="price-rule">Cena dotyczy przelewów internetowych w <br><strong>polskich bankach</strong> oraz <strong>kart płatniczych</strong> <br>Visa i Mastercard</p>
							<div id="open-reg" class="btn register-button">
								<img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets-pricing/check-circle.svg" alt="">
								<span>Zarejestruj się</span> 
							</div>
						</div>
					</div>

					<div class="col-12 col-md-6 right">
						<div class="right-description top-description">
							<h5 class="text-center">
							<?php echo $right_title;?>
							</h5>
						</div>
						<div class="right-table">
							<div class="train d-none d-md-block">
								<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-pricing/high-speed-train.svg" alt="">
							</div>
							<div class="shadow-box">
								
							</div>
							<h4><?php echo $right_inside_title;?></h4>
							<ul>
								<?php foreach($benefits as $onebenefit){
									echo "<li>",$onebenefit,"</li>";
								};?>
							</ul>
							<div id="open-con" class="btn contact-button">
								<img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets-pricing/check-circle.svg" alt="">
								<span>Skontaktuj się z nami</span> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="application/javascript">
        document.addEventListener("DOMContentLoaded", init());

        function init(){
            var buttons = {
                closereg: document.getElementById('close-reg'),
				closecon: document.getElementById('close-con'),
				openreg: document.getElementById('open-reg'),
                opencon: document.getElementById('open-con'),
            };
            var regState = {
                opened: false,
			};
			var conState = {
                opened: false,
            };
			var register = document.getElementById('register-over');
			var contact = document.getElementById('contact-over');

            function regopenclose(){
                if(regState.opened == true){
					regState.opened = false,
					register.classList.remove('active');
                } else {
                    regState.opened = true,
					register.classList.add('active');
                }
			}
			function conopenclose(){
                if(conState.opened == true){
					conState.opened = false,
					contact.classList.remove('active');
                } else {
                    conState.opened = true,
					contact.classList.add('active');
                }
            }

            buttons.closereg.addEventListener('click', regopenclose);
			buttons.closecon.addEventListener('click', conopenclose);
			buttons.openreg.addEventListener('click', regopenclose);
            buttons.opencon.addEventListener('click', conopenclose);
        };
    </script>