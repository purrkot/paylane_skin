<?php 
$login = 'merchant-link';
$option_name = 'paylane';
$login = rwmb_meta( $login, array( 'object_type' => 'setting' ), $option_name );
$thanks = 'phone-thanks';
$thanks_content = rwmb_meta( $thanks, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="phone thanks invisible">
    <div class="icon"><img src="<?php echo get_template_directory_uri();?>/learn/check-circle-green.svg" alt="Sukces | PayLane - patnoci elektroniczne"></div>
    <div class="content">
        <p><?php echo $thanks_content;?></p>
    </div>
</div>