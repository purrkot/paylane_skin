        <div class="register_form_component">
            <div class="container">
                    <div class="cloud left">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-register/cloud-left.svg">
                    </div>
                    <div class="cloud right">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-register/cloud-right.svg">
                    </div>
                <div class="row justify-content-center title-column">  
                    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2>Cały świat w zasięgu<br> Twojego biznesu!</h2>
                    </div>  
                </div>
                <div class="row middle-column justify-content-center">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-center">
                        <h3>Zarejestruj się</h3>
                        <div class="register-box">
                            <form class="register-form" id="register-form">
                                <?php echo get_template_part( 'register_form_component/register_thanks' );?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><img src="<?php echo get_template_directory_uri(); ?>/assets-register/user.svg"></span>
                                        </div>
                                        <input type="text"  placeholder="Imię i Nazwisko" class="form-control name-lastname-input" required data-validation="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend"><img src="<?php echo get_template_directory_uri(); ?>/assets-register/mail.svg"></span>
                                        </div>
                                        <input type="email" placeholder="E-mail" class="form-control email-input" required data-validation="required">
                                    </div>
                                </div>
                                <div id="promo_group" class="form-group code-group">
                                    <span>Mam kod promocyjny</span>
                                    <div class="input-group promo-icon">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><img src="<?php echo get_template_directory_uri(); ?>/assets-register/promo.svg"></span>
                                        </div>
                                        <input id="promo" type="text" placeholder="Kod Promocyjny" class="form-control promocode-input" >
                                    </div>
                                </div>
                                <div class=" text-center form-group submit-form">
                                <?php 
                                    $privacy_id = 'privacy-link';
                                    $terms_id = 'terms-link';
                                    $captcha = 'captcha';
                                    $option_name = 'paylane';
                                    $post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
                                    $post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
                                    $site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
								?>
                                    <div class="terms">Akceptuję <a href="<?php echo get_the_permalink($post_id_2);?>">regulamin</a> i <a href="<?php echo get_the_permalink($post_id);?>">politykę prywatności</a></div>
                                    <input type="hidden" class="formtype-input" value="registration">
                                    <div class="g-recaptcha" data-sitekey="<?php echo $site_key;?>"></div>
                                    <button type="submit" class="btn submit submit-button"><img src="<?php echo get_template_directory_uri(); ?>/assets-register/check-circle.svg">Zarejestruj się</button>
                                </div>
                                <div class="validation-errors">
                                </div>
                            </form>
                            <img class="rocket-overlay" src="<?php echo get_template_directory_uri(); ?>/assets-register/rocket.svg">
                        </div>
                        <div class="text-center recommendation-box">
                        <?php
                            $args = array(
                                'post_type' => 'klient',
                                'posts_per_page' => 1,
                                'orderby'=>'rand',
                                'meta_query' => array( 
                                    array( 
                                        'key'     => 'client-photo',
                                        'compare' => 'EXISTS'
                                    ),
                                    array( 
                                        'key'     => 'client-test',
                                        'compare' => 'EXISTS'
                                    )
                                )
                                );
                        
                            query_posts($args);
                            if (have_posts() ) : while (have_posts() ) : the_post(); ?>				
                                <img class="img-fluid" src="<?php echo rwmb_meta('client-photo')['url'];?>">
                                    <p class="s1"><?php echo rwmb_meta('client-test');?></p>
                                    <p class="s2"><?php echo rwmb_meta('testimonial-name');?></p>
                                    <p class="s3"><?php the_title();?></p>								
                        <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                        </div>
                    </div>
                </div>
                    <div class="logo-block">
                    <div class="row justify-content-center">
                        <div class="col-12 text-center">
                            <h5>Zaufali nam</h5>
                        </div>
                        <div class="col-12 text-center">
                            <div class="row justify-content-center align-content-center align-items-center">
                                <?php
                                $args = array(
                                    'post_type' => 'klient',
                                    'posts_per_page' => 4,
                                    'orderby'=>'rand'
                                    );
                                query_posts($args);
                                if (have_posts() ) : while (have_posts() ) : the_post(); ?>	
                                    <img class="img-fluid client" src="<?php echo get_the_post_thumbnail_url();?>">
                                <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
