        <div class="register_form_component">
            <div class="container">
                    <div class="cloud left">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-register/cloud-left.svg">
                    </div>
                    <div class="cloud right">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-register/cloud-right.svg">
                    </div>
                <div class="row justify-content-center title-column">  
                    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2>Cały świat w zasięgu<br> Twojego biznesu!</h2>
                    </div>  
                </div>
                <div class="row middle-column justify-content-center">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-center sales-wrapper">
                        <h3>Zadzwonimy do Ciebie</h3>
                        <div class="register-box noborder">
                                <?php 
								$privacy_id = 'privacy-link';
                                $terms_id = 'terms-link';
                                $captcha = 'captcha';
								$option_name = 'paylane';
								$post_id = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
                                $post_id_2 = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
                                $site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
								?>
                                <form action="" class="sales-form text-center">
                                    <div class="phone-icon">
                                        <input type="tel" class="phone" placeholder="Numer telefonu">
                                    </div>
                                    <p class="text-center" >Akceptuję <a href="#">politykę prywatności</a></p>
                                    <input type="hidden" class="formtype-input" value="sales-contact">
                                    <div class="g-recaptcha" data-callback="contactRecaptcha1" data-sitekey="<?php echo $site_key;?>"></div>
                                    <button type="submit" class="submit submit-button" value="Dodaj numer telefonu">Zostaw swój numer telefonu</button>
                                </form>
                            <img class="rocket-overlay" src="<?php echo get_template_directory_uri(); ?>/assets-register/rocket.svg">
                        </div>
                        <div class="text-center recommendation-box">
                        <?php
                            $args = array(
                                'post_type' => 'klient',
                                'posts_per_page' => 1,
                                'orderby'=>'rand',
                                'meta_query' => array( 
                                    array( 
                                        'key'     => 'client-photo',
                                        'compare' => 'EXISTS'
                                    ),
                                    array( 
                                        'key'     => 'client-test',
                                        'compare' => 'EXISTS'
                                    )
                                )
                                );
                        
                            query_posts($args);
                            if (have_posts() ) : while (have_posts() ) : the_post(); ?>				
                                <img class="img-fluid" src="<?php echo rwmb_meta('client-photo')['url'];?>">
                                    <p class="s1"><?php echo rwmb_meta('client-test');?></p>
                                    <p class="s2"><?php echo rwmb_meta('testimonial-name');?></p>
                                    <p class="s3"><?php the_title();?></p>								
                        <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                        </div>
                    </div>
                </div>
                    <div class="logo-block">
                    <div class="row justify-content-center">
                        <div class="col-12 text-center">
                            <h5>Zaufali nam</h5>
                        </div>
                        <div class="col-12 text-center">
                            <div class="row justify-content-center align-content-center align-items-center">
                                <?php
                                $args = array(
                                    'post_type' => 'klient',
                                    'posts_per_page' => 4,
                                    'orderby'=>'rand'
                                    );
                            
                                query_posts($args);
                                if (have_posts() ) : while (have_posts() ) : the_post(); ?>	
                                    <img class="img-fluid client" src="<?php echo get_the_post_thumbnail_url();?>">
                                <?php endwhile; endif; wp_reset_postdata();wp_reset_query();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
