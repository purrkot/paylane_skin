document.addEventListener("DOMContentLoaded", function(){
    var promo ="utm_promo";
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
      };

      var promo_code = getURLParameter(promo),
      promo_group = document.getElementById('promo_group'),
      promo_field = document.getElementById('promo');

      function InitPromoCode(){
      if(promo_code === null){

      } else {
          promo_group.style.visibility = "hidden";
          promo_group.style.height = "0px";
          promo_field.value = promo_code.toString();
      }
    };
    InitPromoCode();

});


jQuery(document).ready(function ($) {
    let RegisterFormComponent = function(){
        var self=this;
        
        //init events
        $('.register-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData();
        });

        $('#register-form').on('validation', function(evt, valid){
            self.valid = valid;
        });

        $('.register-form .code-group span').on('click',function(e){
            $('.register-form .promo-icon').toggleClass('visible');
        });

    }
    
    RegisterFormComponent.prototype.sendFormData = function(){
        var self = this;
        if(self.valid){
            let namelastname = $('.register-form .name-lastname-input').val();
            let email = $('.register-form .email-input').val();
            let promocode = $('.register-form .promocode-input').val();
            let formtype = $('.register-form .formtype-input').val();
            let g_recaptcha_response = grecaptcha.getResponse();
            $.post('/ajax.php',{
                'formtype': formtype, 
                'name': namelastname, 
                'email': email, 
                'promocode': promocode,
                'g-recaptcha-response': g_recaptcha_response
            } ).fail(function(xhr, status, error) {
                    console.log(error);
                    console.log(status);
            }).done(function(data){
                    data = JSON.parse(data);
                    console.log('Done');
                    if(data.info==200){
                        $('.register-box .thanks').removeClass('invisible');
                    }else{
                        $('.register-form .validation-errors').empty().append(data.errormessage);
                    }

            });
        }
    }


    //init
    let oRegisterFormComponent = new RegisterFormComponent();
    $.validate({
        form : '#register-form',
        lang: 'pl',
        modules : 'html5',
    });
});