var response1
var contactRecaptcha1 = function(a){
    response1 = a
}

var resetCaptcha = function(){
    try{
        grecaptcha.reset(0)
        grecaptcha.reset(1)
    }catch(e){}
}

jQuery(document).ready(function ($) {
    let ContactFormComponent = function(){
        var self=this;
        //init events
        $('.sales-form .submit-button').on('click',function(e){
            e.preventDefault();
            self.sendFormData(e);
        });

    }
    
    ContactFormComponent.prototype.sendFormData = function(e){

        let phone = $('.sales-form .phone').val();
        let formtype = $('.sales-form .formtype-input').val();
        $.post('/ajax.php',{
            'formtype': formtype, 
            'phone': phone,
            'g-recaptcha-response': response1
        } ).done(function(data){
            resetCaptcha()
            data = JSON.parse(data);
            if(data.info==200){
                $('.sales-form .validation-errors').empty().append('Wiadomość wysłana pomyślnie');
            }else{
                $('.sales-form .validation-errors').empty().append(data.errormessage);
            } 
        })

    }


    //init
    let oContactFormComponent = new ContactFormComponent();
    $.validate({
        form : '.sales-form',
        lang: 'pl',
        modules : 'html5',
    });
});