<div id="payments" class="logo_rotator_component glide">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
        <div class="glide__slide">
            <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_ing.svg">
        </div>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_barclays.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_idea.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_paypal.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_mastercard.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_sofort.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_applepay.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_creditagricole.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_ing.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_barclays.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_idea.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_paypal.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_mastercard.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_sofort.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_applepay.svg">
            </li>
            <li class="glide__slide">
                <img src="<?php echo get_template_directory_uri(); ?>/assets-home/bank_creditagricole.svg">
            </li>
        </ul>
    </div>
</div>

<script>
var glide = new Glide('.glide', {
    autoplay: 3000,
    animationDuration: 750,
    animationTimingFunc: 'ease-in-out',
  type: 'carousel',
  perView: 7,
  breakpoints: {
    1440: {
      perView: 5
    },
    960: {
      perView: 3
    },
    500: {
      perView: 2
    }
  }
});

glide.mount();
</script>