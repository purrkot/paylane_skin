<?php 
/**
 * Only use component inside the blog-single post loop
 * Don't render component outside the blog-single post loop as it will break apart
 */
?>

<div class="blog_full_image_title_header container-fluid" style="background-image: url('<?php the_post_thumbnail_url("full");?>');">
    <div class="cover">
        <div class="col-12 text-center align-self-center">
            <div class="container1440 container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="title"><?php the_title();?></h2>
                    </div>
                </div>  
            </div>
        </div>

        <div class="author-wrapper text-center">
            <div class="date"><?php the_date();?></div>

            <div class="author" style="background:url(<?php echo get_wp_user_avatar_src($user_id, 'original'); ?>);"></div>

            <div class="name"><?php the_author();?></div>
        </div>

    </div>
</div>
