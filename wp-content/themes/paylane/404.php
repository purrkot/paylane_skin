<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage paylane 0.9
 * @since paylane 0.1
 */

get_header(); ?>
	<div class="container 404 errors">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-6 text-right">
                <img src="<?php echo get_template_directory_uri();?>/assets-errors/404.svg" alt="PayLane - Płatności elektroniczne">
            </div>
            <div class="col-12 col-sm-6 col-md-6 galaxy-title">
                <h2>Ups...!<br>To koniec trasy!</h2>
            </div>
            <div class="col-12 text-center galaxy-claim">
                <p>Naciśnij przycisk start aby przenieść się do innej galaktyki</p>
            </div>
            <div class="col-12 galaxy">
                <img src="<?php echo get_template_directory_uri();?>/assets-errors/space.svg" alt="PayLane - Płatności elektroniczne">
                <div class="start btn btn-fixed-medium btn-blue">Start</div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>