<?php 
echo get_header();

echo get_template_part("home_rainbow_jumbotron_component/home_rainbow_jumbotron_component");
echo get_template_part("home_business_integrations_component/home_business_integrations_component");
echo get_template_part("home_separator_dotted/home_separator_dotted");
echo get_template_part("home_world_range_component/home_world_range_component");
echo get_template_part("home_separator_dotted/home_separator_dotted");
echo get_template_part("home_api_intro_component/home_api_intro_component");

echo get_template_part("home_advantages_component/home_advantages_component");
echo get_template_part("home_newsletter_component/home_newsletter_component");

echo get_footer();
?>