<?php 
$register = 'register-link';
$option_name = 'paylane';
$register_id = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="main-menu">
    <div class="psycat" style="background:url('<?php echo get_template_directory_uri();?>/top_menu_component/dziubek.svg')"></div>
    <div class="container1440">
            <div class="top_menu_component">
                    <a href="<?php echo get_home_url();?>" class="logo">
                        <img src="<?php echo rwmb_meta( 'paylane-logo', array( 'object_type' => 'setting' ), 'paylane' );?>">
                    </a>
                    <nav class="top-nav d-none d-xs-none d-sm-none d-md-none d-lg-flex">
                        <?php wp_nav_menu( 
                                array( 
                                    'theme_location' => 'primary-menu'
                                ) 
                            ); ?>
                    </nav>

                    <nav class="tablet-nav d-xs-block d-lg-none">
                        <div class="menu">
                            <a class="btn btn-yellow d-none d-md-inline-block btn-small " href="http://merchant.paylane.com">Zaloguj się</a>
                            <a class="btn btn-blue d-none d-md-inline-block btn-small " href="<?php the_permalink($register_id);?>">Załóź konto</a>
                            <div id="burger">
                                <div class="line1"></div>
                                <div class="line2"></div>
                            </div>
                        </div>
                    </nav>
                </div>
    </div>
</div>
<style>
    .tablet_nav .sub-menu:after, .menu-item:after {background: url('<?php echo get_template_directory_uri();?>/top_menu_component/menu-stripe.png');}
    .top-nav .menu .sub-menu::after{background: url('<?php echo get_template_directory_uri();?>/top_menu_component/subicons.svg');}
    .menu-item a:after {background: url('<?php echo get_template_directory_uri();?>/top_menu_component/chevrons-right.svg');}
</style>
<div id="drawer" class="drawer <?php if(is_admin_bar_showing()) {echo "admin-bar";}; ?>">
<?php wp_nav_menu( 
        array( 
            'theme_location' => 'tablet-menu'
        ) 
    ); ?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center copyright">©PayLane 2018</div>
        </div>
    </div>
</div>
<script>
    jQuery( document ).ready(function($) {
        var $plusdiv = $( "<div class='plus' style='background:url(<?php echo get_template_directory_uri();?>/top_menu_component/plus.svg)'></div>" );
            $(".menu li").append($plusdiv);
    });
</script>