/**
 * Toggle active on clicked plus button
 * @param object domObject 
 */
function plusClickedToggleAction(domObject){
    domObject.classList.toggle('active');
    let submenu = domObject.closest("li").getElementsByClassName('sub-menu');
    if (submenu.length) {
        submenu[0].classList.toggle('active');
    }
}
/**
 * Removes every active classes from buttons and submenus but excluded clicked item
 * @param object domObjectsCollection collection of plus objects to remove active class
 * @param object clickedDomObject  object that will be excluded from operation (clicked plus button)
 */
function deactivateActivatedPluses(domObjectsCollection,clickedDomObject){
    for(let i=0;i<domObjectsCollection.length;i++){
        if( domObjectsCollection[i]!=clickedDomObject && domObjectsCollection[i].classList.contains('active') ){
            domObjectsCollection[i].classList.remove('active');
            if(domObjectsCollection[i].closest("li").getElementsByClassName('sub-menu').length){
                domObjectsCollection[i].closest("li").getElementsByClassName('sub-menu')[0].classList.remove('active');
            }

        }
    }
}

function activateMenuActions() {
    //burger activation
    document.getElementById('burger').addEventListener('click', function(event) {
        document.getElementById('burger').classList.toggle('active');
        document.getElementById('drawer').classList.toggle('active');
    }, false);

    //.plus button visibility and event activation
    let plusitems = document.getElementById('menu-tablet-menu').getElementsByClassName('plus');
    for (let i = 0; i < plusitems.length; i++) {
        //if got submenu
        if (plusitems[i].closest('li').getElementsByClassName('sub-menu').length === 0) {
            plusitems[i].classList.add('hidden');
        } else {
            plusitems[i].addEventListener('click', function(event) {
                //toggle clicked item
                plusClickedToggleAction(event.target);
                //deactivate active buttons but clicked target
                deactivateActivatedPluses(plusitems,event.target);
                
            });
        }

    }
}

document.addEventListener("DOMContentLoaded", function(event) {

    activateMenuActions();

});