<?php
    get_header();
    echo get_template_part("full_image_title_header/full_image_title_header");
    echo get_template_part("blog_submenu_component/blog_submenu_component");
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $loop = new WP_Query(array('post_type'=>'post', 'posts_per_page'=>5, 'paged'=>$paged,));
    if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo get_template_part("blog_preview_component/blog_preview_component");
    endwhile;
        echo "<div class='container container1440'><div class='row justify-content-center'><div class='col-sm-12 col-md-12 col-lg-7 pagination blog'>";
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '',
            'next_text'    => sprintf( '%1$s <i></i>', __( 'Starsze', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        echo "</div></div></div>";
    endif;
    get_footer('wave');?>