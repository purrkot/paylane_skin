<?php 
/**
 * Template Name: FAQ
 *
 */
get_header('arrow');
    echo get_template_part('faq_header_component/faq_header_component');
    echo get_template_part('faq_section_viewer_component/faq_section_viewer_component');
    echo get_template_part('faq_call_us_component/faq_call_us_component');
get_footer('faq');
?>