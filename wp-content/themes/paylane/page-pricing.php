<?php 
/**
 * Template Name: Cennik
 *
 */
get_header();        
	echo get_template_part('pricing_header_component/pricing_header_component');
	echo get_template_part('pricing_train_component/pricing_train_component');
	echo get_template_part('pricing_logo_rotator_component/pricing_logo_rotator_component');
	echo get_template_part('pricing_footer_component/pricing_footer_component');
get_footer('wave');?>