<div class="container oferta_promo">
    <div class="row justify-content-center">
        <?php $args = array(
                        'post_type' =>  'oferta',
                        'posts_per_page' => 3,
                        'orderby' =>  'rand',
                    );
            $promo = new WP_Query($args);
            if ($promo->have_posts() ) : while ($promo->have_posts() ) : $promo->the_post();?>
        <div class="col-sm-12 col-md-4 promo-wrapper">
            <div class="waves">
                <img class="wave" src="<?php echo get_template_directory_uri();?>/oferta_single/background-small-wave-grey.svg" alt="">
                <?php $file = rwmb_meta('svg');
                        $url = "url";
                    foreach($file as $single) {
                        echo "<img class='icon' src='$single[$url]'/> ";
                    }
                ?>
            </div>
            <div class="col-12 col-sm-12">
                <h5><?php the_title();?></h5>
            </div>
            <div class="col-12 col-sm-12">
                <p><?php echo strip_tags(get_the_excerpt());?></p>
            </div>
            <div class="col-12 col-sm-12">
                <h5><a href="<?php the_permalink();?>">Dowiedz się więcej<span class="chevron"><img src="<?php echo get_template_directory_uri();?>/oferta_single/chevrons-right.svg" alt="chevron"></span></a></h5>
            </div>
        </div>
        <?php endwhile;endif; wp_reset_postdata();?>
    </div>
</div>


