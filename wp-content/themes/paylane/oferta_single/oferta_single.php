<div class="container oferta_single">
    <div class="row">
        <div class="cloud left"><img src="<?php echo get_template_directory_uri();?>/oferta_single/cloud-right.svg" alt=""></div>
        <div class="cloud right"><img src="<?php echo get_template_directory_uri();?>/oferta_single/cloud-left.svg" alt=""></div>
        <div class="col-12 col-sm-12 text-center icon-header">
            <?php $file = rwmb_meta('svg');
                    $url = "url";
                foreach($file as $single) {
                    echo "<img src='$single[$url]'/> ";
                }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <h1 class="title"><?php the_title();?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="goback" id="goback">Wstecz</div>
            <div class="separator-gradient--full-width"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-4">
            <div class="story">
            <div class="oval" style="background:url(<?php echo rwmb_meta('story-photo')['url'];?>);"></div>
                <div class="story-text">
                    <p class="name"><?php echo rwmb_meta('story-name');?></p>
                    <p class="position"><?php echo rwmb_meta('story-position');?></p>
                    <p class="story"><?php echo rwmb_meta('story-story');?></p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 content">
            <?php the_content();?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-2">
            <div class="social-wrapper">
                <a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/twitter.svg"></a>
                <img class="social-icon" id="facebook-share-<?php the_ID();?>" src="<?php echo get_template_directory_uri(); ?>/assets-blog/facebook.svg">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/linked-in.svg"></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="separator-gradient--full-width"></div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(){
        var backward = document.getElementById('goback');
        backward.addEventListener('click', function(){
            window.history.back();
        });
    });
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '1918887535015636',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>