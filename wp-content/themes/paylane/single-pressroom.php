<?php
    get_header();
    /**
     * Correct wordpress content loop without double content rendering
     */
    
    if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo get_template_part('press/press_title_header');
        echo get_template_part('blog_single_content/blog_single_content');
    endwhile; endif; wp_reset_postdata();
    /*
    * Featured Loop
    */
    echo get_template_part('blog_featured/loop-featured');
    /**
     * Footer component
     */
    get_footer('wave');
?>