<?php
/*
* Render component inside the loop
*/

//Check if pressroom - common single.php file, render conditionally download files
if ( 'pressroom' == get_post_type() ) { 
    $doc = rwmb_meta('doc');
    $zip = rwmb_meta('zip');
    $xls = rwmb_meta('xls');
    $pdf = rwmb_meta('pdf');
    $doc_name = rwmb_meta('doc-name');
    $zip_name = rwmb_meta('zip-name');
    $xls_name = rwmb_meta('xls-name');
    $pdf_name = rwmb_meta('pdf-name');
}
?>

<div class="container blog-content">
    <div class="row">
    <?php if ( 'post' == get_post_type() ) { ?>
        <div class="col-lg-1"></div>
        <div class="col-sm-12 col-lg-9">
            <div class="goback" id="goback">Wstecz</div>
        </div>
    <?php };?>
    <?php if ( 'pressroom' == get_post_type() ) { ?>
        <div class="separator-grey--full-width top"></div>
    <?php } else {;?>
        <div class="separator-gradient--full-width top"></div>
    <?php };?>

    </div>
    <div class="row justify-content-center">
    <?php if ( 'pressroom' == get_post_type() ) { ?>
        <?php if( !empty($doc)|| !empty($pdf) || !empty($zip) || !empty($xls)){ ?>
        <div class="col-sm-12 col-md-3 col-lg-3 text-center file-download">
            <h5 class="files">Pobierz pliki:</h5>
            <?php if(!empty($zip)){?><p><a href="<?php echo $zip;?>" download><img src="<?php echo get_template_directory_uri();?>/press/zip.svg" alt="Pobierz plik ZIP"><?php echo $zip_name;?></a></p><?php };?>
            <?php if(!empty($pdf)){?><p><a href="<?php echo $pdf;?>" download><img src="<?php echo get_template_directory_uri();?>/press/pdf.svg" alt="Pobierz plik PDF"><?php echo $pdf_name;?></a></p><?php };?>
            <?php if(!empty($doc)){?><p><a href="<?php echo $doc;?>" download><img src="<?php echo get_template_directory_uri();?>/press/doc.svg" alt="Pobierz plik DOC"><?php echo $doc_name;?></a></p><?php };?>
            <?php if(!empty($xls)){?><p><a href="<?php echo $xls;?>" download><img src="<?php echo get_template_directory_uri();?>/press/xls.svg" alt="Pobierz plik XLS"><?php echo $xls_name;?></a></p><?php };?>
        </div>
        <?php };?>
    <?php };?>
    <?php if ( 'post' == get_post_type() ) { ?>
        <div class="col-lg-1"></div> 
    <?php };?>
        <article class="col-sm-12 col-lg-9">
        <?php if(has_post_format('video')){ 
            $mp4 = rwmb_meta('video-mp4');
            $ogg = rwmb_meta('video-ogg');
            $webm = rwmb_meta('video-webm');?>
                    <video class="video-player video-js" controls>
                        <?php if(!empty($mp4)){ ?>
                        <source src="<?php echo $mp4;?>" type="video/mp4">
                        <?php };?>
                        <?php if(!empty($ogg)){ ?>
                        <source src="<?php echo $ogg;?>" type="video/ogg">
                        <?php };?>
                        <?php if(!empty($ogg)){ ?>
                        <source src="<?php echo $webm;?>" type="video/webm">
                        <?php };?>
                    </video>
        <?php };?>
        
            <?php the_content();?>
        </article>
        <?php if ( 'post' == get_post_type() ) { ?>
            <div class="col-sm-12 text-center col-lg-2">
                <div class="social-wrapper">
                <a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/twitter.svg"></a>
                <img class="social-icon" id="facebook-share-<?php the_ID();?>" src="<?php echo get_template_directory_uri(); ?>/assets-blog/facebook.svg">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/linked-in.svg"></a>
                </div>
            </div>
        <?php };?>
    </div>
    <?php if ( 'pressroom' == get_post_type() ) { ?>
        <div class="separator-grey--full-width bottom"></div>
    <?php } else {;?>
        <div class="separator-gradient--full-width bottom"></div>
    <?php };?>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(){
        var backward = document.getElementById('goback');
        backward.addEventListener('click', function(){
            window.history.back();
        });
        videojs(document.querySelector('.video-player'));
    });
</script>
<?php if ( 'post' == get_post_type() ) { ?>
 <script>
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '1918887535015636',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>
<?php };?>