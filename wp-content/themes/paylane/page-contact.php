<?php 
/**
 * Template Name: Kontakt
 *
 */
get_header();?>
<div class="overflowed">
<div class="contact-wrapper container">
        <div class="row justify-content-center">
            <?php echo get_template_part('contact/contact-title');?>
        </div>
        <div class="row">
            <?php echo get_template_part('contact/contact-form');?>
        </div>
        <div class="row">
            <?php echo get_template_part('contact/contact-box');?>
        </div>
</div>
</div>
<?php get_footer();?>