
<div class="blog_submenu_component">
    <div class="container1440 container">
        <div class="row">
            <div class="col-12">
            <?php   $taxonomy     = 'category';
                    $orderby      = 'name'; 
                    $show_count   = false;
                    $pad_counts   = false;
                    $hierarchical = true;
                    $title        = '';
                    
                    $args = array(
                    'taxonomy'     => $taxonomy,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty' => false,
                    'depth' => 1,
                    );
            ?>
 
                <ul class="blog-menu">
                    <?php wp_list_categories( $args ); ?>
                </ul>
                <div class="text-center wings-separator--single d-block d-sm-block d-md-none">
                    <img src="<?php echo get_template_directory_uri(); ?>/blog_submenu_component/menu-stripe.png" alt="Pasek, separator | PayLane - Płatności elektroniczne">
                </div>
                <div class="text-center wings-separator d-none d-sm-none d-md-block">
                <?php if(is_category(32)){ ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-blog/wings-grey.svg" alt="Separator | PayLane - Płatności elektroniczne">
                    <?php } else { ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-blog/stripes.svg" alt="Separator | PayLane - Płatności elektroniczne">
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
