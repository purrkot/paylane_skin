<?php 
    $merchant_claim = rwmb_meta('merchant-claim');
    $merchant_claim2 = rwmb_meta('merchant-claim-2');
    $merchant_claim3 = rwmb_meta('merchant-claim-3');
    $merchant_claim4 = rwmb_meta('merchant-claim-4');
    $merchant_claim5 = rwmb_meta('merchant-claim-5');
    $merchant_content = rwmb_meta('merchant-content-1');
    $merchant_content2 = rwmb_meta('merchant-content-2');
    $merchant_benefits = rwmb_meta('merchant-benefit');

    $merchant_screen1 = rwmb_meta('merchant-screen-1', array(array( 'size' => 'full' )));
    $merchant_screen2 = rwmb_meta('merchant-screen-2', array(array( 'size' => 'full' )));

    $option_name = 'paylane';
    $register = 'register-link';
    $register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="offer_merchant_panel_component overflowed">
    <div class="merchant-top-section">
    <img class="circle-right" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/circle-blue.svg">
        <div class="container container1440">
            <div class="row title">
                <div class="col-lg-12 text-center">
                    <h2><?php echo $merchant_claim;?></h2>
                </div>
            </div>
            <div class="row button">
                <div class="col-lg-12 text-center">
                    <div class="merchant"><?php the_title();?></div>
                    <img class="cloud-left" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/cloud-left.svg">
                    <img class="cloud-right" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/cloud-right.svg">
                </div>
            </div>
            <div class="row preview">
                <div class="col-lg-12 text-center">
                    <img class="macbook-screen img-fluid" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/macbook.svg">
                    <img class="img-fluid steering-wheel" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/steering-wheel-blue.svg">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="button-bar center">
                        <a href="#wiecej" class="btn btn-yellow btn-fixed-medium">Dowiedz się więcej</a>
                        <span>albo</span>
                        <a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-fixed-medium">Wypróbuj</a>
                    </div>
                </div>
            </div>
            <div id="wiecej" class="row screenshot">
                <div class="col-lg-4 offset-lg-2">
                    <h3><?php echo $merchant_claim2;?></h3>
                    <p><?php echo $merchant_content;?></p>
                </div>
                <div class="col-lg-6">
                    <img class="img-fluid dash" src="<?php echo $merchant_screen1['full_url']; ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="merchant-bottom-section">
    <img class="circle-left" src="<?php echo get_template_directory_uri();?>/assets-offer-merchant-panel/circle-red.svg">
        <div class="container container1440 ">
            <div class="row file-export justify-content-center">
                <div class="col-6  col-sm-6 col-md-4 col-lg-2 extension">
                    <img class="img-fluid csv" src="<?php echo get_template_directory_uri(); ?>/assets-offer-merchant-panel/svg.svg">
                    
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2 extension">
                    <img class="img-fluid pdf" src="<?php echo get_template_directory_uri(); ?>/assets-offer-merchant-panel/pdf.svg">   
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 d-flex align-items-center text-center text-sm-center text-md-left export-copy">
                    <span><?php echo $merchant_claim3;?></span>
                </div>
                <div class="d-none d-sm-none d-md-none d-lg-inline-block col-12 col-sm-12 col-md-4 col-lg-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets-offer-merchant-panel/gondola.svg">
                </div>
            </div>
            <div class="row desktop-panel justify-content-center align-items-center">
                <div class="col-lg-12 text-center">
                    <h3><?php echo $merchant_claim4;?></h3>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-right">
                    <img class="img-fluid dash" src="<?php echo $merchant_screen2['full_url']; ?>">
                </div>
                <div class="col-8 col-sm-6 col-md-6 col-lg-6">
                    <ul>
                        <?php foreach ($merchant_benefits as $benfit){
                            echo "<li>",$benfit,"</li>";
                        };?>
                    </ul>
                    <img class="realtime" src="<?php echo get_template_directory_uri(); ?>/assets-offer-merchant-panel/realtime.svg">
                </div>
            </div>
            <div class="row mobile-panel justify-content-center">
                <div class="col-12 col-sm-12 col-md-6 col-lg-5 content">
                    <h3><?php echo $merchant_claim5;?></h3>
                    <p><?php echo $merchant_content2;?></p>
                    <div class="button-bar center">
                        <a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-fixed-medium">Załóż konto</a>
                        <span>albo</span>
                        <a href="<?php the_permalink($register_url);?>" class="btn btn-yellow btn-fixed-medium">Wypróbuj</a>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-3 text-center text-sm-center text-md-right text-lg-left phone">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-offer-merchant-panel/minimal-light.png">
                </div>
            </div>
        </div>
    </div>
</div>