<div class="offer_main_comment_component">
    <div class="row">
        <div class="col-12 text-center image-col">
            <div class="image-wrapper text-center">
            <?php 
            $src = get_template_directory_uri().'/assets-home/face.jpg';
            if(isset($photo) && $photo!='' && isset($photo['url']) && $photo['url'] !='' ){
                $src = $photo['url'];
            }
            ?>
                <img class="img-fluid" src="<?php echo $src; ?>">
                
            </div>
        </div>
        <div class="col-12 text-center content-col">
            <div class="content-wrapper">
                <h3><?php echo $name_lastname; ?></h3>
                <h4><?php echo $description; ?></h4>
                <p class="excerpt text-left">
                <?php echo $comment; ?>
                </p>
                <div class="url text-left">
                    <a href="<?php the_permalink();?>">
                    <?php if($link_text!='') {
                        echo $link_text;
                    }else{
                        echo 'Dowiedz się więcej';
                    }   
                    ?>                     
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/arrows-right.svg">
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>