<div class="offer_main_security_component">
    <div class="container container1920">
        <div class="row justify-content-center first">
            <div class="col-lg-8 col-md-6 col-sm-12 col-12 text-center">
                <img class="img-fluid showicon" src="<?php echo get_template_directory_uri() ?>/assets-offer-main/shield-anchoir.svg">
            </div>
        </div>
        <div class="row justify-content-center second">
            <div class="col-lg-8 col-md-6 col-sm-12 col-12 text-center">
                <h2>I wypłyń bezpiecznie na szerokie wody</h2>
                <p>Narzędzia zabezpieczające płatności i indywidualne podejście do klienta dają Ci<br> 
                pewność, spokój i bezpieczeństwo niezbędne podczas prowadzenia biznesu</p>
            </div>
        </div>
        <div class="row justify-content-center third">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <div class="row security-changer">
                    <?php 
                        $securityContent = array();
                        $the_query = new WP_Query(array(
                            'post_type'=>'oferta',
                            'tax_query'=>array(array(
                                'taxonomy'=>'oferta_categories',
                                'field'=>'slug',
                                'terms'=>'bezpieczenstwo'
                            ))
                        ));
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                    
                        array_push($securityContent,array(
                            'url'=>get_the_permalink(),
                            'the_title'=>get_the_title(),
                            'the_excerpt'=>get_the_excerpt(),
                            'ID'=>get_the_id()
                        ));
                    endwhile; endif; 
                    ?>       
                    <div class="col-lg-3 col-md-3 col-sm-12 col-left selectors">
                        <ul>
                            <?php
                                foreach($securityContent as $key=>$value) {
                                    echo '<li class="handler">'.$value['the_title'].'</li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-right">
                            <ul>
                                <?php foreach($securityContent as $key=>$value){ ?>
                                    <div class="table">
                                        <p><?php echo $value['the_excerpt'];?></p>
                                        <div>
                                            <a href="<?php echo $value['url'];?>"></a>
                                        </div>
                                            <img class="idiconsvg" src="<?php echo reset(rwmb_meta('svg',array(),$value['ID']))['url']; ?>">
                                    </div>  
                                <?php };?>
                            </ul>
                        </div> 
                </div>
            </div>
        </div>
        <div class="row justify-content-center fourth">
            <div class="col-lg-8 col-md-8 col-sm-12 col-12 text-center">
                <div class="button-bar center">
                    <a class="btn btn-blue btn-medium">Załóż konto</a>
                    <span>albo</span>
                    <a class="btn btn-yellow btn-medium">Skontaktuj się z nami</a>
                </div>
            </div>
        </div>

    </div>
</div>