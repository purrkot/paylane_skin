<?php 
/**
 * Metaboxes for backend rendering
 */
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="offer_main_intro_component">
    <div class="container container1920 text-center">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-10 text-lg-left text-xs-center">
                <h2 class="offer-title-bold">Z naszą ofertą wypłyniesz na szerokie wody</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-10 col-sm-12 text-left">
                <div class="separator">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-10 col-sm-12 text-left">
                <div class="description text-left">
                    Naszym celem jest tworzenie rozwiązań idealnie dobranych pod Twój model biznesowy. Od lat pomagamy firmom śmiało płynąć po
                    oceanie możliwości, jakie tworzą nowoczesne rozwiązania w płatnościach online. Sprawdż, jak wiele rozwiązań
                    może wesprzeć Twój biznes.
                </div>
                <div class="button-bar center">
                    <a href="#integracje" class="btn btn-yellow btn-medium">Dowiedz się więcej</a>
                        <span>albo</span>
                    <a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-medium">Załóż konto</a>
                </div>
            </div>
        </div>
    </div>

</div>