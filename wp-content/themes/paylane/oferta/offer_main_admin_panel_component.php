<div class="offer_main_admin_panel_component">
    <div class="container container1920">
        <div class="row justify-content-center first">
            <div class="col-lg-8 col-md-12 text-center">
                <img class="img-fluid showicon" src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/Steering_wheel.svg">
            </div>
        </div>
        <div class="row justify-content-center second">
            <div class="col-lg-8 col-md-12 text-center">
                <h2>Więc trzymaj kurs!</h2>
                <h3>Nowoczesny panel administracyjny i rozbudowane opcje raportowe to pełna kontrola dla Twojego biznesu.</h3>
            </div>
        </div>
                <?php $the_query = new WP_Query(array(
                        'post_type'=>'oferta',
                        'orderby' => 'publish_date',
                        'order' => 'ASC',
                        'tax_query'=>array(array(
                            'taxonomy'=>'oferta_categories',
                            'field'=>'slug',
                            'terms'=>'admin-merchant-and-notifications'
                        ))
                    ));
                ?>
                <?php 
                $key=0;
                if ( $the_query->have_posts() ){
                        while ( $the_query->have_posts() ) {
                        $the_query->the_post(); 
                        if($key==0):?>
                        <div class="row justify-content-center third">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-12 macbook text-sm-center text-md-center text-lg-right
                            ">
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/macbook.svg">
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-12 panel text-sm-center text-md-center text-lg-left panel-feature">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt() ?>
                                <div class="button-bar left">
                                    <a href="<?php the_permalink();?>" class="btn btn-yellow btn-fixed-medium">Dowiedz się więcej</a>
                                    <span>albo</span>
                                    <a href="<?php the_permalink();?>" class="btn btn-blue btn-fixed-medium">Załóż konto</a>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center fourth">
                        <?php endif; ?>
                        <?php if($key==1):?>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-12 text-right panel-feature">
                            <?php 
                                foreach (rwmb_meta('svg') as $k => $v) {
                                    echo '<img class="img-fluid" src="'.$v['url'].'">';
                                }
                            ?>
                            <h3><?php the_title(); ?></h3>
                            <div><?php the_excerpt(); ?></div>
                            <a href="<?php the_permalink();?>" class="btn btn-yellow btn-fixed-medium">Dowiedz się więcej</a>
                        </div>
                        <?php endif; ?>
                        <?php if($key==2):?>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-12 text-left panel-feature">
                        <?php 
                                foreach (rwmb_meta('svg') as $k => $v) {
                                    echo '<img class="img-fluid" src="'.$v['url'].'">';
                                }
                            ?>
                            <h3><?php the_title(); ?></h3>
                            <div><?php the_excerpt(); ?></div>
                            <a href="<?php the_permalink();?>" class="btn btn-blue btn-fixed-medium">Wypróbuj</a>
                        </div>

                        <?php endif;
                        $key++;
                        }
                }

                if($key>1){
                    echo '</div>';
                }
                
                ?>
    </div>
</div>
