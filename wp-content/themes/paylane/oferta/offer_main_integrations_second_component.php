<?php 
/**
 * Metaboxes for backend rendering
 */
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$payments_url = rwmb_meta( $payments, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="offer_main_integrations_second_component">
    <div class="container container1920">
        <div class="row justify-content-center integrations-header">
            <div class="col-lg-4 col-md-6 align-self-end content">
                <img class="twoclouds" src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/2clouds.svg">
                <h2>Komfortowe dla Twojego klienta</h2>
                <h3>Płatności mogą być przyjemne! Zobacz, jak sprawić by korzystanie z Twojego sklepu stało się jeszcze wygodniejsze</h3>
            </div>
            <div class="col-lg-4 col-md-6 boat text-center text-sm-center text-md-left">
                <img class="img-fluid showicon" src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/Yacht.svg">
            </div>
        </div>
        <div class="row justify-content-center integrations-middle">
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    <?php 
                        $the_query = new WP_Query(array(
                            'post_type'=>'oferta',
                            'tax_query'=>array(array(
                                'taxonomy'=>'oferta_categories',
                                'field'=>'slug',
                                'terms'=>'komfort'
                            ))
                        ));
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                        get_template_part('oferta/offer_main_preview_article_component');
                    endwhile; 
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <?php
                        $tmpTerm = get_term_by('name', 'komfort', 'oferta_categories');
                        $name_lastname = rwmb_meta('name_lastname',array('object_type'=>'term'),$tmpTerm->term_id);
                        $description = rwmb_meta('description',array('object_type'=>'term'),$tmpTerm->term_id);
                        $comment = rwmb_meta('comment',array('object_type'=>'term'),$tmpTerm->term_id);
                        $photo = rwmb_meta('photo',array('object_type'=>'term'),$tmpTerm->term_id);
                        $link_text = rwmb_meta('link_text',array('object_type'=>'term'),$tmpTerm->term_id);
                        $link_url = rwmb_meta('link_url',array('object_type'=>'term'),$tmpTerm->term_id);
                        if($tmpTerm!='' && $name_lastname!='' && $description !='' && $comment!=''){
                            include(locate_template('oferta/offer_main_comment_component.php'));
                        }
                        ?>
                        </div>
                        <?php
                    endif; ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center integrations-end">
            <div class="col-lg-8 col-md-12 text-center">
                <div class="button-bar center">
                    <a href="<?php the_permalink($payments_url);?>" class="btn btn-yellow btn-medium">Zobacz więcej metod płatności</a>
                    <span>albo</span>
                    <a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-medium">Załóż konto</a>
                </div>
            </div>
        </div>
    </div>
</div>