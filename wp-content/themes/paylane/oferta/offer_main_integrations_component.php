<?php  global $post;?>
<?php 
/**
 * Metaboxes for backend rendering
 */
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';
$plugin = 'plugins-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$plugin_url = rwmb_meta( $plugin, array( 'object_type' => 'setting' ), $option_name );
$offer_url = rwmb_meta( $offer, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>

<div id="integracje" class="offer_main_integrations_component">
    <div class="container container1920">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                <h2 class="text-center text-md-left">Budujemy najlepsze rozwiązania!</h2>
                <h3 class="text-center text-md-left">Solidna konstrukcja naszych rozwiązań to podstawa systemu<br> płatności, któremu możesz ufać</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="row">
                <?php 
                    $the_query = new WP_Query(array(
                        'post_type'=>'oferta',
                        'tax_query'=>array(array(
                            'taxonomy'=>'oferta_categories',
                            'field'=>'slug',
                            'terms'=>'integracje'
                        ))
                    ));
                ?>

                <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                    get_template_part('oferta/offer_main_preview_article_component');
                endwhile; endif; ?>
                    <div class="oil">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri()?>/assets-offer-main/oil-platform.svg">
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center explore-integrations">
            <div class="col-8 text-center">
                <h2>Poznaj nasze integracje</h2>
            </div>
        </div>
        <div class="row justify-content-center explore-logos">
            <div class="col-lg-5 col-md-8 col-sm-12 text-center">
                <div class="row justify-content-center">
                    <?php 
                        $logos = rwmb_meta('logo-integracje',$args, get_queried_object_id());
                        foreach ($logos as $logo) { ?>
                            <div class="col-sm-12 col-12 col-md-6 col-lg-4"><img src="<?php echo $logo['url'];?>"></div>
                        <?php } ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 text-center">
                <div class="button-bar center">
                    <a href="<?php the_permalink($devzone_url);?>"class="btn btn-yellow btn-medium">Pobierz wtyczki</a>
                        <span>albo</span>
                    <a href="<?php the_permalink($devzone_url);?>" class="btn btn-blue btn-medium">Idź do devzone</a>
                </div>
            </div>
        </div>
    </div>
</div>