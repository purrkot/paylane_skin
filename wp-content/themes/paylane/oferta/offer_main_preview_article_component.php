<div class="offer_main_preview_article_component col-lg-4 col-md-6 col-sm-12 col-12">
<div class="icon">
<?php 
    foreach (rwmb_meta('svg') as $key => $value) {
        echo '<img class="img-fluid" src="'.$value['url'].'">';
    }
?>
</div>
<div class="title">
    <?php the_title(); ?>
</div>
<div class="excerpt">
<?php the_excerpt(); ?> 
</div>
    <div class="url">
        <a href="<?php the_permalink();?>">
        Dowiedz się więcej <img src="<?php echo get_template_directory_uri();?>/assets-offer-main/arrows-right.svg">
        </a>
    </div>
</div>