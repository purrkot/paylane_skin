<div class="offer_main_preview_article_minimal_component">
    <div class="title">
        <h4><?php the_title(); ?></h4>
    </div>

<div class="excerpt">
    <?php the_excerpt(); ?> 
</div>
    <div class="url">
        <a href="<?php the_permalink();?>">
        Dowiedz się więcej <img src="<?php echo get_template_directory_uri();?>/assets-offer-main/arrows-right.svg">
        </a>
    </div>
</div>