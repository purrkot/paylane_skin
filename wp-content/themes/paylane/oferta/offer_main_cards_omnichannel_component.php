<?php 
/**
 * Metaboxes for backend rendering
 */
$register = 'register-link';
$devzone = 'devzone-link';
$payments = 'payments-link';
$plugins = 'plugins-link';
$about = 'about-link';
$offer = 'offer-link';

$option_name = 'paylane';
$register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
$devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
$payments_url = rwmb_meta( $payments, array( 'object_type' => 'setting' ), $option_name );
$about_url = rwmb_meta( $about, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="offer_main_cards_omnichannel_component">
    <div class="container container1920">
        <div class="row justify-content-center omnichannel-header">
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-center">
                <h2>Dostosowanie do Twoich potrzeb</h2>
                <h3>Elastyczna oferta i indywidualne podejście do Twoich potrzeb sprawia,<br> że jesteśmy gotowi płynąć z Tobą w każdy rejs!</h3>
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-offer-main/big-small.svg">
            </div>
        </div>
        <div class="row justify-content-center omnichannel-middle">
            <div class="col-lg-4 col-xl-3 col-md-6 col-sm-12 col-12 markopolo">
            <?php 
                    $the_query = new WP_Query(array(
                        'post_type'=>'oferta',
                        'tax_query'=>array(array(
                            'taxonomy'=>'oferta_categories',
                            'field'=>'slug',
                            'terms'=>'dostosowania1'
                        ))
                    ));
                ?>
                <?php if ( $the_query->have_posts() ) : ?>
                <div class="polopolo">
                    <div class="wave-container text-center">
                        <img class="icon-white" src="<?php echo get_template_directory_uri()?>/assets-offer-main/account-updater-white.svg">
                        <img class="wave" src="<?php echo get_template_directory_uri()?>/assets-offer-main/wave-small-left.svg">
                    </div>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                        <?php get_template_part('oferta/offer_main_preview_article_minimal_component');?>
                    <?php endwhile; ?> 
                </div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 markopolo">
            <?php 
                    $the_query = new WP_Query(array(
                        'post_type'=>'oferta',
                        'tax_query'=>array(array(
                            'taxonomy'=>'oferta_categories',
                            'field'=>'slug',
                            'terms'=>'dostosowania2'
                        ))
                    ));
                ?>
                <?php if ( $the_query->have_posts() ) : ?>
                <div class="polopolo">
                    <div class="wave-container text-center">
                        <img class="icon-white" src="<?php echo get_template_directory_uri()?>/assets-offer-main/360-white.svg">
                        <img class="wave" src="<?php echo get_template_directory_uri()?>/assets-offer-main/wave-small-right.svg">
                    </div>
                    <?php  while ( $the_query->have_posts() ) : $the_query->the_post();?>
                        <?php get_template_part('oferta/offer_main_preview_article_minimal_component');?>
                    <?php endwhile;?>
                </div>
                <?php endif; ?>

            </div>
        </div>
        <div class="row justify-content-center omnichannel-end">
            <div class="col-12 dol-sm-12 col-md-12 col-lg-8 text-center">
                <div class="button-bar center">
                    <a href="<?php the_permalink($payments_url);?>" class="btn btn-yellow btn-fixed-medium">Dowiedz się więcej</a>
                    <span>albo</span>
                    <a href="<?php the_permalink($register_url);?>" class="btn btn-blue btn-fixed-medium">Załóż konto</a>
                </div>
            </div>
        </div>
    </div>
</div>