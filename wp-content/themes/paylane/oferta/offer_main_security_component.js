
jQuery(document).ready(function ($) {
    if($('.security-changer').length){
        //set first pair to opent
        $('.security-changer .handler:eq(0), .security-changer .table:eq(0)').addClass('open');

        //init actions
        $('.security-changer .handler').on('click',function(e){
            var clickedIndex = $(e.target).index();
            $('.security-changer .handler').each(function(key,value){
                if(key==clickedIndex){
                    $(this).addClass('open');
                }else{
                    $(this).removeClass('open');
                }
            })
            $('.security-changer .table').each(function(key,value){
                if(key==clickedIndex){
                    $(this).addClass('open');
                }else{
                    $(this).removeClass('open');
                }
            })

        })
    }
})