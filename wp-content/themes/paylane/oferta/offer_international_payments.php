<?php 
	$register_id = 'register-link';
	$contact_id = 'contact-link';
	$option_name = 'paylane';
	$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
	$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="offer_international_payments">
    <div class="container container1440">
        <div class="row justify-content-center">
            <div class="header col-12 col-sm-8">                
                    <h1 class="title">PayLane to globalne możliwości dla Twojego biznesu!</h1>
                    <p class="description">Ponad 30 metod dostępnych w 40 krajach na świecie daje Ci możliwości idealnego dostosowania się do potrzeb Twojego klienta i specyfikacji rynków, na których operujesz.
                    </p>
                    <div class="globe-image text-center">
                        <div class="cloud-left"></div>
                        <div class="space"></div>
                        <div class="cloud-right"></div>
                        <div class="euro"></div>
                        <img src="<?php echo get_template_directory_uri() ?>/assets-international-payments/icon_walut.svg" alt="" class="img-fluid">
                    </div>				
			</div>
		</div>

		<div class="row justify-content-center">
            <div class="center col-12 col-sm-8">
                    <div class="yen"></div>
                    <div class="pound"></div>
                    <div class="dollar"></div>
                    <h2 class="title">160 walut to pełna paleta możliwości dla Twojego biznesu</h2>
                    <p class="description">Płatność odbywa sie w walucie preferowanej przez kupującego,
						 a&nbsp;należności spływają na Twoje konto w walucie najwygodniejszej dla Ciebie!
						 Bez kosztów przewalutowań i bez przekierowań</p>
                    <div class="btn-wrapper">
                        <a href="<?php the_permalink($post_id);?>" class="btn btn-medium btn-blue">Załóż konto</a>
                        <a href="<?php the_permalink($post_id_2);?>" class="btn btn-medium btn-transparent">Skontaktuj się z nami</a>
					</div>
			</div>
		</div>				

		<div class="row justify-content-center">
            <div class="footer col-12 col-sm-8">
                <p class="description">PayLane to najszerszy wybór walut i metod płatności dostępny na rynku.
					Jesteśmy na wszystkich kontynentach świata. Oprócz Antarktydy,
					ale pracujemy i nad tym. Cały czas poszerzamy listę krajów, w których oferujemy nasze usługi.
				</p>				
                <h4 class="bottom-text">W tym momencie współpracujemy z biznesami z następujących krajów:</h4>
                <div class="countries">
                    <?php $loop = new WP_Query(array('post_type'=>'kraje', 'posts_per_page'=>-1, ));
                    if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
                    echo '<a href="', the_permalink() ,'">', the_title(),'</a>';
                    endwhile;endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
