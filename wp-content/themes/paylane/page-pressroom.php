<?php
/**
 * Template Name: Pressroom
 *
 */
    get_header();
    echo get_template_part("full_image_title_header/full_image_title_header");
    echo get_template_part("press/press_menu");

    $args = array(
        'post_type' => 'pressroom',
        'posts_per_page' => 10,
        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
        );

    query_posts($args);

    if (have_posts() ) : while (have_posts() ) : the_post();
        echo get_template_part("blog_preview_component/blog_preview_component");
    endwhile;
    echo "<div class='container container1440'><div class='row justify-content-center'><div class='col-sm-12 col-md-12 col-lg-7 pagination'>";
    echo paginate_links( array(
        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
        'current'      => max( 1, get_query_var( 'paged' ) ),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'plain',
        'end_size'     => 2,
        'mid_size'     => 1,
        'prev_next'    => true,
        'prev_text'    => '',
        'next_text'    => sprintf( '%1$s <i></i>', __( 'Starsze', 'text-domain' ) ),
        'add_args'     => false,
        'add_fragment' => '',
    ) );
    echo "</div></div></div>";
    endif;
        get_footer('wave');
?>