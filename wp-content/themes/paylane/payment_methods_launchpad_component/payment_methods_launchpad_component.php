<?php   
        $register_id = 'register-link';
		$contact_id = 'contact-link';
		$payments_id = 'payments-link';
		$option_name = 'paylane';
		$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
		$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );	
?>
<?php
	$data = array();
	$the_query = new WP_Query(array(
		'post_type'=>'metody-platnosci',
		'tax_query'=>array(array(
			'taxonomy'=>'metody-platnosci_categories',
			'field'=>'slug',
			'terms'=>'platnosci-elektroniczne'
		))
	));
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
		foreach (rwmb_meta('svg') as $k => $v) {
			$imgurl = 	$v['url'];
		}
	array_push($data,array(
		'title'=>get_the_title(),
		'permalink'=>get_the_permalink(),
		'image'=>$imgurl,
		'excerpt'=>substr(get_the_excerpt(),0,300)
	));

endwhile; endif;
?>
<div class="payment_methods_launchpad_component">
	<div class="container container1920">
		<div class="row launchpad-row">
			<div class="col-12 col-lg-6 launchpad-image">
			</div>
			<div class="col-12 col-lg-6 launchpad-description">
				<div class="row">
					<div class="description-wrapper col-md-7 col-lg-8">
						<h3>Z e-portfelami startujesz po więcej!</h3>
						<?php
							foreach($data as $key=>$value){
								echo '<p class="'.( ($key==0)?'active':'' ).'">'.$value['excerpt'].'</p>';
							}
						?>
					</div>
					<div class="col-12 col-md-7 col-lg-8">
						<div class="logos-row">
							<div class="col-auto logos-inner-row text-center text-sm-center text-md-center text-lg-left">
								<?php 
									foreach($data as $key=>$value){
										echo '<a href="'.$value['permalink'].'" data="'.$key.'"><img data="'.$key.'" class="img-fluid'.( ($key==0)?'active':'' ).'" src="'.$value['image'].'" alt=""></a>';
									}								
								?>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="buttons-row row">
							<div  class="col-auto">
								<a class="btn btn-blue" href="<?php the_permalink($post_id);?>">Wypróbuj</a>
							</div>
							<div class="col-12 col-sm-auto col-break d-none d-sm-flex">
								albo
							</div>
							<div  class="col-auto">
								<a class="btn btn-yellow" href="<?php the_permalink($post_id);?>">Załóż konto!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>