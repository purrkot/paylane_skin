jQuery(document).ready(function($){
    if($('.payment_methods_launchpad_component').length){
        $('.payment_methods_launchpad_component .logos-inner-row a').on('mouseover',(e)=>{
            let clickedIndex = $(e.target).attr('data');
            $('.payment_methods_launchpad_component .logos-inner-row img').removeClass('active');
            $(e.target).addClass('active');
            $('.payment_methods_launchpad_component .description-wrapper p.active').removeClass('active');
            $('.payment_methods_launchpad_component .description-wrapper p').eq(clickedIndex).addClass('active');

        })
    }
})