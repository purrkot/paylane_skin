<?php 
    $register = 'register-link';
    $devzone = 'devzone-link';
    $payments = 'payments-link';
    $plugins = 'plugins-link';
    $about = 'about-link';

    $option_name = 'paylane';
    $register_url = rwmb_meta( $register, array( 'object_type' => 'setting' ), $option_name );
    $devzone_url = rwmb_meta( $devzone, array( 'object_type' => 'setting' ), $option_name );
?>
        <div class="home_api_intro_component">
            <div class="container">
                <div class="row justify-content-center align-content-center align-items-md-start align-items-lg-center secure-container">
                    <div class="col-12 col-sm-12 col-md-5 col-lg-6 col-xl-6 secure-icon text-center text-sm-center text-md-right text-lg-right">
                        <img class="secure-form" src="<?php echo rwmb_meta('secure-icon',array( 'size' => 'full' ))['full_url'];?>" alt="Secure Form | PayLane - Płatności elektroniczne">
                    </div>
                    <div class="col-12 col-sm-12 col-md-7 col-lg-6 col-xl-6 secure-content">
                        <h3><?php echo rwmb_meta('secure-title');?></h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-home/line.svg" alt="Linia | PayLane - Płatności elektroniczne">
                        <p><?php echo do_shortcode( wpautop( rwmb_meta('secure-content')));?></p>
                        <div class="button-bar">
                                    <a class="btn btn-blue btn-fixed-medium right" href="<?php the_permalink($register_url);?>">Załóż konto</a>
                                    <a class="btn btn-yellow btn-fixed-medium" href="<?php the_permalink($devzone_url);?>">Idź do Devzone</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <h2><?php echo rwmb_meta('api-title');?></h2>
                    </div>
                    <div class="col-12 text-center api-separator">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets-home/separator2.svg" alt="Separator | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                    </div>
                    <div class="col-12 text-center">
                        <p><?php echo do_shortcode( wpautop( rwmb_meta('api-content')));?></p>
                    </div>
                </div>
                <div class="row api-code">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center api-container">
                                    <p>Zobacz jak łatwo...</p>
                                    <div class="code-holder">
                                        <div class="selectors"><span class="php-selector">php</span><span class="ruby-selector">ruby</span><span class="python-selector">python</span></div>
                                        <img class="img-fluid text-left" src="<?php echo get_template_directory_uri(); ?>/assets-home/api-holder.svg" alt="Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                                        <div class="code-pre" ss-container>
                                            <div class="code php text-left active"><pre><code class="language-php"><?php echo rwmb_meta('php-request');?></code></pre></div>
                                            <div class="code ruby text-left"><pre><code class="language-ruby"><?php echo rwmb_meta('ruby-request');?></code></pre></div>
                                            <div class="code python text-left"><pre><code class="language-python"><?php echo rwmb_meta('python-request');?></code></pre></div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center api-container">
                                    <p>...jest sprawdzić transakcję</p>
                                    <div class="code-holder">
                                        <div class="selectors"><span class="php-selector">php</span><span class="ruby-selector">ruby</span><span class="python-selector">python</span></div>
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets-home/api-holder.svg" alt="Ramka API | PayLane - Płatności elektroniczne, przelewy internetowe, bramka płatnicza">
                                        <div class="code-pre" ss-container>
                                            <div class="code php text-left active"><pre><code class="language-php"><?php echo rwmb_meta('php-response');?></code></pre></div>
                                            <div class="code ruby text-left"><pre><code class="language-ruby"><?php echo rwmb_meta('ruby-response');?></code></pre></div>
                                            <div class="code python text-left"><pre><code class="language-python"><?php echo rwmb_meta('python-response');?></code></pre></div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                <div class="row button-row justify-content-center">
                    <div class="col-lg-12 text-center button-bar center">
                        <a class="btn btn-yellow btn-fixed-medium" href="<?php the_permalink($register_url);?>">Załóż konto!</a>
                            <span>albo</span>
                        <a class="btn btn-blue btn-fixed-medium" href="<?php the_permalink($devzone_url);?>">Idź do Devzone!</a>
                    </div>
                </div>
            </div>
        </div>
<script>

    jQuery(document).ready(function($){
        $phpS = $('.php-selector'),
        $rubyS = $('.ruby-selector'),
        $pythonS = $('.python-selector');

        $php = $('.php'),
        $ruby = $('.ruby'),
        $python = $('.python');
        
        $phpS.on('click',function(){
            $phpS.addClass('active');
            $php.addClass('active');
            $ruby.removeClass('active');
            $python.removeClass('active');
            $rubyS.removeClass('active');
            $pythonS.removeClass('active');
        });
        $rubyS.on('click',function(){
            $rubyS.addClass('active');
            $ruby.addClass('active');
            $php.removeClass('active');
            $python.removeClass('active');
            $phpS.removeClass('active');
            $pythonS.removeClass('active');
        });
        $pythonS.on('click',function(){
            $pythonS.addClass('active');
            $python.addClass('active');
            $ruby.removeClass('active');
            $php.removeClass('active');
            $rubyS.removeClass('active');
            $phpS.removeClass('active');
        });
    });
</script>