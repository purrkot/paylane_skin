<?php
    /*
    Template Name: Oferta
    */
    get_header('arrow');
    global $post;
        get_template_part('oferta/offer_main_intro_component');
        get_template_part('oferta/offer_main_integrations_component');
        get_template_part('oferta/offer_main_integrations_second_component');
        get_template_part('oferta/offer_main_integrations_third_component');
        get_template_part('oferta/offer_main_cards_omnichannel_component');
        get_template_part('oferta/offer_main_admin_panel_component');
        get_template_part('oferta/offer_main_security_component');
    get_footer('wave-sub');
?>