<?php
    get_header();
    echo get_template_part("learn/learn_header");
    echo get_template_part("blog_submenu_component/blog_submenu_component");
    echo get_template_part("learn/learn_loop");
    echo get_template_part("blog_featured/loop-featured");
    get_footer('wave');
?>