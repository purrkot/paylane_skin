
            <div class="container1440 container">
                <div class="blog_preview_component">
                        <div class="row justify-content-center">
                            <div class="col-12 col-sm-12 col-md-10 col-lg-7">
                                <div class="row">
                                <?php if(has_post_thumbnail()){ ?>
                                    <div class="col-xs-12 col-md-4 image-col">
 
                                        <?php if(!wp_is_mobile()){ ?>
                                            <img class="img-fluid" src="<?php the_post_thumbnail_url('blog_preview-thumbnail'); ?>" alt="<?php if(!empty(the_post_thumbnail_caption())){the_post_thumbnail_caption();}else{the_title();}?> | PayLane - Płatności elektroniczne">
                                        <?php } else { ?>
                                            <img class="img-fluid mobile" src="<?php the_post_thumbnail_url('blog_preview-thumbnail');?>" alt="<?php if(!empty(the_post_thumbnail_caption())){the_post_thumbnail_caption();}else{the_title();}?> | PayLane - Płatności elektroniczne">
                                        <?php };?>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8 description-col">
                                <?php } else { ?>
                                    <div class="col-12 description-col">
                                <?php };?>
                                        <span class="author"><?php the_author();echo ", "; the_date();?></span>
                                        <h3><?php the_title();?></h3>
                                        <p><?php the_excerpt();?></p>
                                        <div class="btn-row">
                                            <a href="<?php the_permalink(); ?>" class="btn btn-small btn-blue">Czytaj więcej</a>
                                            <?php if ( 'post' == get_post_type() ) { ?>
                                                <?php echo get_template_part('blog_preview_component/social');?>
                                            <?php };?>
                                            <?php if ( 'pressroom' == get_post_type() ) { 
                                                $doc = rwmb_meta('doc');
                                                $zip = rwmb_meta('zip');
                                                $xls = rwmb_meta('xls');
                                                $pdf = rwmb_meta('pdf');
                                                $doc_name = rwmb_meta('doc-name');
                                                $zip_name = rwmb_meta('zip-name');
                                                $xls_name = rwmb_meta('xls-name');
                                                $pdf_name = rwmb_meta('pdf-name');
                                                ?>
                                                <?php if( !empty($doc)|| !empty($pdf) || !empty($zip) || !empty($xls)){ ?>
                                                <div class="press-download">
                                                    <img class="download" src="<?php echo get_template_directory_uri();?>/press/download-cloud.svg" alt="Ściągnij plik | PayLane - Płatności elektroniczne">
                                                    <?php if(!empty($doc)){?><a href="<?php echo $zip;?>" download><img src="<?php echo get_template_directory_uri();?>/press/zip.svg" alt="Pobierz plik ZIP | PayLane - Płatności elektroniczne"></a><?php };?>
                                                    <?php if(!empty($pdf)){?><a href="<?php echo $pdf;?>" download><img src="<?php echo get_template_directory_uri();?>/press/pdf.svg" alt="Pobierz plik PDF | PayLane - Płatności elektroniczne"></a><?php };?>
                                                    <?php if(!empty($doc)){?><a href="<?php echo $doc;?>" download><img src="<?php echo get_template_directory_uri();?>/press/doc.svg" alt="Pobierz plik DOC | PayLane - Płatności elektroniczne"></a><?php };?>
                                                    <?php if(!empty($xls)){?><a href="<?php echo $xls;?>" download><img src="<?php echo get_template_directory_uri();?>/press/xls.svg" alt="Pobierz plik XLS | PayLane - Płatności elektroniczne"></a><?php };?>
                                                </div> 
                                                <?php };?>
                                            <?php };?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 separator" style="background:url('<?php echo get_template_directory_uri(); ?>/assets-blog/post-separator.svg');"></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
<?php if ( 'post' == get_post_type() ) { ?>
 <script>
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '1918887535015636',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>
<?php };?>