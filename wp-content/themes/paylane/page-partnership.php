<?php 
/*
    Template Name: Program partnerski
*/
get_header();
$benefits = rwmb_meta('benefits');
$icon = 'benefit-icon';
$terms_id = 'terms-link';
$privacy_id = 'privacy-link';
$title = 'benefit-title';
$content = 'benefit-content';
$captcha = 'captcha';
$option_name = 'paylane';
$site_key = rwmb_meta( $captcha, array( 'object_type' => 'setting' ), $option_name );
$terms_link = rwmb_meta( $terms_id, array( 'object_type' => 'setting' ), $option_name );
$privacy_link = rwmb_meta( $privacy_id, array( 'object_type' => 'setting' ), $option_name );
?>
    <div class="container partnership">
            <div class="cloud-left">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_work/cloud-left.svg" class="img-fluid">
			</div>
			<div class="cloud-right">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_work/cloud-right.svg" class="img-fluid">
			</div>
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="title"><?php the_title();?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-8 text-center main-content">
                <?php the_content();?>
            </div>
        </div>
        <div class="row">
            <div class="d-none d-sm-none d-md-block d-none d-sm-none d-md-none d-lg-block col-lg-2"></div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 text-center">
                <div class="contact-wrapper ">
                <div class="form-partner-title text-center">Dowiedz się więcej o programie</div>
                <form action="" class="support-form text-center">
                <?php echo get_template_part( 'register_form_component/contact_thanks' );?>
                    <div class="name-icon">
                        <input type="text" class="name" placeholder="Imię" required data-validation="required">
                    </div>
                    <div class="surname-icon">
                        <input type="text" class="surname" placeholder="Nazwisko" required data-validation="required">
                    </div>
                    <div class="email-icon">
                        <input type="email" class="email" placeholder="Email" required data-validation="required">
                    </div>
                    <div class="company-icon">
                        <input type="text" class="company" placeholder="Nazwa firmy" required data-validation="required">
                    </div>
                    <div class="website-icon">
                        <input type="url" class="website" placeholder="Strona firmy">
                    </div>
                    <div class="message-icon">
                        <textarea name="message" id="message" class="message" placeholder="Wiadomość"></textarea>
                    </div>
                    <p class="text-center" >Akceptuję <a href="<?php the_permalink($terms_link);?>">regulamin</a> i <a href="<?php the_permalink($privacy_link);?>">politykę prywatności</a></p>
                    <input type="hidden" class="formtype-input" value="contact">
                    <div class="g-recaptcha" data-sitekey="<?php echo $site_key;?>"></div>
                    <button type="submit" class="submit submit-button" value="Napisz do nas">
                        Napisz do nas
                    </button>
                </form>
                </div>  
            </div>
            <div class="d-none d-sm-none d-md-none d-lg-block col-lg-1"></div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-5">
                    <?php foreach($benefits as $benefit) { ?>
                        <div class="benefit-holder">
                        <div class="icon"><img src="<?php echo $benefit[$icon];?>" alt=""></div>
                        <h3 class="title"><?php echo $benefit[$title];?></h3>
                        <div class="content"><?php echo do_shortcode( wpautop( $benefit[$content] ) );?></div>
                        </div>
                    <?php }; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid partners">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center partners-title">Zaufały nam już setki firm i przedsiębiorców z całego świata</h1>
            </div> 
        </div>
        <div class="row hohoho">
            <div class="separator-gradient--full-width row"></div>
            <div class="wave">
				<img src="<?php echo get_template_directory_uri() ?>/assets-countries/wave-grey.svg" alt="">
            </div>
            <div class="row clients-wave justify-content-center glide"> 
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <?php $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                        if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                            <li class="glide__slide"><?php the_post_thumbnail();?></li>
                        <?php endwhile; endif;?>
                     </ul>
                </div>
            </div>
            <div class="row clients-wave justify-content-center glide2"> 
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                <?php 
                     $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                        <li class="glide__slide"><?php the_post_thumbnail();?></li>
                     <?php endwhile; endif;?>
                 </ul>
            </div>
            </div>
            <div class="row clients-wave justify-content-center glide3"> 
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                <?php 
                     $loop = new WP_Query(array('post_type'=>'klient', 'posts_per_page'=>-1, 'orderby'=>'rand' ));
                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();?>
                        <li class="glide__slide"><?php the_post_thumbnail();?></li>
                     <?php endwhile; endif;?>
                 </ul>
            </div>
            </div>
        </div>
    </div>
    <script>
    var args = {
        autoplay: 3000,
        animationDuration: 750,
        animationTimingFunc: 'ease-in-out',
    type: 'carousel',
    perView: 8,
    hoverpause:false,
    breakpoints: {
        1440: {
        perView: 7
        },
        960: {
        perView: 4
        },
        500: {
        perView: 2
        }
    }
    };
    var args2 = {
    autoplay: 3000,
    animationDuration: 750,
    animationTimingFunc: 'ease-in-out',
    type: 'carousel',
    perView: 8,
    direction: 'rtl',
    hoverpause:false,
    breakpoints: {
        1440: {
        perView: 7
        },
        960: {
        perView: 5
        },
        500: {
        perView: 4
        }
    }
    };
    var glide = new Glide('.glide', args);
    var glide2 = new Glide('.glide2', args2);
    var glide3 = new Glide('.glide3', args);
    glide.mount();
    glide2.mount();
    glide3.mount();
    </script>
<?php get_footer();?>