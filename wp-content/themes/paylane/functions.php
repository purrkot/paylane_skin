<?php

/**
 * Enqueue style/js scripts
 */
function paylane_enqueue_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_script('jquery','',array(),true);
    wp_enqueue_script('top_menu_component', get_template_directory_uri().'/top_menu_component/top_menu_component.js',array(),true);
    wp_enqueue_script('jquery_form-validator', get_template_directory_uri().'/node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js',array('jquery'),true);
    wp_enqueue_script('offer_main_security_component', get_template_directory_uri().'/oferta/offer_main_security_component.js',array('jquery'),true);
    wp_enqueue_script('payment_methods_pay_card_component', get_template_directory_uri().'/payment_methods_pay_card_component/payment_methods_pay_card_component.js',array('jquery'),true);
    wp_enqueue_script('payment_methods_launchpad_component', get_template_directory_uri().'/payment_methods_launchpad_component/payment_methods_launchpad_component.js',array('jquery'),true);
    wp_enqueue_script('footer_component', get_template_directory_uri().'/footer_component/footer_component.js',array('jquery'),true);
    wp_enqueue_script('register_form_component', get_template_directory_uri().'/register_form_component/register_form_component.js',array('jquery'),true);
    wp_enqueue_script('google-captcha','https://www.google.com/recaptcha/api.js',true);
    wp_enqueue_script('ebook_single_component', get_template_directory_uri().'/ebook_single_component/ebook_single_component.js',array('jquery'),true);
    wp_enqueue_script('contact-form', get_template_directory_uri().'/contact/contact-form.js',array('jquery'),true);
    
    wp_enqueue_script('simple-scroll', get_template_directory_uri().'/home_api_intro_component/simple-scroll.js',true);
    wp_enqueue_script('glide', get_template_directory_uri().'/glide/glide.min.js',true);
    wp_enqueue_script('glide', get_template_directory_uri().'/glide/glide.modular.esm.js',true);
    
    wp_enqueue_style( 'glide', get_template_directory_uri().'/glide/glide.core.min.css');
    
    //if(is_post_type_archive( 'faq' ) || get_post_type()=='faq'){ //TODO odblokowac ale cos nei lapie czasem post typu
        wp_enqueue_script('faq_section_viewer_component', get_template_directory_uri().'/faq_section_viewer_component/faq_section_viewer_component.js',array('jquery'),true);
    //}
    if(is_post_type_archive( 'devzone' ) || get_post_type()=='devzone' || is_page_template('page-main.php') ){
    /*prism editor*/
        wp_enqueue_script('devzone_section_viewer_component', get_template_directory_uri().'/devzone_section_viewer_component/devzone_section_viewer_component.js',array('jquery','prismjs-toolbar'),true);
        wp_enqueue_script('prismjs', get_template_directory_uri().'/node_modules/prismjs/prism.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-markup-templating', get_template_directory_uri().'/node_modules/prismjs/components/prism-markup-templating.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-php', get_template_directory_uri().'/node_modules/prismjs/components/prism-php.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-ruby', get_template_directory_uri().'/node_modules/prismjs/components/prism-ruby.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-python', get_template_directory_uri().'/node_modules/prismjs/components/prism-python.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-java', get_template_directory_uri().'/node_modules/prismjs/components/prism-java.js',array('jquery'),true);
        wp_enqueue_script('prismjs-language-json', get_template_directory_uri().'/node_modules/prismjs/components/prism-json.js',array('jquery'),true);
        wp_enqueue_script('prismjs-line-numbers', get_template_directory_uri().'/node_modules/prismjs/plugins/line-numbers/prism-line-numbers.js',array('jquery'),true);
        wp_enqueue_script('prismjs-toolbar', get_template_directory_uri().'/node_modules/prismjs/plugins/toolbar/prism-toolbar.js',array('jquery','prismjs'),true);
    }

    if(is_404()){
        wp_enqueue_script('phaser', get_template_directory_uri().'/assets-errors/phaser.min.js', true);
    }
    //prism pomoc w kopiowaniu schowka
    wp_enqueue_script('clipboard-polyfill', get_template_directory_uri().'/node_modules/clipboard-polyfill/build/clipboard-polyfill.js',array('jquery','prismjs-toolbar'),true);
    if(is_single()){
        if (has_post_format('video')){
            wp_enqueue_script('videojs', get_template_directory_uri().'/assets-blog/video.min.js',array('jquery'),false);
        };
    };
}

function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {  
 
    $style_formats = array(  
        // These are the custom styles
        array(  
            'title' => 'yellow Button',  
            'block' => 'span',  
            'classes' => 'btn btn-medium btn-yellow',
            'wrapper' => true,
        ), 
        array(  
            'title' => 'blue Button',  
            'block' => 'span',  
            'classes' => 'btn btn-medium btn-blue',
            'wrapper' => true,
        ),  
        array(  
            'title' => 'Content Block',  
            'block' => 'span',  
            'classes' => 'content-block',
            'wrapper' => true,
        ),
        array(  
            'title' => 'Highlighter',  
            'block' => 'span',  
            'classes' => 'highlighter',
            'wrapper' => true,
        ),
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
    
    return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

/**
 * 
 * Meta dla oferty
 * 
 */

// Check if you're editind your page

add_filter( 'rwmb_meta_boxes', 'oferta_main_register_meta_boxes' );
function oferta_main_register_meta_boxes( $meta_boxes ) {
    // Get Post ID
	$meta_boxes[] = array (
		'title' => 'Oferta page',
		'id' => 'oferta-glowna',
		'post_types' => array(
			'page',
		),
		'context' => 'normal',
		'priority' => 'high',
		'status' => 'publish',
		'fields' => array(
			
			array (
				'id' => 'logo-integracje',
				'type' => 'file_advanced',
				'name' => 'Integracje',
				'max_file_uploads' => 7,
				'tab' => 'oferta-integracje',
			),
			
			array (
				'id' => 'heading_10',
				'type' => 'heading',
				'name' => 'Pierwszy testimonial',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'avatar-1',
				'type' => 'single_image',
				'name' => 'Avatar',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'name-surname-1',
				'type' => 'text',
				'name' => 'Imię i nazwisko',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'position-1',
				'type' => 'text',
				'name' => 'Stanowisko',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'desc-1',
				'type' => 'textarea',
				'name' => 'Opis',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'url_8',
				'type' => 'url',
				'name' => 'Link do oferty',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'divider_9',
				'type' => 'divider',
				'name' => 'Divider',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'heading_10_copy_10',
				'type' => 'heading',
				'name' => 'Drugi testimonial',
				'tab' => 'testimonials',
			),
			
			array (
				'id' => 'avatar-2',
				'type' => 'single_image',
				'name' => 'Avatar',
				'tab' => 'testimonials',
			), 
			array (
				'id' => 'name-surname-2',
				'type' => 'text',
				'name' => 'Imię i nazwisko',
				'tab' => 'testimonials',
			),
			array (
				'id' => 'position-2',
				'type' => 'text',
				'name' => 'Stanowisko',
				'tab' => 'testimonials',
			),
			array (
				'id' => 'desc-2',
				'type' => 'textarea',
				'name' => 'Opis',
				'tab' => 'testimonials',
			),
			array (
				'id' => 'url-2',
				'type' => 'url',
				'name' => 'Link do oferty',
				'tab' => 'testimonials',
			),
			array (
				'id' => 'url-faq',
				'type' => 'url',
				'name' => 'URL FAQ',
				'tab' => 'oferta-maping',
			),
			array (
				'id' => 'url-register',
				'type' => 'url',
				'name' => 'URL Rejestracja',
				'tab' => 'oferta-maping',
			),
			array (
				'id' => 'url_pulign',
				'type' => 'url',
				'name' => 'URL Wtyczki',
				'tab' => 'oferta-maping',
			),
			array (
				'id' => 'url-metody',
				'type' => 'url',
				'name' => 'URL Metody płatności',
				'tab' => 'oferta-maping',
			),
			array (
				'id' => 'url_support',
				'type' => 'url',
				'name' => 'URL Support',
				'tab' => 'oferta-maping',
			),
		),
		'tab_style' => 'default',
		'tab_wrapper' => true,
		'show' => array(
			'relation' => 'OR',
			'template' => 
			array (
				0 => 'page-oferta.php',
			),
		),
		'tabs' => array(
			'oferta-integracje' => 
			array (
				'label' => 'Integracje',
				'icon' => 'dashicons-admin-plugins',
			),
			'testimonials' => 
			array (
				'label' => 'Wypowiedzi',
				'icon' => 'dashicons-carrot',
			),
			'oferta-maping' => 
			array (
				'label' => 'Link przycisków',
				'icon' => 'dashicons-admin-links',
			),
		),
	);
	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'oferta_single_register_meta_boxes' );
function oferta_single_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Oferta',
		'id' => 'oferta',
		'post_types' => array(
			'oferta',
		),
		'context' => 'normal',
		'priority' => 'high',
		'status' => 'publish',
		'autosave' => true,
		'fields' => array(
			
			array (
				'id' => 'svg',
				'type' => 'file_advanced',
				'name' => 'Ikona SVG',
				'max_file_uploads' => 1,
				'required' => 1,
            ),
            array (
				'id' => 'svg-white',
				'type' => 'file_advanced',
				'name' => 'Ikona SVG Biała',
				'max_file_uploads' => 1,
				'required' => 1,
			),
			
			array (
				'id' => 'divider_6',
				'type' => 'divider',
				'name' => 'Divider',
			),
			
			array (
				'id' => 'story-photo',
				'type' => 'single_image',
				'name' => 'Avatar',
			),
			
			array (
				'id' => 'story-name',
				'type' => 'text',
				'name' => 'Imię i nazwisko',
			),
			
			array (
				'id' => 'story-position',
				'type' => 'text',
				'name' => 'Stanowisko',
			),
			
			array (
				'id' => 'story-story',
				'type' => 'textarea',
				'name' => 'Story',
			),
			
			array (
				'id' => 'divider_7',
				'type' => 'divider',
				'name' => 'Divider',
			),
		),
	);
	return $meta_boxes;
}


/**
 * Add Team custom fields
 */
add_filter( 'rwmb_meta_boxes', 'team_register_meta_boxes' );
function team_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Zespół',
		'id' => 'team',
		'post_types' => array(
			'nasz-zespol',
		),
		'context' => 'normal',
		'priority' => 'high',
		'status' => 'publish',
		'autosave' => true,
		'fields' => array(
			array (
				'id' => 'american',
				'type' => 'single_image',
				'name' => 'Plan amerykański',
				'tab' => 'team-data-tab',
			),
			array (
				'id' => 'textarea_2',
				'type' => 'textarea',
				'name' => 'Stanowisko',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'heading_3',
				'type' => 'heading',
				'name' => 'Social media',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'url-facebook',
				'type' => 'url',
				'name' => 'Facebook URL',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'url-twitter',
				'type' => 'url',
				'name' => 'Twitter URL',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'url-linked-in',
				'type' => 'url',
				'name' => 'Linked In URL',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'url-github',
				'type' => 'url',
				'name' => 'Github URL',
				'tab' => 'team-data-tab',
			),
			
			array (
				'id' => 'url-gitlab',
				'type' => 'url',
				'name' => 'GitLab URL',
				'tab' => 'team-data-tab',
			),
		),
		'tab_style' => 'default',
		'tab_wrapper' => true,
		'tabs' => array(
			'team-data-tab' => 
			array (
				'label' => 'Dane',
				'icon' => 'dashicons-admin-users',
			),
		),
	);
	return $meta_boxes;
}

/**
 * Add custom word for pagination instead of "page"
 *
 */
function page_change() {
      global $wp_rewrite;  // Get the global wordpress rewrite-rules/settings
      // Change the base pagination property which sets the wordpress pagination slug.
      $wp_rewrite->pagination_base = "strona";  //where new-slug is the slug you want to use ;)
}
add_action( 'init', 'page_change' );


function cat_queries( $query ) {
    // do not alter the query on wp-admin pages and only alter it if it's the main query
    if (!is_admin() && $query->is_main_query()){
   
      if(is_category()||is_archive()){
        $query->set('posts_per_page', 5);
      }
  
    }
  }
  add_action( 'pre_get_posts', 'cat_queries' );

/**
 * Add SVG upload capability
 */

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

/**
 * Register standard paylane menus
 */
function register_menus(){
    //registering both navigation menus
    register_nav_menus(array(
        'primary-menu'  => __('Header Menu'),
        'tablet-menu'  => __('Tablet Menu'),
        'devzone-menu'  => __('Devzone Menu'),
        'faq-menu'  => __('Faq Menu'),
        'footer-menu-1'   => __('1st Footer Menu'),
        'footer-menu-2'   => __('2nd Footer Menu'),
        'footer-menu-3'   => __('3rd Footer Menu')
    ));
}

add_theme_support( 'post-formats', array( 'image', 'video' ) );


/**
 * Add additional template hierarchy - child categories take the template from parent category
 */
function new_subcategory_hierarchy() { 
    $category = get_queried_object();

    $parent_id = $category->category_parent;
    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );

/**
 * Register custom image thumbnails
 */
function register_thumbnail_sizes() {
    add_image_size( 'blog_preview-thumbnail', 660, 900, true ); // 300 pixels wide (and unlimited height)
    add_image_size('galeria-thumbnail-short', 364, 339, true);//for gallery
    add_image_size('galeria-thumbnail-long', 752, 339, true);//for gallery
    add_image_size( 'medium-logo', 360, 9999);
    add_image_size( 'footer-feature', 1920, 400, array('center','center') );
}

/**
 * Register FAQ post type and taxonomy
 */
function faq_post_type() {
    $labels = array(
        'name' => _x('FAQ', 'post type general name'),
        'singular_name' => _x('FAQ', 'post type singular name'),
        'add_new' => _x('Dodaj FAQ', 'book'),
        'add_new_item' => __('Dodaj FAQ'),
        'edit_item' => __('Edytuj FAQ'),
        'new_item' => __('Nowy FAQ'),
        'all_items' => __('Wszystkie FAQ'),
        'view_item' => __('Podgląd FAQ'),
        'search_items' => __('Szukaj FAQ'),
        'not_found' => __('Nie znaleziono FAQ'),
        'not_found_in_trash' => __('Nie znaleziono FAQ w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'FAQ'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje porady FAQ',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail','page-attributes'),
        'has_archive' => true,
        'hierarchical' => true,
    );
    register_post_type('faq', $args);

    register_taxonomy( 'faq_categories', array('faq'), array(
        'hierarchical' => true,
        'label' => 'Kategorie FAQ',
        'singular_label' => 'Kategoria FAQ',
        'rewrite' => array( 'slug' => 'faq', 'with_front'=> false )
        )
    );

    register_taxonomy_for_object_type( 'faq_categories', 'faq' );
}
function taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');
    
    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {
	    
            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {
                
                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {
		    
                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));
		    
                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules; 
}
add_filter('generate_rewrite_rules', 'taxonomy_slug_rewrite');
/**
 * Register Oferta post type and taxonomy
 */

function oferta_post_type() {
    $labels = array(
        'name' => _x('Oferta', 'post type general name'),
        'singular_name' => _x('Oferta', 'post type singular name'),
        'add_new' => _x('Dodaj ofertę', 'oferta'),
        'add_new_item' => __('Dodaj nową ofertę'),
        'edit_item' => __('Edytuj ofertę'),
        'new_item' => __('Nowa oferta'),
        'all_items' => __('Wszystkie oferty'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Oferta'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy Oferty',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt','page-attributes'),
        'has_archive' => false,
    );
    register_post_type('oferta', $args);

    register_taxonomy( 'oferta_categories', array('oferta'), array(
        'hierarchical' => true,
        'label' => 'Działy oferty',
        'singular_label' => 'Dział oferty',
        'rewrite' => array( 'slug' => 'oferta', 'with_front'=> false )
        )
    );

    register_taxonomy_for_object_type( 'oferta_categories', 'oferta' );
}
/**
 * Register wypowiedzi post type and taxonomy
 */

function funfact_post_type() {
    $labels = array(
        'name' => _x('Wypowiedź', 'post type general name'),
        'singular_name' => _x('Wypowiedź', 'post type singular name'),
        'add_new' => _x('Dodaj wypowiedź', 'pressroom'),
        'add_new_item' => __('Dodaj nową wypowiedź'),
        'edit_item' => __('Edytuj wypowiedź'),
        'new_item' => __('Nowa wypowiedź'),
        'all_items' => __('Wszystkie wypowiedzi'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Wypowiedzi'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy Wypowiedzi',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title','thumbnail', 'excerpt','page-attributes'),
        'has_archive' => true,
        'hierarchical' => true,
    );
    register_post_type('funfact', $args);
}

/**
 * Register ebook post type and taxonomy
 */

function ebook_post_type() {
    $labels = array(
        'name' => _x('ebook', 'post type general name'),
        'singular_name' => _x('Ebook', 'post type singular name'),
        'add_new' => _x('Dodaj ebook', 'pressroom'),
        'add_new_item' => __('Dodaj ebook'),
        'edit_item' => __('Edytuj ebook'),
        'new_item' => __('Nowy ebook'),
        'all_items' => __('Wszystkie ebooki'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'E-booki'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy ebooki',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt',  'page-attributes'),
        'has_archive' => true,
        'hierarchical' => true,
        'taxonomies' => array('post_tag'),
    );
    register_post_type('ebook', $args);

    register_taxonomy( 'ebook_categories', array('ebook'), array(
        'hierarchical' => true,
        'label' => 'Działy ebooków',
        'singular_label' => 'Dział ebook',
        'rewrite' => array( 'slug' => 'e-book', 'with_front'=> false )
        )
    );
    register_taxonomy_for_object_type( 'ebook_categories', 'ebook' );
}

/**
 * Register Metody Płatności post type and taxonomy
 */

function metody_post_type() {
    $labels = array(
        'name' => _x('Metoda płatności', 'post type general name'),
        'singular_name' => _x('Metoda płatności', 'post type singular name'),
        'add_new' => _x('Dodaj metodę płatności', 'pressroom'),
        'add_new_item' => __('Dodaj metodę płatności'),
        'edit_item' => __('Edytuj metodę płatności'),
        'new_item' => __('Nowa metods płatności'),
        'all_items' => __('Wszystkie metody płatności'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Metody płatności'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy metody płatności',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
        'has_archive' => false,
    );
    register_post_type('metody-platnosci', $args);

    register_taxonomy( 'metody-platnosci_categories', array('metody-platnosci'), array(
        'hierarchical' => true,
        'label' => 'Działy metody płatności',
        'singular_label' => 'Dział metody płatności',
        'rewrite' => array( 'slug' => 'metody-platnosci', 'with_front'=> false )
        )
    );
    register_taxonomy_for_object_type( 'metody_categories', 'metody-platnosci' );
}

/**
 * Register Metody Płatności post type and taxonomy
 */

function kraje_post_type() {
    $labels = array(
        'name' => _x('Państwo', 'post type general name'),
        'singular_name' => _x('Państwo', 'post type singular name'),
        'add_new' => _x('Dodaj państwo', 'pressroom'),
        'add_new_item' => __('Dodaj państwo'),
        'edit_item' => __('Edytuj państwo'),
        'new_item' => __('Nowe państwo'),
        'all_items' => __('Wszystkie państwa'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Państwa'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Dział Państw dla płatności międzynarodowych',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
        'has_archive' => true,
    );
    register_post_type('kraje', $args);
}
function praca_post_type() {
    $labels = array(
        'name' => _x('Praca', 'post type general name'),
        'singular_name' => _x('Praca', 'post type singular name'),
        'add_new' => _x('Dodaj pracę', 'pressroom'),
        'add_new_item' => __('Dodaj ofertę pracy'),
        'edit_item' => __('Edytuj ofertę pracy'),
        'new_item' => __('Nowe państwo'),
        'all_items' => __('Wszystkie oferty pracy'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Oferty pracy'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Dział ofert pracy w PayLane',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
        'has_archive' => true,
    );
    register_post_type('oferty-pracy', $args);
    register_taxonomy( 'miasta', array('oferty-pracy'), array(
        'hierarchical' => true,
        'label' => 'Miasta',
        'singular_label' => 'Miasta',
        'rewrite' => array( 'slug' => 'miasta', 'with_front'=> false )
        )
    );
    register_taxonomy_for_object_type( 'miasta', 'oferty-pracy' );
}

/**
 * Register PressRoom post type and taxonomy
 */

function pressroom_post_type() {
    $labels = array(
        'name' => _x('Press Room', 'post type general name'),
        'singular_name' => _x('Press Room', 'post type singular name'),
        'add_new' => _x('Dodaj informację prasową', 'pressroom'),
        'add_new_item' => __('Dodaj nową informację prasową'),
        'edit_item' => __('Edytuj informację prasową'),
        'new_item' => __('Nowa infromacja prasowa'),
        'all_items' => __('Wszystkie informacje prasowe'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Press Room'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy Press Room',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'has_archive' => true,
    );
    register_post_type('pressroom', $args);
}

/**
 * Register Team post type and taxonomy
 */
function team_title( $title, $post ) {
    if ( 'nasz-zespol' == $post->post_type ) {
        $title = 'Wpisz Imię i Nazwisko';
    }
    return $title;
}
add_filter( 'enter_title_here', 'team_title', 10, 2 );

function team_post_type() {
    $labels = array(
        'name' => _x('Zespół', 'post type general name'),
        'singular_name' => _x('Zespół', 'post type singular name'),
        'add_new' => _x('Dodaj członka zespołu', 'pressroom'),
        'add_new_item' => __('Dodaj nowego członka zespołu'),
        'edit_item' => __('Edytuj członka zespołu'),
        'new_item' => __('Nowy członek zespołu'),
        'all_items' => __('Wszyscy członkowie zespołu'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Zespół'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy Press Room',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'has_archive' => true,
    );
    register_post_type('nasz-zespol', $args);
}

/**
 * Register Devzone post type and taxonomy
 */

function devzone_post_type() {
    $labels = array(
        'name' => _x('Devzone', 'post type general name'),
        'singular_name' => _x('Devzone', 'post type singular name'),
        'add_new' => _x('Dodaj wpis Devzone', 'devzone'),
        'add_new_item' => __('Dodaj nowy wpis'),
        'edit_item' => __('Edytuj wpis'),
        'new_item' => __('Nowy wpis'),
        'all_items' => __('Wszystkie'),
        'view_item' => __('Podgląd'),
        'search_items' => __('Szukaj'),
        'not_found' => __('Nie znaleziono'),
        'not_found_in_trash' => __('Nie znaleziono w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Devzone'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje działy Devzone',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor'),
        'has_archive' => true,
    );
    register_post_type('devzone', $args);

    register_taxonomy( 'devzone_categories', array('devzone'), array(
        'hierarchical' => true,
        'label' => 'Działy Devzone',
        'singular_label' => 'Dział Devzone',
        'rewrite' => array( 'slug' => 'devzone', 'with_front'=> false ),
        )
    );

    register_taxonomy_for_object_type( 'devzone_categories', 'devzone' );
}
function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('init', 'custom_taxonomy_flush_rewrite');
/**
 * Register Clients post type and taxonomy
 */
function klient_post_type() {
    $labels = array(
        'name' => _x('Klient', 'post type general name'),
        'singular_name' => _x('Klient', 'post type singular name'),
        'add_new' => _x('Dodaj klienta', 'legal'),
        'add_new_item' => __('Dodaj klienta'),
        'edit_item' => __('Edytuj klienta'),
        'new_item' => __('Nowy klient'),
        'all_items' => __('Wszyscy klienci'),
        'view_item' => __('Podgląd klientów'),
        'search_items' => __('Szukaj klientów'),
        'not_found' => __('Nie znaleziono klientów'),
        'not_found_in_trash' => __('Nie znaleziono klientów w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Klienci'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje klientów',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => false,
        'rewrite' => array( 'slug' => 'klient', 'with_front'=> false ),
    );
    register_post_type('klient', $args);
}



/**
 * Register legal post type and taxonomy
 */
function legal_post_type() {
    $labels = array(
        'name' => _x('Dokument prawny', 'post type general name'),
        'singular_name' => _x('Dokument prawny', 'post type singular name'),
        'add_new' => _x('Dodaj dokument', 'legal'),
        'add_new_item' => __('Dodaj dokument'),
        'edit_item' => __('Edytuj dokument'),
        'new_item' => __('Nowy dokument'),
        'all_items' => __('Wszystkie dokuemtny prawne'),
        'view_item' => __('Podgląd dokumanetów prawnych'),
        'search_items' => __('Szukaj dokumanetów prawnych'),
        'not_found' => __('Nie znaleziono dokumentów prawnych'),
        'not_found_in_trash' => __('Nie znaleziono dokumentów prawnych w usuniętych'),
        'parent_item_colon' => '',
        'menu_name' => 'Dokumenty prawne'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Przetrzymuje porady FAQ',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => false,
        'rewrite' => array( 'slug' => 'dokumenty-prawne', 'with_front'=> false ),
    );
    register_post_type('legal', $args);
}

/**
 * register meta-box
 */
function getRequestFieldsArray($level){
    $prefix = 'paylane_';
    
    $requestStructureMetaArray = array(
            'name'  => 'Request Code',
            'desc'  => 'Request Code',
            'id'    => $prefix . 'request_code',
            'type'  => 'group',
            'clone' => 'true',
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => 'Request {#}',
            // List of sub-fields
            'fields'=>array(
                array(
                    'name' => 'Pole',
                    'id'   => 'pole',
                    'type' => 'text',
                ),
                array(
                    'name' => 'Format',
                    'id'   => 'format',
                    'type' => 'text',
                ),
                array(
                    'name' => 'Wymagane',
                    'id'   => 'wymagane',
                    'type' => 'checkbox',
                ),
                array(
                    'name' => 'Opis',
                    'id'   => 'opis',
                    'type' => 'textarea',
                ),
            ),    
        
    );
    
    if($level>0){
        array_push( $requestStructureMetaArray['fields'],getRequestFieldsArray($level-1));
    }

    return $requestStructureMetaArray;
}

function getResponseFieldsArray($level){
    $prefix = 'paylane_';
    
    $responseStructureMetaArray = array( 
            'name'  => 'Response Code',
            'desc'  => 'Response Code',
            'id'    => $prefix . 'response_code',
            'type'  => 'group',
            'clone' => 'true',
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => 'Response {#}',
            // List of sub-fields
            'fields'=>array(
                array(
                    'name' => 'Pole',
                    'id'   => 'pole',
                    'type' => 'text',
                ),
                array(
                    'name' => 'Format',
                    'id'   => 'format',
                    'type' => 'text',
                ),
                array(
                    'name' => 'Opis',
                    'id'   => 'opis',
                    'type' => 'textarea',
                ),
            ),    
    );
    
    if($level>0){
        array_push( $responseStructureMetaArray['fields'],getResponseFieldsArray($level-1));
    }

    return $responseStructureMetaArray;
}

function paylane_register_meta_boxes( $meta_boxes ) {
    $prefix = 'paylane_';

    //request nested structure
    $meta_boxes_fields = array();
    array_push($meta_boxes_fields,getRequestFieldsArray(3));

    //endpoint url
    array_push($meta_boxes_fields,array(
        'name' => 'API Endpoint',
        'desc' => 'API Endpoint',
        'id'   => $prefix.'request_api_endpoint',
        'type' => 'group',
        'clone'=> false,
        'sort_clone'=>false,
        'collapsible'=>true,
        'group_title'=>'Request API Endpoint',
        'fields'=>array(
            array(
                'name'=>'Request Method',
                'id'=>'request_method',
                'type'=>'select_advanced',
                'options'         => array("GET"=>"GET","HEAD"=>"HEAD","POST"=>"POST","PUT"=>"PUT","DELETE"=>"DELETE","OPTIONS"=>"OPTIONS","PATCH"=>"PATCH"),
                'multiple'        => false,
                'placeholder'     => 'Request Method',
                'select_all_none' => false,
            ),
            array(
                'name'=>'Request URI',
                'id'=>'request_uri',
                'type'=>'textarea'
            )
        )
    ));

    //endpoint errors
    array_push($meta_boxes_fields,array(
        'name' => 'API Endpoint Error',
        'desc' => 'API Endpoint Error',
        'id'   => $prefix.'request_api_endpoint_error',
        'type' => 'group',
        'clone'=> true,
        'sort_clone'=>true,
        'collapsible'=>true,
        'group_title'=>'Request API Endpoint Error',
        'fields'=>array(
            array(
                'name'=>'Request Error',
                'id'=>'request_method_error',
                'type'=>'text',
            ),
            array(
                'name'=>'Request Meaning',
                'id'=>'request_method_error_meaning',
                'type'=>'textarea'
            )
        )
));


    //request code example title
    array_push($meta_boxes_fields,array(
        'name' => 'Request Code Example Title',
        'desc' => 'Request Code Example Title',
        'id'   => $prefix.'request_code_example_title',
        'group_title'=>'Request Code Example Title',
        'fields'=>array(
            array(
                'name'=>'Tytuł odpowiedzi',
                'id'=>'request_title',
                'type'=>'textarea'
            )
        )
    ));
    //request code examples
    array_push($meta_boxes_fields,array(
            'name' => 'Request Code Example',
            'desc' => 'Request Code Example',
            'id'   => $prefix.'request_code_example',
            'type' => 'group',
            'clone'=> 'true',
            'sort_clone'=>true,
            'collapsible'=>'true',
            'group_title'=>'Request Code Example',
            'fields'=>array(
                array(
                    'name'=>'Język',
                    'id'=>'jezyk',
                    'type'=>'select_advanced',
                    'options'         => array("html"=>"HTML","css"=>"CSS","javascript"=>"JavaScript","java"=>"Android","ruby"=>"Ruby","python"=>"Python","json"=>"JSON","php"=>"PHP","php-extras"=>"PHP Extras"),
                    'multiple'        => false,
                    'placeholder'     => 'Wybierz Język',
                    'select_all_none' => false,
                ),
                array(
                    'name'=>'Kod',
                    'id'=>'kod',
                    'type'=>'textarea'
                )
            )
    ));

    //response nested structure
    array_push($meta_boxes_fields,getResponseFieldsArray(3));

    //TODO: response code example title
    array_push($meta_boxes_fields,array(
        'name' => 'Response Code Example Title',
        'desc' => 'Response Code Example Title',
        'id'   => $prefix.'response_code_example_title',
        'group_title'=>'Response Code Example Title',
        'fields'=>array(
            array(
                'name'=>'Tytuł odpowiedzi',
                'id'=>'response_title',
                'type'=>'textarea'
            )
        )
    ));
    //response code examples
    array_push($meta_boxes_fields,array(
        'name' => 'Response Code Example',
        'desc' => 'Response Code Example',
        'id'   => $prefix.'response_code_example',
        'type' => 'group',
        'clone'=> 'true',
        'sort_clone'=>true,
        'collapsible'=>'true',
        'group_title'=>'Response Code Example',
        'fields'=>array(
            array(
                'name'=>'Język',
                'id'=>'jezyk',
                'type'=>'select_advanced',
                'options'         => array("success"=>"Success","Error"=>"Error","html"=>"HTML","php"=>"PHP","json"=>"JSON","python"=>"Python","javascript"=>"JavaScript","ruby"=>"Ruby","java"=>"Android"),
                'multiple'        => false,
                'placeholder'     => 'Wybierz Język',
                'select_all_none' => false,
            ),
            array(
                'name'=>'Kod',
                'id'=>'kod',
                'type'=>'textarea'
            )
        )
));


    $meta_boxes[] =  array(
        'id'         => 'apirecord',
        'title'      => 'Dokumentacja API',
        'post_types' => 'devzone',
        'context'    => 'normal',
        'priority'   => 'high',
        'fields'     => $meta_boxes_fields 
    );


    return $meta_boxes;
}

/*
 *Metabox Custom Fields and Custom Fields Groups for PressRoom
 */

function press_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
        'title' => 'Press Room',
        'tabs' => array(
            'word' => array(
                'label' => 'Word',
            ),
            'excel'  => array(
                'label' => 'Excel',
            ),
            'pdf'    => array(
                'label' => 'PDF',
            ),
            'zip'    => array(
                'label' => 'ZIP',
            ),
        ),

        // Tab style: 'default', 'box' or 'left'. Optional
        'tab_style' => 'left',

        // Show meta box wrapper around tabs? true (default) or false. Optional
        'tab_wrapper' => true,



		'id' => 'press-room',
		'post_types' => array(
			'pressroom',
		),
		'context' => 'after_editor',
		'priority' => 'high',
		'status' => 'publish',
		'autosave' => true,
		'fields' => array(
			array (
				'id' => 'pdf-name',
				'type' => 'text',
				'name' => 'Nazwa pliku',
                'tab'  => 'pdf',
			),
			array (
				'id' => 'pdf',
				'type' => 'file_input',
				'name' => 'Dodaj plik PDF',
                'file_type' => 'pdf',
                'tab'  => 'pdf',
			),
			array (
				'id' => 'doc-name',
				'type' => 'text',
				'name' => 'Nazwa pliku',
                'tab'  => 'word',
			),
			array (
				'id' => 'doc',
				'type' => 'file_input',
				'name' => 'Dodaj plik DOC',
                'file_type' => 'doc',
                'tab'  => 'word',
			),
			array (
				'id' => 'xls-name',
				'type' => 'text',
				'name' => 'Nazwa pliku',
                'tab'  => 'excel',
			),
			array (
				'id' => 'xls',
				'type' => 'file_input',
				'name' => 'Dodaj plik EXCEL',
                'file_type' => 'xls',
                'tab'  => 'excel',
			),
			array (
				'id' => 'zip-name',
				'type' => 'text',
				'name' => 'Nazwa pliku',
                'tab'  => 'zip',
			),
			array (
				'id' => 'zip',
				'type' => 'file_input',
				'name' => 'Dodaj plik ZIP',
                'file_type' => 'zip',
                'tab'  => 'zip',
			),
		),
	);
	return $meta_boxes;
}


/**
 * Kontakt metaboxes
 */
add_filter( 'rwmb_meta_boxes', 'kontakt_small_register_meta_boxes' );
function kontakt_small_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Claim',
		'id' => 'kontakt_claim',
		'post_types' => array(
			'page',
		),
		'context' => 'normal',
        'autosave' => true,
        'show' => array(
			'relation' => 'OR',
			'template' => 
			array (
				0 => 'page-contact.php',
			),
		),
		'fields' => array(
			array (
				'id' => 'contact-claim',
				'type' => 'textarea',
				'name' => 'Główny claim',
            ),
			array (
				'id' => 'contact-subtitle',
				'type' => 'textarea',
				'name' => 'Podtytuł',
            ),
        ),
    );
    return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'kontakt_register_meta_boxes' );
function kontakt_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'kontakt',
		'id' => 'kontakt',
		'post_types' => array(
			'page',
		),
		'context' => 'normal',
		'priority' => 'high',
		'status' => 'publish',
        'autosave' => true,
        'tab_style' => 'default',
		'tab_wrapper' => true,
		'show' => array(
			'relation' => 'OR',
			'template' => 
			array (
				0 => 'page-contact.php',
			),
		),
		'tabs' => array(
			'sales-tab' => 
			array (
				'label' => 'Sprzedaż',
				'icon' => 'dashicons-cart',
			),
			'support-tab' => 
			array (
				'label' => 'Support',
				'icon' => 'dashicons-phone',
			),
			'adress-tab' => 
			array (
				'label' => 'Adres i mapa',
				'icon' => 'dashicons-location',
			),
		),
		'fields' => array(
			array (
				'id' => 'sales-title',
				'type' => 'text',
                'name' => 'Tytuł sekcji sprzedaż',
                'tab'  => 'sales-tab',
            ),
            array (
				'id' => 'sales-subtitle',
				'type' => 'text',
                'name' => 'Sprzedaż podtytuł',
                'tab'  => 'sales-tab',
			),
			
			array (
				'id' => 'sales-image',
				'type' => 'single_image',
                'name' => 'Zdjęcie do formularza',
                'tab'  => 'sales-tab',
            ),
            array (
				'id' => 'sales-form-button',
				'type' => 'text',
                'name' => 'Copy buttonu w formularzu',
                'tab'  => 'sales-tab',
            ),
			array (
				'id' => 'support-title',
				'type' => 'text',
                'name' => 'Tytuł sekcji support',
                'tab'  => 'support-tab',
			),
			array (
				'id' => 'support-subtitle',
				'type' => 'text',
                'name' => 'Support podtytuł',
                'tab'  => 'support-tab',
			),
			array (
				'id' => 'support-form-button',
				'type' => 'text',
                'name' => 'Copy buttonu w formularzu',
                'tab'  => 'support-tab',
            ),
			array (
				'id' => 'contact-adress',
				'name' => 'Adres kontaktowy',
                'type' => 'wysiwyg',
                'tab'  => 'adress-tab',
			),
			
			array (
				'id' => 'box-view',
				'type' => 'single_image',
                'name' => 'Tło adresu',
                'tab'  => 'adress-tab',
			),
			
			array (
				'id' => 'map-address',
				'type' => 'url',
                'name' => 'URL Adresu OpenMaps',
                'tab'  => 'adress-tab',
			),
		),
	);
	return $meta_boxes;
}

/**
 * add custom upload mimes
 */
function custom_upload_mimes($mimes = array()) {

    // Add a key and value for the CSV file type
    $mimes['svg'] = "text/svg+xml";

    return $mimes;
}
function fix_svg() {
    echo '<style type="text/css">
          .attachment-266x266, .thumbnail img { 
               width: 100% !important; 
               height: auto !important; 
          }
          </style>';
 }
 add_action('admin_head', 'fix_svg');

/**
 * Register FAQ Category icons
 */
add_filter( 'rwmb_meta_boxes', 'faq_icons' );
function faq_icons ( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Faq icons',
		'id' => 'faq-icons',
		'status' => 'publish',
		'fields' => array(
			
			array (
				'id' => 'category-icon',
				'type' => 'file_advanced',
				'name' => 'Ikona kategorii FAQ',
                'required' => 1,
                'max_file_uploads' => 1,
				'label_description' => 'Dodaj ikonę kategorii aby była wyświetlana w menu obok kategorii',
			),
		),
		'taxonomies' => array(
			'faq_categories',
		),
	);
	return $meta_boxes;
}

/*
 * Register theme customize options 
 */

// Register settings page. In this case, it's a theme options page
add_filter( 'mb_settings_pages', 'prefix_options_page' );
function prefix_options_page( $settings_pages ) {
    $settings_pages[] = array(
        'id'          => 'paylane',
        'option_name' => 'paylane',
        'menu_title'  => 'Paylane',
        'icon_url'    => 'dashicons-tagcloud',
        'style'       => 'no-boxes',
        'columns'     => 1,
        'tabs'        => array(
            'general' => 'Główne ustawienia',
            'design'  => 'Email Ustawienia',
            'faq'     => 'FAQ i pomoc',
            'security'=> 'Zabezpieczenia API'
        ),
        'position'    => 68,
    );
    return $settings_pages;
}

// Register meta boxes and fields for settings page
add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'             => 'general',
        'title'          => 'General',
        'settings_pages' => 'paylane',
        'tab'            => 'general',

        'fields' => array(
            array(
                'name' => 'Logo',
                'id'   => 'paylane-logo',
                'type' => 'file_input',
            ),
            array(
                'name' => 'Logo PEP Czarne',
                'id'   => 'pep-logo-black',
                'type' => 'single_image',
            ),
            array(
                'name' => 'Logo PEP pomarańczowe',
                'id'   => 'pep-logo-orange',
                'type' => 'single_image',
            ),
            array(
                'name' => 'Logo PEP białe',
                'id'   => 'pep-logo-white',
                'type' => 'single_image',
            ),
            array(
                'name' => 'Merchant Panel URL',
                'desc' => 'Link zewnętrzny do Merchant Panelu',
                'id'   => 'merchant-link',
                'type' => 'url',
            ),
            array(
                'name' => 'Facebook fanpage',
                'id'   => 'facebook-page',
                'type' => 'url',
            ),
            array(
                'name' => 'Linked In fanpage',
                'id'   => 'linekdin-page',
                'type' => 'url',
            ),
            array(
                'name' => 'Twitter',
                'id'   => 'twitter-page',
                'type' => 'url',
            ),
            array(
                'name' => 'Github',
                'id'   => 'github-page',
                'type' => 'url',
            ),
            array(
                'name' => 'Gitlab',
                'id'   => 'gitlab-page',
                'type' => 'url',
            ),
            array (
				'id' => 'privacy-link',
				'type' => 'post',
				'name' => 'Polityka prywatności URL',
				'desc' => 'Ustaw politykę prywatności dla formularzy',
				'post_type' => 'legal',
				'field_type' => 'select_advanced',
				'inline' => true,
			),
			
			array (
				'id' => 'terms-link',
				'type' => 'post',
				'name' => 'Ragulamin URL',
				'desc' => 'Ustaw regulamin dla formularzy',
				'post_type' => 'legal',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'register-link',
				'type' => 'post',
				'name' => 'Rejestracja konta URL',
				'desc' => 'Ustaw stronę rejestracji konta',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'contact-link',
				'type' => 'post',
				'name' => 'Kontakt URL',
				'desc' => 'Ustaw stronę kontaktu',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'payments-link',
				'type' => 'post',
				'name' => 'Metody płatności URL',
				'desc' => 'Ustaw stronę metod płatności',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'devzone-link',
				'type' => 'post',
				'name' => 'Devzone URL',
				'desc' => 'Ustaw devzone płatności',
				'post_type' => 'devzone',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'knowledge-link',
				'type' => 'taxonomy',
				'name' => 'Księga wiedzy URL',
				'desc' => 'Ustaw księgę wiedzy',
                'taxonomy'   => 'category',
				'field_type' => 'select_advanced',
            ),
            array (
				'id' => 'blog-link',
				'type' => 'url',
				'name' => 'Blog URL',
				'desc' => 'Ustaw kategorie Blog',
				'field_type' => 'select',
            ),
            array (
				'id' => 'pressroom-link',
				'type' => 'post',
				'name' => 'Pressroom URL',
				'desc' => 'Ustaw stronę Pressroom',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'work-link',
				'type' => 'post',
				'name' => 'Praca URL',
				'desc' => 'Ustaw stronę Ofert Pracy',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
			),
            array (
				'id' => 'plugins-link',
				'type' => 'post',
				'name' => 'Wtyczki URL',
				'desc' => 'Ustaw stronę wtyczek w devzone',
				'post_type' => 'devzone',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'offer-link',
				'type' => 'post',
				'name' => 'Oferta URL',
				'desc' => 'Ustaw stronę Oferty',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
            ),
            array (
				'id' => 'about-link',
				'type' => 'post',
				'name' => 'O nas URL',
				'desc' => 'Ustaw stronę O nas',
				'post_type' => 'page',
				'field_type' => 'select_advanced',
				'inline' => true,
			),
		),
    );
    $meta_boxes[] = array(
        'id'             => 'additional',
        'title'          => 'Dodatkowe pola',
        'settings_pages' => 'paylane',
        'tab'            => 'design',

        'fields' => array(
            array (
				'id' => 'email-link',
				'type' => 'email',
				'name' => 'Adres email do korespondencji',
				'desc' => 'Ustaw adres email używany do korespondencji',
            ),
            array (
				'id' => 'newsletter-thanks',
				'type' => 'textarea',
				'name' => 'Podziękowanie za zapis do newslettera'
            ),
            array (
				'id' => 'contact-thanks',
				'type' => 'textarea',
				'name' => 'Podziękowanie za kontakt'
            ),
            array (
				'id' => 'phone-thanks',
				'type' => 'textarea',
				'name' => 'Podziękowanie za zostawienie numeru telefonu'
            ),
            array (
				'id' => 'register-thanks',
				'type' => 'textarea',
				'name' => 'Podziękowanie za rejestrację'
			),
        ),
    );

    $meta_boxes[] = array(
        'id'             => 'info',
        'title'          => 'Podstawowe informacje',
        'settings_pages' => 'paylane',
        'tab'            => 'faq',
        'fields'         => array(
            array(
                'name' => 'Copright',
                'id'   => 'footer-copy',
                'type' => 'textarea',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'             => 'security',
        'title'          => 'Zabeczpieczenia',
        'settings_pages' => 'paylane',
        'tab'            => 'security',

        'fields' => array(
            array(
                'name' => 'API Security Token',
                'id'   => 'paylane-token',
                'type' => 'textarea',
            ),
            array(
                'name' => 'Google Captcha Site Key',
                'id'   => 'captcha',
                'type' => 'textarea',
            ),
        ),
    );
    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'ebook_register_meta_boxes' );
function ebook_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Ebook ID',
		'id' => 'ebook-id',
		'post_types' => array(
			'ebook',
		),
		'context' => 'normal',
		'priority' => 'high',
		'status' => 'publish',
		'autosave' => true,
		'fields' => array(
			
			array (
				'id' => 'ebook-id',
				'type' => 'number',
				'name' => 'ID Ebooka',
				'desc' => 'Wpisz tutaj ID ebooka w API',
				'required' => 1,
			),
		),
	);
	return $meta_boxes;
}

/**
 * register actions
 */


add_action('upload_mimes', 'custom_upload_mimes');
add_action('init', 'oferta_post_type');
add_action('init', 'metody_post_type');
add_action('init', 'faq_post_type');
add_action('init', 'klient_post_type');
add_action('init', 'devzone_post_type');
add_action('init', 'pressroom_post_type');
add_action('init', 'legal_post_type');
add_action('init', 'funfact_post_type');
add_action('init', 'ebook_post_type');
add_action('init', 'kraje_post_type');
add_action('init', 'praca_post_type');
add_action('init', 'team_post_type');
add_filter( 'rwmb_meta_boxes', 'paylane_register_meta_boxes' );
add_filter( 'rwmb_meta_boxes', 'press_register_meta_boxes' );
add_theme_support( 'post-thumbnails' );
add_action( 'after_setup_theme', 'register_thumbnail_sizes' );
add_action( 'wp_enqueue_scripts', 'paylane_enqueue_scripts' );
add_action( 'init', 'register_menus' );


/**
 * Custom gallery
*/

function custom_gallery($attr){
    $output = array();
    array_push($output,'<div class="custom-gallery row">');
    //array(4) { ["link"]=> string(4) "none" ["columns"]=> string(1) "2" ["size"]=> string(5) "large" ["ids"]=> string(17) "59,60,61,62,63,64" }
    $ids = explode(',',$attr['ids']);
    $i = 0;
    foreach ($ids as $key=>$id) {
      if($i>=4){
        $i=0;
      }
      $columnClass = 'col-6 col-sm-6 col-md-4';
      $thumbnailType = 'galeria-thumbnail-short';
      if($i==1 || $i==2 || wp_is_mobile() ){
        $thumbnailType = 'galeria-thumbnail-long';
        $columnClass = 'col-6 col-sm-6 col-md-8';
      }
      array_push($output, '<div class="column '.$columnClass.'"><a data-lightbox="gallery" href="'.wp_get_attachment_image_src($id,'large')[0].'"><img src="'.wp_get_attachment_image_src($id,$thumbnailType)[0].'"></a></div>');
      $i++;
    }
    array_push($output,'</div>');
    return implode('',$output);
  }
  
  function override_default_gallery(){
    remove_shortcode('gallery');
    add_shortcode('gallery','custom_gallery');
  }
  
  add_action('after_setup_theme','override_default_gallery');
  

/*
*PayLane Devzone format
*/

