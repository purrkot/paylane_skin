<?php
require_once('devzone_menu_walker.php');

function generateDocumentationRow($type,$meta,$lastMeta='',$firstInImmersion=false,$lastInIntermission=false,$level=0){
    $level++;
    $out = '';
    if($firstInImmersion && $level==1){
        $out.='<div class="row request-header '.$type.'"><div class="col-12"><div class="subcol"><div class="row">';
        $out.='  <div class="col-12"><h4>Struktura '.(($type=='request')?'zapytania':'odpowiedzi').'</h4></div>';
            if($type=='request'):
                $out.='<div class="col col-1st justify-content-center align-self-center">Pole</div>';
                $out.='<div class="col col-2nd justify-content-center align-self-center">Format</div>';
                $out.='<div class="col col-3rd justify-content-center align-self-center">Wymagane</div>';
                $out.='<div class="col col-4th justify-content-center align-self-center">Opis</div>';    
            elseif($type=='response'):
                $out.='<div class="col col-1st justify-content-center align-self-center">Pole</div>';
                $out.='<div class="col col-2nd justify-content-center align-self-center">Format</div>';
                $out.='<div class="col col-3rd justify-content-center align-self-center">Opis</div>';
            endif;
        $out.='</div>';
        $out.='</div>';
        $out.='</div>';
        $out.='</div>';
    }
    if($lastMeta!='' && $firstInImmersion){
            $out.='<div class="row">';
            $out.='  <div class="col-12"><h4>Struktura '.$lastMeta["pole"].'</h4></div>';
            $out.='  <div class="col-12"><div class="subcol '.(($level%2)?'odd':'even').' level-'.$level.'">'; 
        }elseif($firstInImmersion){
            $out.='<div class="row">';
            $out.='  <div class="col-12"><div class="subcol '.(($level%2)?'odd':'even').' level-'.$level.'">'; 
        }

            $out.='<div class="row"><div class="col-12"><div class="frame '.(($level%2)?'odd':'even').' level-'.$level.'"><div class="divider-line"></div><div class="row">';
                if($type=='request'):
                    $out.='<div class="col col-1st justify-content-center align-self-center">'.$meta["pole"].'</div>';
                    $out.='<div class="col col-2nd justify-content-center align-self-center">'.$meta["format"].'</div>';
                    $out.='<div class="col col-3rd justify-content-center align-self-center">'. ( ($meta["wymagane"]=='')?'nie':'tak' ) .'</div>';
                    $out.='<div class="col col-4th justify-content-center align-self-center">'.$meta["opis"];
                    if($meta["paylane_request_code"]){
                        $out.='<div class="row"><div class="col-12"><div class="btn btn-expand">Rozwiń</div></div></div>';
                    }
                    $out.='</div>';
                    $out.='<div class="col-12 detailed-substructure">';
                    if($meta["paylane_request_code"]){
                        foreach($meta["paylane_request_code"] as $key => $singleMeta){
                            $out.=generateDocumentationRow($type,$singleMeta,$meta,($key==0),$key==(count($meta['paylane_request_code'])-1),$level);
                        }            
                    }
                    $out.='</div>';
                elseif($type=='response'):
                    $out.='<div class="col col-1st justify-content-center align-self-center">'.$meta["pole"].'</div>';
                    $out.='<div class="col col-2nd justify-content-center align-self-center">'.$meta["format"].'</div>';
                    $out.='<div class="col col-3rd justify-content-center align-self-center">'.$meta["opis"];
                    if($meta["paylane_response_code"]){
                        $out.='<div class="row"><div class="col-12"><div class="btn btn-expand">Rozwiń</div></div></div>';
                    }
                    $out.='</div>';

                    $out.='<div class="col-12 detailed-substructure">';
                    if($meta["paylane_response_code"]){
                        foreach($meta["paylane_response_code"] as $key => $singleMeta){
                            $out.=generateDocumentationRow($type,$singleMeta,$meta,($key==0),$key==(count($meta['paylane_response_code'])-1),$level);
                        }            
                    }
                    $out.='</div>';
                endif;
            $out.='</div></div></div></div>';
    if($lastInIntermission){
        $out.='</div></div>';
        $out.='</div>';
    }
    return $out;
    
}

function generateTable($currentLoopPage,$table_column_type){
    $output = '';
    
        switch ($table_column_type) {
            case 2:
                if(rwmb_meta('table-2',array(),$currentLoopPage->ID)!=""){
                    $col_data[0] = rwmb_meta('field-2-1',array(),$currentLoopPage->ID);
                    $col_data[1] = rwmb_meta('field-2-2',array(),$currentLoopPage->ID);
                    $maxLen = 0;
                    foreach($col_data as $val){
                        if(count($val)>0){
                            $maxLen = count($val);
                        }
                    }                 

                    $output.='<div class="rwmb_table">';
                    $output.='<h4>'.rwmb_meta('table-2',array(),$currentLoopPage->ID).'</h4>';
                    $output.='<table class="table">';
                    $output.='  <thead>';
                    $output.='    <tr>';
                    $output.='      <th scope="col">'.rwmb_meta('column-1-title',array(),$currentLoopPage->ID).'</th>';
                    $output.='      <th scope="col">'.rwmb_meta('column-2-title',array(),$currentLoopPage->ID).'</th>';
                    $output.='    </tr>';
                    $output.='  </thead>';
                    $output.='<tbody>';
                    for($i=0;$i<$maxLen;$i++){
                        $output.='<tr>';
                            $output.='<td>'.$col_data[0][$i].'</td>';
                            $output.='<td>'.$col_data[1][$i].'</td>';
                        $output.='</tr>';
                    }
                    $output.='</tbody>';
                    $output.='</table>';
                    $output.='</div>';
                }
                break;
            
            case 3:
                if(rwmb_meta('table-name',array(),$currentLoopPage->ID)!=""){
                        $col_data[0] = rwmb_meta('field-1',array(),$currentLoopPage->ID);
                        $col_data[1] = rwmb_meta('field-2',array(),$currentLoopPage->ID);
                        $col_data[2] = rwmb_meta('field-3',array(),$currentLoopPage->ID);
                        $maxLen = 0;
                        foreach($col_data as $val){
                            if(count($val)>0){
                                $maxLen = count($val);
                            }
                        }                 
                        
                        $output.='<div class="rwmb_table">';
                        $output.='<h4>'.rwmb_meta('table-name',array(),$currentLoopPage->ID).'</h4>';
                        $output.='<table class="table">';
                        $output.='  <thead>';
                        $output.='    <tr>';
                        $output.='      <th scope="col">'.rwmb_meta('label-1',array(),$currentLoopPage->ID).'</th>';
                        $output.='      <th scope="col">'.rwmb_meta('label-2',array(),$currentLoopPage->ID).'</th>';
                        $output.='      <th scope="col">'.rwmb_meta('label-3',array(),$currentLoopPage->ID).'</th>';
                        $output.='    </tr>';
                        $output.='  </thead>';
                        $output.='<tbody>';
                        for($i=0;$i<$maxLen;$i++){
                            $output.='<tr>';
                                $output.='<td>'.$col_data[0][$i].'</td>';
                                $output.='<td>'.$col_data[1][$i].'</td>';
                                $output.='<td>'.$col_data[2][$i].'</td>';
                            $output.='</tr>';
                        }
                        $output.='</tbody>';
                        $output.='</table>';
                        $output.='</div>';
                    }
                break;
        }
    
    return $output;
}

function generateNavMenuWidget(){
    $o='<div class="NavMenuWidget">';
    $o.='<div id="devopener"><img src="'.get_template_directory_uri().'/assets-devzone/mobile-chevron.svg"></div>';
    $o.=wp_nav_menu( 
        array( 
            'theme_location' => 'devzone-menu',
            'menu_class'=>'devzone_section_viewer_root',
            'echo'=>false,
            'walker'  => new DevzoneMenuWalker() //use our custom walker
        ) 
    );
    $o.='</div>';
    return $o;
}

function generateIntroWidget($currentLoopPage){
    $o = '<div class="IntroWidget">';
    $o.= '<h3 id="'.$currentLoopPage->post_name.'">'.$currentLoopPage->post_title.'</h3>';
    $o.= $currentLoopPage->post_content;
    $o.= '</div>';
    return $o;
}
function generateTwoColumnTableWidget($currentLoopPage){
    $o='<div class="TwoColumnTableWidget">';
    $o.=generateTable($currentLoopPage,2);
    $o.='</div>';
    return $o;    
}
function generateThreeColumnTableWidget($currentLoopPage){
    $o='<div class="ThreeColumnTableWidget">';
    $o.=generateTable($currentLoopPage,3);
    $o.='</div>';
    return $o;
}
function generateRequestTableWidget($currentLoopPage){
    $o='<div class="RequestTableWidget">';
    $myMeta = rwmb_meta('paylane_request_code',array(),$currentLoopPage->ID);                            
    foreach ($myMeta as $key => $singleMeta): 
        $o.=generateDocumentationRow('request',$singleMeta,'',($key==0),$key==(count($myMeta)-1));
    endforeach; 
    $o.='</div>';
    return $o;
}
function generateRequestEndpointWidget($currentLoopPage){
    $o='';
    $myMeta = rwmb_meta('paylane_request_api_endpoint',array(),$currentLoopPage->ID);
    if( is_array($myMeta) && isset($myMeta['request_method']) && isset($myMeta['request_uri']) ):
        $o.= '<div class="RequestEndpointWidget">';
            $o.='<div class="endpoint-info">';
                $o.='<span>API Endpoint:</span> <span class="request_uri">'.$myMeta['request_method'].'</span> <span class="request_url">'.$myMeta['request_uri'].'</span>';
            $o.='</div>';
        $o.= '</div>';
        endif;
    return $o;
}
function generateRequestErrorWidget($currentLoopPage){
    $o='';
    $myMeta = rwmb_meta('paylane_request_api_endpoint_error',array(),$currentLoopPage->ID);
    if( is_array($myMeta) ):
        $o.= '<div class="RequestEndpointErrorWidget">';
            foreach($myMeta as $key=>$singleMeta){
                $o.='<div class="endpoint-info">';
                    $o.='<span>API Error:</span> <span class="request_error">'.$singleMeta['request_method_error'].'</span> <span class="request_error_meaning">'.$singleMeta['request_method_error_meaning'].'</span>';
                $o.='</div>';
            }
        $o.= '</div>';
        endif;
    return $o;
}
function generateRequestCodeEditorWidget($currentLoopPage){
    $langNames = array("html"=>"HTML","xml"=>"XML","python"=>"Python","svg"=>"SVG","mathml"=>"MathML","css"=>"CSS","clike"=>"C-like","javascript"=>"JavaScript","java"=>"Android","abap"=>"ABAP","actionscript"=>"ActionScript","apacheconf"=>"Apache Configuration","apl"=>"APL","applescript"=>"AppleScript","arff"=>"ARFF","asciidoc"=>"AsciiDoc","asm6502"=>"6502 Assembly","aspnet"=>"ASP.NET (C#)","autohotkey"=>"AutoHotkey","autoit"=>"AutoIt","basic"=>"BASIC","csharp"=>"C#","cpp"=>"C++","coffeescript"=>"CoffeeScript","csp"=>"Content-Security-Policy","css-extras"=>"CSS Extras","django"=>"Django/Jinja2","erb"=>"ERB","fsharp"=>"F#","gedcom"=>"GEDCOM","glsl"=>"GLSL","graphql"=>"GraphQL","http"=>"HTTP","hpkp"=>"HTTP Public-Key-Pins","hsts"=>"HTTP Strict-Transport-Security","ichigojam"=>"IchigoJam","inform7"=>"Inform 7","json"=>"JSON","latex"=>"LaTeX","livescript"=>"LiveScript","lolcode"=>"LOLCODE","markup-templating"=>"Markup templating","matlab"=>"MATLAB","mel"=>"MEL","n4js"=>"N4JS","nasm"=>"NASM","nginx"=>"nginx","nsis"=>"NSIS","objectivec"=>"Objective-C","ocaml"=>"OCaml","opencl"=>"OpenCL","parigp"=>"PARI/GP","objectpascal"=>"Object Pascal","php"=>"PHP","php-extras"=>"PHP Extras","plsql"=>"PL/SQL","powershell"=>"PowerShell","properties"=>".properties","protobuf"=>"Protocol Buffers","q"=>"Q (kdb+ database)","jsx"=>"React JSX","tsx"=>"React TSX","renpy"=>"Ren'py","rest"=>"reST (reStructuredText)","sas"=>"SAS","sass"=>"Sass (Sass)","scss"=>"Sass (Scss)","sql"=>"SQL","soy"=>"Soy (Closure Template)","tap"=>"TAP","tt2"=>"Template Toolkit 2","typescript"=>"TypeScript","vbnet"=>"VB.Net","vhdl"=>"VHDL","vim"=>"vim","ruby"=>"Ruby","visual-basic"=>"Visual Basic","wasm"=>"WebAssembly","wiki"=>"Wiki markup","xojo"=>"Xojo (REALbasic)","xquery"=>"XQuery","yaml"=>"YAML");
    $myMeta = rwmb_meta('paylane_request_code_example',array(),$currentLoopPage->ID); 
    $myMeta2 = rwmb_meta('paylane_request_code_example_title',array(),$currentLoopPage->ID); 
    $o.='';
    if($myMeta){
    $o.='<div class="RequestCodeEditorWidget">';
        $o.='<div class="row">';
        $o.='<div class="col request-title text-left">';
        if($myMeta2){
            $o.=$myMeta2;
        }else{
            $o.="Sample Request";
        }
        $o.='</div>';
            $o.='<div class="col language_changer text-right">';
            foreach($myMeta as $key=>$value){
                    $o.='<span>'.$langNames[$value['jezyk']].'</span>';
                    if($key != count($myMeta)-1){
                        $o.='/';
                    }
            }
            $o.='</div>';
        $o.='</div>';
        $o.='<div class="row">';
            $o.='<div class="drawer_container col">';
                foreach($myMeta as $key=>$value){
                        $o.='<div class="language_drawer">';
                            //$o.='<pre class="line-numbers"><code class="language-'.$value['jezyk'].'">'.htmlspecialchars($value['kod']).'</code></pre>';
                            $o.='<pre class="line-numbers"><code class="language-'.$value['jezyk'].'">'.htmlspecialchars($value['kod']).'</code></pre>';
                        $o.='</div>';
                }
            $o.='</div>';
        $o.='</div>';
    $o.='</div>';
    }
    return $o;
}
function generateResponseTableWidget($currentLoopPage){
    $o='';
    $myMeta = rwmb_meta('paylane_response_code',array(),$currentLoopPage->ID);                     
    if($myMeta){
        $o='<div class="ResponseTableWidget">';
        foreach ($myMeta as $key => $singleMeta): 
            $o.=generateDocumentationRow('response',$singleMeta,'',($key==0),$key==(count($myMeta)-1));
        endforeach; 
        $o.='</div>';
    }
    return $o;
}
function generateResponseCodeEditorWidget($currentLoopPage){
    $langNames = array("html"=>"HTML","xml"=>"XML","python"=>"Python","svg"=>"SVG","mathml"=>"MathML","css"=>"CSS","clike"=>"C-like","javascript"=>"JavaScript","java"=>"Android","abap"=>"ABAP","actionscript"=>"ActionScript","apacheconf"=>"Apache Configuration","apl"=>"APL","applescript"=>"AppleScript","arff"=>"ARFF","asciidoc"=>"AsciiDoc","asm6502"=>"6502 Assembly","aspnet"=>"ASP.NET (C#)","autohotkey"=>"AutoHotkey","autoit"=>"AutoIt","basic"=>"BASIC","csharp"=>"C#","cpp"=>"C++","coffeescript"=>"CoffeeScript","csp"=>"Content-Security-Policy","css-extras"=>"CSS Extras","django"=>"Django/Jinja2","erb"=>"ERB","fsharp"=>"F#","gedcom"=>"GEDCOM","glsl"=>"GLSL","graphql"=>"GraphQL","http"=>"HTTP","hpkp"=>"HTTP Public-Key-Pins","hsts"=>"HTTP Strict-Transport-Security","ichigojam"=>"IchigoJam","inform7"=>"Inform 7","json"=>"JSON","latex"=>"LaTeX","livescript"=>"LiveScript","lolcode"=>"LOLCODE","markup-templating"=>"Markup templating","matlab"=>"MATLAB","mel"=>"MEL","n4js"=>"N4JS","nasm"=>"NASM","nginx"=>"nginx","nsis"=>"NSIS","objectivec"=>"Objective-C","ocaml"=>"OCaml","opencl"=>"OpenCL","parigp"=>"PARI/GP","objectpascal"=>"Object Pascal","php"=>"PHP","php-extras"=>"PHP Extras","plsql"=>"PL/SQL","powershell"=>"PowerShell","properties"=>".properties","protobuf"=>"Protocol Buffers","q"=>"Q (kdb+ database)","jsx"=>"React JSX","tsx"=>"React TSX","renpy"=>"Ren'py","rest"=>"reST (reStructuredText)","sas"=>"SAS","sass"=>"Sass (Sass)","scss"=>"Sass (Scss)","sql"=>"SQL","soy"=>"Soy (Closure Template)","tap"=>"TAP","tt2"=>"Template Toolkit 2","typescript"=>"TypeScript","vbnet"=>"VB.Net","vhdl"=>"VHDL","vim"=>"vim","ruby"=>"Ruby","visual-basic"=>"Visual Basic","wasm"=>"WebAssembly","wiki"=>"Wiki markup","xojo"=>"Xojo (REALbasic)","xquery"=>"XQuery","yaml"=>"YAML");
    $myMeta = rwmb_meta('paylane_response_code_example',array(),$currentLoopPage->ID); 
    $myMeta2 = rwmb_meta('paylane_response_code_example_title',array(),$currentLoopPage->ID); 
    $o.='';
    if($myMeta){
    $o.='<div class="ResponseCodeEditorWidget">';
        $o.='<div class="row">';
            $o.='<div class="col request-title text-left">';
            if($myMeta2){
                $o.=$myMeta2;
            }else{
                $o.="Sample Response";
            }
            $o.='</div>';
            $o.='<div class="col language_changer text-right">';
            foreach($myMeta as $key=>$value){
                    $o.='<span>'.$langNames[$value['jezyk']].'</span>';
                    if($key != count($myMeta)-1){
                        $o.='/';
                    }
            }
            $o.='</div>';
        $o.='</div>';
        $o.='<div class="row">';
            $o.='<div class="drawer_container col">';
                foreach($myMeta as $key=>$value){
                        $o.='<div class="language_drawer">';
                            $o.='<pre class="line-numbers"><code class="language-'.$value['jezyk'].'">'.htmlspecialchars($value['kod']).'</code></pre>';
                        $o.='</div>';
                }
            $o.='</div>';
        $o.='</div>';
    $o.='</div>';
    }
    return $o;
}
?>

    <div class="devzone_section_viewer_component d-flex">
        <div class="container container1920">
            <div class="row">
                <div class="col col-nav-menu" ss-container>
                    <?php echo generateNavMenuWidget(); ?>
                </div>
                <div id="openerfilter"></div>
                <div class="col col-content">
                    
                    <div class="col text-center devzone-header">
                        <h2>Devzone</h2>
                    </div>
                    
                    <?php
                    global $devzoneIds;
                    if(!is_array($devzoneIds)){ $devzoneIds = array(); }
                    foreach ($devzoneIds as $post_id): 
                        $currentLoopPage = get_post($post_id);
                    ?>
                        <div class="row page-content">
                            <div class="col">
                                <div class="row row-request">
                                    <?php 
                                        $cr=''; 
                                        $cr.=generateRequestEndpointWidget($currentLoopPage);
                                        $cr.=generateRequestCodeEditorWidget($currentLoopPage);
                                        $cr.=generateRequestErrorWidget($currentLoopPage);
                                        $cl='';
                                        $cl.=generateIntroWidget($currentLoopPage);
                                        $cl.=generateTwoColumnTableWidget($currentLoopPage);
                                        $cl.=generateThreeColumnTableWidget($currentLoopPage);
                                        $cl.=generateRequestTableWidget($currentLoopPage);
                                    ?>
                                    <?php if($cl!=""): ?>
                                        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-left <?php echo ($cr=="")?"no-col-r":""; ?>">
                                                <?php echo $cl;  ?>
                                            </div>
                                            <?php endif; ?>
                                            
                                            <?php if($cr!=""):?>
                                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-right  <?php echo ($cl=="")?"no-col-l":""; ?>">
                                                <div class="sticky-wrapper">
                                                    <?php echo $cr; ?>
                                                </div>
                                        </div>
                                    <?php endif;?>
                                </div>
                            
                                <div class="row row-response">
                                                                       
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-left">
                                        <?php echo generateResponseTableWidget($currentLoopPage); ?>
                                    </div>
                                    <?php if($cr!=""):?>
                                        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-right">
                                            <div class="sticky-wrapper">
                                                <?php echo generateResponseCodeEditorWidget($currentLoopPage); ?>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>

    </div>