jQuery(document).ready(function ($) {
    /** 
     * Devzone paylane_request_code paylane_response_model Model
     */
    var DevzonePaylaneRequestCode = function(MyDomObject){
        var self = this;
        self.container = MyDomObject;
        
        //collapse all 
        $(self.container).find('.frame').each(function(){
            $(this).addClass('collapsed-structure');
        })

        $(self.container).find('.btn').each(function(){
            $(this).on('click',function(e){
                if( $(e.target).hasClass('btn-expand') ){
                    $(e.target).text('Zwiń').removeClass('btn-expand').addClass('btn-collapse');
                    $(e.target).closest('.frame').removeClass('collapsed-structure').addClass('expanded-structure');
                    $(e.target).closest('.frame').closest('.row').removeClass('collapsed-row').addClass('expanded-row');
                }else{
                    $(e.target).text('Rozwiń').removeClass('btn-collapse').addClass('btn-expand');
                    $(e.target).closest('.frame').removeClass('expanded-structure').addClass('collapsed-structure');
                    $(e.target).closest('.frame').closest('.row').removeClass('expanded-row').addClass('collapsed-row');
                }
                
            });
        })
    }



    /**
     * Devzone Code Editor Model
     */
    var DevzoneSectionEditorModel = function (myDomObject) {
        var self = this;
        self.container = myDomObject;
        self.languages = $(myDomObject).find('.language_changer span');
        self.drawers = $(myDomObject).find('.drawer_container .language_drawer');

        //hide other languages
        self.languages.each(function (index, value) {
            if (index == 0) {
                $(self.languages[0]).addClass('active');
                $(self.drawers[0]).addClass('active');
            } else {
                $(self.drawers[index]).addClass('d-none');
            }
            //initialize actions
            $(self.languages[index]).on('click', function () {
                $(self.languages).removeClass('active');
                $(this).addClass('active');
                $(self.drawers).removeClass('active').addClass('d-none');
                $(self.drawers[$(self.languages).index($(this))]).addClass('active').removeClass('d-none');
            })

        })
    }

    /**
     * Devzone Section Viewer Controller
     */
    var DevzoneSectionViewerController = function () {
        var self = this;
        self.devzonePaylaneRequestCodes = [];
        self.devzoneSectionEditorModels = [];

        //activate code editor actions
        $('.RequestCodeEditorWidget, .ResponseCodeEditorWidget').each(function (index, value) {
            self.devzoneSectionEditorModels.push(new DevzoneSectionEditorModel(value));
        });

        //activate code table actions
        $('.RequestTableWidget, .ResponseTableWidget').each(function (index, value) {
            self.devzonePaylaneRequestCodes.push(new DevzonePaylaneRequestCode(value));
        });

        //activate menu actions
        if($('#devopener').length){
            $('#devopener').on('click',function(e){
                // if( $(e.target).hasClass('line1') || $(e.target).hasClass('line2')){
                //     $(e.target).closest('.rwd-menu-handler').toggleClass('open');
                // }else{
                //     $(e.target).toggleClass('open');
                // }
                // $(e.target).closest('.devzone_section_viewer_container').toggleClass('open');
                $('.devzone_section_viewer_component').toggleClass('nav-shown');
            })
        }

    }

    function setPrismToolbar() {
        Prism.plugins.toolbar.registerButton('hello-world', {
            text: 'copy',
            onClick: function (env) { // optional
                let tmpClipboardCopiedText = env.code;
                clipboard.writeText(tmpClipboardCopiedText);
            }
        });
    }

    if ($('.devzone_section_viewer_component').length) {
        let devzoneSectionViewerController = new DevzoneSectionViewerController();
        setPrismToolbar();
    }
});