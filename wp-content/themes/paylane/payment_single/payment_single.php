<?php 
	$register_id = 'register-link';
	$contact_id = 'offer-link';
	$option_name = 'paylane';
	$post_id = rwmb_meta( $register_id, array( 'object_type' => 'setting' ), $option_name );
	$post_id_2 = rwmb_meta( $contact_id, array( 'object_type' => 'setting' ), $option_name );
?>
<div class="payment_single">
    <div class="container-fluid header-wave">
            <img src="<?php echo get_template_directory_uri();?>/payment_single/header-wave.svg" alt="PayLane Wave">
            <h2 class="title"><?php the_title();?></h2>
    </div>
    <div class="container">
        <div class="row small-payments">
            <div class="col-12 text-center">
            <?php $args = array(
                        'post_type' =>  'metody-platnosci',
                        'posts_per_page'=>10,
                        'orderby' =>  'rand',
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'metody-platnosci_categories',
                                'field'    => 'slug',
                                'terms'    => array( 'karty-platnicze','platnosci-elektroniczne'),
                                'operator' => 'IN',
                            ),
                            array(
                                'taxonomy' => 'metody-platnosci_categories',
                                'field'    => 'slug',
                                'terms'    => array( 'banki'),
                                'operator' => 'NOT IN',
                            ),
                        ),
                    );
            $payment = new WP_Query($args);
            if ($payment->have_posts() ) : while ($payment->have_posts() ) : $payment->the_post();?>
                <a class="small-payment" href="<?php the_permalink();?>"><?php the_title()?></a>
            <?php endwhile; endif; wp_reset_postdata(); wp_reset_query();?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="separator-gradient--full-width"></div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-4 col-lg-4 payment-logo text-center">
                    <?php the_post_thumbnail();?>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-5 content">
                <?php the_content();?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 snippets">
                <?php $popular = rwmb_meta('method-popular');
                        $currency = rwmb_meta('text_2');?>
                <div class="row">
                    <div class="popular col-12 col-lg-2 text-center text-lg-left text-lg-right">Waluty rozliczeniowe</div>
                    <div class="fragment col-12 col-lg-10 text-center text-lg-left">
                    <?php echo $currency;?>
                    </div>
                </div>
            </div>
            <div class="col-12 snippets">
                <div class="row">
                    <div class="popular col-12 col-lg-2 text-center text-lg-left text-lg-right">Popularne w krajach</div>
                    <div class="fragment col-12 col-lg-10 text-center text-lg-left">
                        <?php foreach($popular as $country){
                            echo '<div class="country"><a href="', the_permalink($country) ,'">', get_the_title($country),'</a>,&nbsp;</div>';
                        };?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 social-wrapper">
                <a href="https://twitter.com/share?ref_src=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/twitter.svg"></a>
                <img class="social-icon" id="facebook-share-<?php the_ID();?>" src="<?php echo get_template_directory_uri(); ?>/assets-blog/facebook.svg">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/assets-blog/linked-in.svg"></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="separator-gradient--full-width"></div>
            </div>
        </div>
        <div class="row call-to-action">
            <div class="col-sm-12 sol-md-6 col-lg-6 col-xl-6 text-center">
                <h2>Poznaj szerzej naszą ofertę</h2>
            </div>
            <div class="col-sm-12 sol-md-6 col-lg-6 col-xl-6 text-center button-bar center">
                <a class="btn btn-blue btn-fixed-medium" href="<?php the_permalink($post_id);?>">Załóź konto</a>
                    <span>albo</span>
                <a class="btn btn-yellow btn-fixed-medium" href="<?php the_permalink($post_id_2);?>">Dowiedz się więcej</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="separator-gradient--full-width"></div>
            </div>
        </div>
    </div>
</div>
<?php $prev_post = get_adjacent_post( true, '', true, 'metody-platnosci_categories' ); ?>
<?php $next_post = get_adjacent_post( true, '', false, 'metody-platnosci_categories'); ?>
<div class="container-fluid other-payments align-middle" style="background:url(<?php echo get_template_directory_uri();?>/payment_single/steering.jpg);">
    <div class="cover"></div>
    <?php if ( is_a( $prev_post, 'WP_Post' ) ) {  ?>
                    <div class="left d-none d-sm-none d-md-inline-flex">
                        <a href="<?php echo get_permalink( $prev_post->ID ); ?>">
                        <img src="<?php echo get_template_directory_uri();?>/payment_single/left.svg" alt="">
                        <p class="d-none d-sm-none d-md-none d-lg-block">Dowiedz się<br>więcej</p>
                        </a>
                    </div>
    <?php } ?>
    <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
                    <div class="right d-none d-sm-none d-md-inline-flex">
                        <a href="<?php echo get_permalink( $next_post->ID ); ?>">
                        <img src="<?php echo get_template_directory_uri();?>/payment_single/right.svg" alt="">
                        <p class="d-none d-sm-none d-md-none d-lg-block">Dowiedz się<br>więcej</p>
                        </a>
                    </div>
    <?php } ?>
    <div class="container align-middle">
        <div class="row justify-content-center align-middle">
            <div class="col-sm-12 col-md-6 col-lg-4 prev">
            <?php if ( is_a( $prev_post, 'WP_Post' ) ) {  ?>
                <a href="<?php echo get_permalink( $prev_post->ID ); ?>"><h2><?php echo get_the_title( $prev_post->ID ); ?></h2>
                <p><?php echo get_the_excerpt( $prev_post->ID ); ?></p></a>
            <?php } ?>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 next">
            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
                <a href="<?php echo get_permalink( $next_post->ID ); ?>"><h2><?php echo get_the_title( $next_post->ID ); ?></h2>
                <p><?php echo get_the_excerpt( $next_post->ID ); ?></p></a>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById('facebook-share-<?php the_ID();?>').onclick = function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            app_id: '1918887535015636',
            href: '<?php the_permalink();?>',
        }, function(response){});
    }
</script>